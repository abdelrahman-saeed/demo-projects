<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Storehouse extends MY_Controller {

    function __construct() {
        parent::__construct();
    }

    function index() {

        $data['products'] = query("SELECT product.*,
            (select count(*) from supplier_invoice_product where product.id =supplier_invoice_product.product_id AND supplier_invoice_product.sold =0) as count,
            (select sum(coast) from supplier_invoice_product where product.id =supplier_invoice_product.product_id AND supplier_invoice_product.sold =0) as coast
            
from product  
 ");
        view('storehouseShow', $data);
    }
    
    function rest_product($id) {
        $data['product'] = get_row('product',['id'=>$id]);
        $data['rest'] = query(" SELECT supplier_invoice_product.*,
             supplier_invoice.date,
             product.name as name
             from supplier_invoice_product
             JOIN product ON product.id = supplier_invoice_product.product_id
             JOIN supplier_invoice ON supplier_invoice.id = supplier_invoice_product.supplier_invoice_id
             where product_id = $id && supplier_invoice_product.sold = 0
");
         view('rest_product',$data);
    }
    
    

    public function category() {

        $crud = new grocery_CRUD();

        $crud->set_table('category');
        $crud->set_subject('نوع');


        $crud->columns('id', 'name');

        $crud->display_as('id', 'ID');
        $crud->display_as('name', 'النوع');

        $crud->unset_delete();
        $crud->set_rules('name', 'النوع', 'trim|required|max_length[100]');

        $crud->callback_after_insert(array($this, 'user_insert_category'));
        $crud->callback_after_update(array($this, 'user_update_category'));

        $output = $crud->render();
        $this->load->view('crud', $output);
    }

    function user_insert_category($post_array, $primary_key) {
        $info = array(
            "title" => 'اضافة نوع منتج',
            "table" => 'category',
            "table_id" => $primary_key,
            "user_id" => $this->tank_auth->get_user_id()
        );

        userlog($info);

        return true;
    }

    function user_update_category($post_array, $primary_key) {
        $info = array(
            "title" => 'تعديل بيانات نوع منتج',
            "table" => 'category',
            "table_id" => $primary_key,
            "user_id" => $this->tank_auth->get_user_id()
        );

        userlog($info);

        return true;
    }

    public function product() {

        $crud = new grocery_CRUD();

        $crud->set_table('product');
        $crud->set_subject('منتج');

        $crud->columns('id', 'category_id', 'name', 'tiny_name', 'core', 'hard', 'ram', 'viga');
        $crud->fields('category_id', 'name','tiny_name', 'core', 'hard', 'ram', 'viga');

        $crud->display_as('id', 'ID');
        $crud->display_as('name', 'اسم المنتج');
        $crud->display_as('tiny_name', 'اسم صغير');
        $crud->display_as('category_id', 'نوع المنتج');
        $crud->display_as('archive', 'ارشيف');

        $crud->unset_delete();
        
        $crud->unique_fields(array('name'));
        
        $crud->required_fields('category_id');

        $crud->set_relation('category_id', 'category', 'name');

        $crud->callback_after_insert(array($this, 'user_insert_product'));
        $crud->callback_after_update(array($this, 'user_update_product'));

        $output = $crud->render();
        $this->load->view('crud', $output);
    }

    function user_insert_product($post_array, $primary_key) {
        $info = array(
            "title" => 'اضافة منتج',
            "table" => 'product',
            "table_id" => $primary_key,
            "user_id" => $this->tank_auth->get_user_id()
        );

        userlog($info);

        return true;
    }

    function user_update_product($post_array, $primary_key) {
        $info = array(
            "title" => 'تعديل بيانات منتج',
            "table" => 'product',
            "table_id" => $primary_key,
            "user_id" => $this->tank_auth->get_user_id()
        );

        userlog($info);

        return true;
    }

    //تفاصيل المخزن
    function details($prod_id) {

        $data['prod'] = get_row('product', array('id' => $prod_id));
        if ($data['prod'] == null)
            redirect(base_url() . '404');
        // المنتجات الواردة
        $income = query(" SELECT supplier_invoice.id,'income' as kind,supplier_id,type,supplier.name as supp_name,
            count(supplier_invoice_product.product_id)  as count , supplier_invoice.date as date , supplier_invoice_product.product_id 
   from supplier_invoice 
  
LEFT JOIN supplier
ON supplier.id = supplier_invoice.supplier_id 
LEFT JOIN supplier_invoice_product 
ON supplier_invoice_product.supplier_invoice_id=supplier_invoice.id And supplier_invoice.type = 'supplier' And supplier_invoice_product.product_id = $prod_id
GROUP BY supplier_invoice_product.supplier_invoice_id
HAVING count > 0 And supplier_invoice_product.product_id != 0 And supplier_invoice_product.supplier_invoice_id != 0

            ");

        //  print_r($income);die;
        // المنتجات الصادرة
        $outcome = query(" SELECT client_invoice.id,'outcome' as kind,client_id,client.name as client_name, 
 count(client_invoice_product.id)  as count , client_invoice.date as date , client_invoice_product.product_id 
              from client_invoice 
LEFT JOIN client
ON client.id = client_invoice.client_id 
LEFT JOIN client_invoice_product
ON client_invoice_product.client_invoice_id=client_invoice.id And client_invoice_product.product_id = $prod_id
GROUP BY client_invoice_product.client_invoice_id
HAVING count > 0 And client_invoice_product.product_id != 0 And client_invoice_product.client_invoice_id != 0
");

        // print_r($outcome);die;
        // المرتجعات
        $back = query(" SELECT supplier_invoice.id,'back' as kind,client_id,type,client_invoice_id,client.name as client_back_name, 
             count(supplier_invoice_product.id)  as count , supplier_invoice.date as date , supplier_invoice_product.product_id 
              from supplier_invoice 
LEFT JOIN client
ON client.id = supplier_invoice.client_id 
LEFT JOIN supplier_invoice_product
ON supplier_invoice_product.supplier_invoice_id=supplier_invoice.id And supplier_invoice.type = 'client_back' And supplier_invoice_product.product_id = $prod_id
GROUP BY supplier_invoice_product.supplier_invoice_id
HAVING count > 0 And supplier_invoice_product.product_id != 0 And supplier_invoice_product.supplier_invoice_id != 0;
");
        //print_r($back);die;

        $all_prod = array_merge($income, $outcome);
        $all = array_merge($all_prod, $back);
        // print_r($all);die;

        $orderByDate = array();
        foreach ($all as $key => $row) {
            $orderByDate[$key] = strtotime($row->date);
        }

        array_multisort($orderByDate, SORT_ASC, $all); //ترتيب تصاعدى حسب التاريخ
        //print_r($all);die;
        $data['data'] = $all;

        view('store_details', $data);
    }

    // البحث.....

    public function priceList() {
        view('priceListShow');
    }

    public function search_priceList() {
        //  if (!$this->input->is_ajax_request()) { exit('<h2>Bad request.</h2>'); }    

        $data = array('');
        $bar = $this->input->post('search_inp');
        $this->db->select('*,product.name As p_name,category.name As c_name');
        $this->db->from('supplier_invoice_product');
        $this->db->where('supplier_invoice_product.barcode', $bar);
        $this->db->join('product', 'product.id = supplier_invoice_product.product_id ', 'left');
        $this->db->join('category', 'category.id = product.category_id', 'left');
        $query = $this->db->get();
        $prod = $query->result();

//print_r($prod);die();
        if ($prod != NULL) {
            foreach ($prod as $row) {
                if ($row->core != NULL) {
                    $th1 = "<th>Core</th>";
                    $td1 = "<td>" . $row->core . "</td>";
                } else {
                    $th1 = '';
                    $td1 = '';
                }

                if ($row->hard != NULL) {
                    $th2 = "<th>Hard</th>";
                    $td2 = "<td>" . $row->hard . "</td>";
                } else {
                    $th2 = '';
                    $td2 = '';
                }

                if ($row->ram != NULL) {
                    $th3 = "<th>Ram</th>";
                    $td3 = "<td>" . $row->ram . "</td>";
                } else {
                    $th3 = '';
                    $td3 = '';
                }

                if ($row->viga != NULL) {
                    $th4 = "<th>Viga</th>";
                    $td4 = "<td>" . $row->viga . "</td>";
                } else {
                    $th4 = '';
                    $td4 = '';
                }

                $p_name = $row->p_name;
                $c_name = $row->c_name;
                $price = $row->price;
                $barcod = $row->barcode;
                if ($row->sold == 0) {
                    $data['state'] = 1;
                } else {
                    $data['state'] = 2;
                }
            }

            $info = '<tr class="b'.$barcod.'">
                        <td>' . $p_name . '</td>
                        <td>' . $c_name . '</td>
                        <td>' . $barcod . '</td>
                        <td>                                      
                <table  class="table table-striped table-bordered table-hover" >
                <thead>
                ' . $th1 . '
                ' . $th2 . '
                ' . $th3 . '
                ' . $th4 . '
                </thead>
                <tbody>
                    <tr class="bg_invoice">
                    ' . $td1 . '
                    ' . $td2 . '
                    ' . $td3 . '
                    ' . $td4 . '
                    </tr>
                    </tbody>
                </table> 
                     </td>
                     <td  >' . $price . '</td>
                     <td><a id="'.$price.'"   onclick="delete_row(this)" >حذف</a> </td>
                     </tr>';
            $data['prod'] = $info;
			$data['price'] = $price;
        } else {
            $data['state'] = 3;
        }

        echo json_encode($data);


//view('priceListShow');
    }

    public function searchResult() {
        $data = array('');
        $id = $this->cate_name;

        if ($this->input->post('search_data')) {
            $bar = $this->input->post('search_data');
            $product = get_row('supplier_invoice_product', $arr = array('barcode' => $bar));
            if ($product != null) {
                $id = $product->id;
            }
        }

        $this->db->select('*,product.name As p_name,category.name As c_name');
        $this->db->from('supplier_invoice_product');
        $this->db->where('supplier_invoice_product.id', $id);
        $this->db->join('product', 'product.id = supplier_invoice_product.product_id ', 'left');
        $this->db->join('category', 'category.id = product.category_id', 'left');
        $query = $this->db->get();
        $data['prod'] = $query->result();
//print_r($data);die();

        view('searchResult', $data);
    }

    public function get_autocomplete($search_data) {
        $this->db->select('*');
        $this->db->where('barcode', $search_data);
        $pro = $this->db->get('supplier_invoice_product', 1);
        if ($this->db->affected_rows() == NULL)
            return NULL;
        else {
            return $pro;
        }
    }

    public function autocomplete() {
        $data = array('');
        $search_data = $this->input->post('search_data');
        $query = $this->get_autocomplete($search_data);

        if ($query != NULL) {
            foreach ($query->result() as $row):
                if ($row->sold == 0) {
                    $data['sold'] = 0;
                    $data['resp'] = "<li style='color:green;'>المنتج متاح</li>";
                    $data['dir'] = $row->id;
                } else {
                    $data['sold'] = 1;
                    $data['resp'] = "<li style='color:red;'>المنتج مباع من قبل</li>";
                    $data['dir'] = $row->id;
                }
            endforeach;
        } else {
            $data['sold'] = 2;
            $data['resp'] = "<li style='color:red;'>المنتج غير متاح</li>";
            $data['dir'] = '#';
        }

        echo json_encode($data);
    }

}
