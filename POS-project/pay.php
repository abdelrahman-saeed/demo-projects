<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Pay extends MY_Controller {

    public $total = 0;

    public function __construct() {
        parent::__construct();

        }

//تقارير الخزينة    
//مجموع المشتريات    
    public function total_buy($date1 = null, $date2 = null) {

        $total = 0;
        if ($date1 != null and $date2 != null) {
            $buy = query("select 
(select sum(supplier_invoice.plus) from  supplier_invoice   where supplier_invoice.type='supplier' ) as inv_plus,
   (select sum(supplier_invoice.minus) from  supplier_invoice   where supplier_invoice.type='supplier' ) as inv_minus,            
    sum(supplier_invoice_product.plus)as pro_plus,
    sum(supplier_invoice_product.coast) as pro_coast
    from supplier_invoice
     JOIN supplier_invoice_product ON supplier_invoice.id = supplier_invoice_product.supplier_invoice_id
      where supplier_invoice.type='supplier' AND date >= '$date1' And date <= '$date2' ");
        } else {
            $buy = query("select 
(select sum(supplier_invoice.plus) from  supplier_invoice   where supplier_invoice.type='supplier' ) as inv_plus,
   (select sum(supplier_invoice.minus) from  supplier_invoice   where supplier_invoice.type='supplier' ) as inv_minus,            
    sum(supplier_invoice_product.plus)as pro_plus,
    sum(supplier_invoice_product.coast) as pro_coast
    from supplier_invoice
     JOIN supplier_invoice_product ON supplier_invoice.id = supplier_invoice_product.supplier_invoice_id
      where supplier_invoice.type='supplier'   ");
        }
     //  print_r($buy);die;

        if ($buy != null)
            $total = ($buy[0]->inv_plus + $buy[0]->pro_plus + $buy[0]->pro_coast) - $buy[0]->inv_minus;

        return $total;
    }
    
     // مجموع البيع المتوقع
    public function total_expect_sell($date1 = null, $date2 = null) {

        $total = 0;
        if ($date1 != null and $date2 != null) {
            $buy = query("select 
(select sum(supplier_invoice.plus) from  supplier_invoice   where supplier_invoice.type='supplier' ) as inv_plus,
   (select sum(supplier_invoice.minus) from  supplier_invoice   where supplier_invoice.type='supplier' ) as inv_minus,            
    sum(supplier_invoice_product.plus)as pro_plus,
    sum(supplier_invoice_product.price) as pro_price
    from supplier_invoice
     JOIN supplier_invoice_product ON supplier_invoice.id = supplier_invoice_product.supplier_invoice_id
      where supplier_invoice.type='supplier' AND date >= '$date1' And date <= '$date2' ");
        } else {
            $buy = query("select 
(select sum(supplier_invoice.plus) from  supplier_invoice   where supplier_invoice.type='supplier' ) as inv_plus,
   (select sum(supplier_invoice.minus) from  supplier_invoice   where supplier_invoice.type='supplier' ) as inv_minus,            
    sum(supplier_invoice_product.plus)as pro_plus,
    sum(supplier_invoice_product.price) as pro_price
    from supplier_invoice
     JOIN supplier_invoice_product ON supplier_invoice.id = supplier_invoice_product.supplier_invoice_id
      where supplier_invoice.type='supplier'   ");
        }
      // print_r($buy);die;

        if ($buy != null)
            $total = $buy[0]->pro_price   ;

        return $total;
    }
  
    //مجموع المرتجعات    
    public function total_back($date1 = null, $date2 = null) {

        $total = 0;
        if ($date1 != null and $date2 != null) {
            $back = query("select sum(supplier_invoice_product.coast)as coast 
    from supplier_invoice
    JOIN supplier_invoice_product ON supplier_invoice.id = supplier_invoice_product.supplier_invoice_id
where type='client_back' AND date >= '$date1' And date <= '$date2' ");
        } else {
            $back = query("select sum(supplier_invoice_product.coast)as coast 
    from supplier_invoice
    JOIN supplier_invoice_product ON supplier_invoice.id = supplier_invoice_product.supplier_invoice_id
where type='client_back' ");
        }
   

        if ($back != null)
            $total =  $back[0]->coast ;

        return $total;
    }
    

//مجموع المبيعات    
    public function total_sell($date1 = null, $date2 = null) {

        $total = 0;
        if ($date1 != null and $date2 != null) {
            $sell = query("select sum(plus) as cm_plus, sum(minus) as cm_minus,
    (select sum(client_invoice_product.price) from client_invoice_product) as c_price
    from client_invoice where date >= '$date1' And date <= '$date2' ");
        } else {
            $sell = query("select sum(plus) as cm_plus, sum(minus) as cm_minus, 
    (select sum(client_invoice_product.price) from client_invoice_product) as c_price
    from client_invoice");
        }


        if ($sell != null)
            $total = $sell[0]->cm_plus + $sell[0]->c_price - $sell[0]->cm_minus;

        return $total;
    }

// مجموع المصروفات
    public function all_outcome($date1 = null, $date2 = null) {

        $total = 0;
        if ($date1 != null and $date2 != null) {
            $out = query("select sum(price) as out_price from pay where category = 'outcome' And date >= '$date1' And date <= '$date2' ");
        } else {
            $out = query("select sum(price) as out_price from pay where category = 'outcome'");
        }

        if ($out != null)
            $total = $out[0]->out_price;

        return $total;
    }

// مجموع الايرادات
    public function all_income($date1 = null, $date2 = null) {

        $total = 0;
        if ($date1 != null and $date2 != null) {
            $in = query("select sum(price) as in_price from pay where category = 'income' And date >= '$date1' And date <= '$date2' ");
        } else {
            $in = query("select sum(price) as in_price from pay where category = 'income'");
        }

        if ($in != null)
            $total = $in[0]->in_price;

        return $total;
    }

// سعر البضاعة فى المخزن
    public function prod_store_coast($date1 = null, $date2 = null) {

        $total = 0;
        if ($date1 != null and $date2 != null) {
            $coast = query("select sum(coast) as prod_coast from supplier_invoice_product join supplier_invoice on supplier_invoice.id=supplier_invoice_product.supplier_invoice_id where sold = '0' And date >= '$date1' And date <= '$date2' ");
        } else {
            $coast = query("select sum(coast) as prod_coast from supplier_invoice_product where sold = '0'");
        }

       //print_r($coast);die;
        if ($coast != null)
            $total = $coast[0]->prod_coast;

        return $total;
    }

// مجموع دفعات العملاء
    public function all_clientpay($date1 = null, $date2 = null) {

        $total = 0;
        if ($date1 != null and $date2 != null) {
            $cp = query("select sum(price) as cp_price from pay where category = 'client' And date >= '$date1' And date <= '$date2' ");
        } else {
            $cp = query("select sum(price) as cp_price from pay where category = 'client'");
        }

        if ($cp != null)
            $total = $cp[0]->cp_price;

        return $total;
    }

// مجموع دفعات الموردين
    public function all_supplierpay($date1 = null, $date2 = null) {

        $total = 0;
        if ($date1 != null and $date2 != null) {
            $sp = query("select sum(price) as sp_price from pay where category = 'supplier' And date >= '$date1' And date <= '$date2' ");
        } else {
            $sp = query("select sum(price) as sp_price from pay where category = 'supplier'");
        }

        if ($sp != null)
            $total = $sp[0]->sp_price;

        return $total;
    }

//التقارير    
    public function reports($date1 = null, $date2 = null) {
        $data = array('');
        $date1 = $this->input->post('from');
        $date2 = $this->input->post('to');

        if (isset($date1) and $date1 != null and isset($date2) and $date2 != null) {

            $date1 = $this->input->post('from') . ' 00:00';
            $date2 = $this->input->post('to') . ' 23:59';


            $buy = $this->total_buy($date1, $date2);
            $prod_coast = $this->prod_store_coast($date1, $date2);
            $sell_expect = $this->total_expect_sell($date1, $date2);
            $out = $this->all_outcome($date1, $date2);
            $in = $this->all_income($date1, $date2);
            $client = $this->all_clientpay($date1, $date2);
            $supp = $this->all_supplierpay($date1, $date2);
            $back = $this->total_back($date1, $date2);
            $sell = $this->total_sell($date1, $date2)-$back;
        } else {
            
            $buy = $this->total_buy();
            $sell_expect = $this->total_expect_sell();
            $prod_coast = $this->prod_store_coast();
            $out = $this->all_outcome();
            $in = $this->all_income();
            $client = $this->all_clientpay();
            $supp = $this->all_supplierpay();
            $back = $this->total_back();
            $sell = $this->total_sell()-$back;
        }

        $data = array('buy' => $buy, 'sell' => $sell, 'out' => $out, 'in' => $in, 'client' => $client, 'supp' => $supp,'prod_coast'=>$prod_coast);

        $expected_earn = $sell_expect  - $buy ;
        $data['expected_earn'] = round($expected_earn); // القيمة المتوقعة لارباح المنتجات


        $all_expected_earn = ($sell_expect + $in )  -( $buy + $out);
        $data['all_expected_earn'] = round($all_expected_earn); // القيمة المتوقعة للارباح عامة
        
        $real_earn = ($client + $in) - ($buy + $out);
        $data['real_earn'] = round($real_earn);  // القيمة الفعلية

 
        $re_client = $sell -  $client ;
        $data['re_client'] = round($re_client); // باقى حساب العملاء


        $re_supp = $buy - $supp;
        $data['re_supp'] = round($re_supp); // باقى حساب الموردين

        
        $partenrs = query("select * from partner"); // الشركاء
        $data['partenrs'] = $partenrs;

        $credit = query("select sum(ini_credit) as total_c from partner");
        $total_credit = $credit[0]->total_c;
        
        $mony_save = ($total_credit+$client+$in)-($supp+$out) ;
        $data['mony_save'] = $mony_save;
        
// print_r($data);die;
        view('pay_reports', $data);
    }

//------------------------------------------------------------------------------------------------------------------------------------------
    //مجموع الواردات
    function totalIncome() {
        $data = array('');
       
        $date1 = $this->input->post('from') . ' 00:00';
        $date2 = $this->input->post('to') . ' 23:59';
        $where = "";
 if($this->input->post('from') != null && $this->input->post('to') != null)
     $where = " And pay.date >= '$date1'  And pay.date <= '$date2' ";
        $total = query(" SELECT sum(pay.price) as total_price,pay.category,pay.income_reson_id,pay.date,income_reson.name as name 
            from pay
            LEFT JOIN income_reson ON pay.income_reson_id = income_reson.id
              where pay.category = 'income'
              $where
            GROUP BY pay.income_reson_id
           ");
        $data['total_in'] = $total;
        //echo $this->db->last_query();die;
       // print_r($total);die;
        view('total_income', $data);
    }

    //مجموع المصاريف
    function totalOutcome() {
        $data = array('');
        $date1 = $this->input->post('from') . ' 00:00';
        $date2 = $this->input->post('to') . ' 23:59';
 $where = "";
 if($this->input->post('from') != null && $this->input->post('to') != null)
     $where = " And pay.date >= '$date1'  And pay.date <= '$date2' ";
   
        $total = query("SELECT sum(pay.price) as total_price,pay.category,pay.outcome_reson_id,pay.date,outcome_reson.name as name 
            from pay
            LEFT JOIN outcome_reson ON pay.outcome_reson_id = outcome_reson.id
              where pay.category = 'outcome'
              $where
            GROUP BY pay.outcome_reson_id");
        $data['total_out'] = $total;
        //echo $this->db->last_query();die;
        //print_r($total);die;
        view('total_outcome', $data);
    }

    
    
    
    public function client(){
    $data=array();
    $total = 0;
    //$all_clients = table('client');
   $all_clients =  query('select * from client');
    //print_r($all_clients);die;
    foreach ($all_clients as $key => $value) {
        $id = $all_clients[$key]->id;
        $total = $all_clients[$key]->ini_credit;
        $invoices = table('client_invoice', ['client_id' => $id]);        // الفواتير
        if ($invoices != null) {
            foreach ($invoices as $row) {
                $total += invoice_client_price($row->id);         // بيجمع اسعار الفواتير
            }
        }
        
        $total -= pay_client($id);
        if ($total > 0)
            $text = 'عليه';
        else
            $text = 'ليه';
        if ($total == 0)
            $text = '';
        $total = abs($total) . '  ' . $text;
        
        $all_clients[$key]->total = $total;
        
    }
    //print_r($all_clients);die;   
    
     $data['all'] = $all_clients;  
     
     view('countClient', $data);
                
    }
    
    public function supplier(){
    $data=array();
    $total = 0;
   $all_suppliers =  query('select * from supplier');
    //print_r($all_suppliers);die;
    foreach ($all_suppliers as $key => $value) {
        $id = $all_suppliers[$key]->id;
        $total = $all_suppliers[$key]->ini_credit;
        $invoices = table('supplier_invoice', ['supplier_id' => $id]);        // الفواتير
        if ($invoices != null) {
            foreach ($invoices as $row) {
                $total += invoice_supplier_price($row->id);         // بيجمع اسعار الفواتير
            }
        }
        
        $total -= pay_supplier($id);
        if ($total > 0)
           $text = 'ليه'; 
        else
           $text = 'عليه'; 
        if ($total == 0)
            $text = '';
        $total = abs($total) . '  ' . $text;
        
        $all_suppliers[$key]->total = $total;
        
    }
    //print_r($all_suppliers);die;   
    
     $data['all'] = $all_suppliers;  
     
     view('countSupplier', $data);
                
    }
    
    
    public function client_group(){
        
    $data=array();
    $total = 0;
    $client_total = 0;
    //$all_clients = table('client');
   $all_groups =  query('select * from client_category');
    //print_r($all_clients);die;
    foreach ($all_groups as $key => $value) {
        $total = 0;
        
        $id = $all_groups[$key]->id;
        
        $all_groups[$key]->clients = table('client', array('category_id'=>$id));  
      
        
        
        foreach ($all_groups[$key]->clients as $key2 => $value2) {
        $client_total = 0;
            
        $client_total += $all_groups[$key]->clients[$key2]->ini_credit;
        $client_id = $all_groups[$key]->clients[$key2]->id;
        
        $invoices = table('client_invoice', ['client_id' => $client_id]);        // الفواتير
        if ($invoices != null) {
            foreach ($invoices as $row) {
                $client_total += invoice_client_price($row->id);         // بيجمع اسعار الفواتير
            }
        }
            
        $client_total -= pay_client($client_id);
        
        $all_groups[$key]->clients[$key2]->total = $client_total;
        
        $total += $client_total;
        
        }
        
        
        if ($total > 0)
            $text = 'عليهم';
        else
            $text = 'ليهم';
        if ($total == 0)
            $text = '';
        $total = abs($total) . '  ' . $text;
        
        $all_groups[$key]->total = $total;
       
        
    }
    //print_r($all_groups);die;   
    
     $data['all'] = $all_groups;  
     
     view('count_client_group', $data);
 
        
        
    }


  public function supplier_group(){
        
     $data=array();
    $total = 0;
    $supplier_total = 0;
    //$all_clients = table('client');
   $all_groups =  query('select * from supplier_category');
    //print_r($all_clients);die;
    foreach ($all_groups as $key => $value) {
        $total = 0;
        
        $id = $all_groups[$key]->id;
        
        $all_groups[$key]->suppliers = table('supplier', array('category_id'=>$id));  
      
        
        
        foreach ($all_groups[$key]->suppliers as $key2 => $value2) {
        $supplier_total = 0;
            
        $supplier_total += $all_groups[$key]->suppliers[$key2]->ini_credit;
        $supplier_id = $all_groups[$key]->suppliers[$key2]->id;
        
        $invoices = table('supplier_invoice', ['supplier_id' => $supplier_id]);        // الفواتير
        if ($invoices != null) {
            foreach ($invoices as $row) {
                $supplier_total += invoice_supplier_price($row->id);         // بيجمع اسعار الفواتير
            }
        }
            
        $supplier_total -= pay_supplier($supplier_id);
        
        $all_groups[$key]->suppliers[$key2]->total = $supplier_total;
        
        $total += $supplier_total;
        
        }
        
        
        if ($total > 0)
            $text = 'ليهم';
        else
            $text = 'عليهم';
        if ($total == 0)
            $text = '';
        $total = abs($total) . '  ' . $text;
        
        $all_groups[$key]->total = $total;
       
        
    }
    //print_r($all_groups);die;   
    
     $data['all'] = $all_groups;  
     
     view('count_supplier_group', $data);
 
        
    
        
        
    }







    /*    
    function client() {

        $crud = new grocery_CRUD();
        $crud->set_table('client');
        $crud->set_subject('حساب العملاء');
        $crud->columns('name', 'total');
        $crud->callback_column('total', array($this, '_callback_column_total_client'));
        $crud->callback_column('name', array($this, '_callback_column_name_client'));
        $crud->unset_operations();
        $crud->display_as('name', 'الاسم')
                ->display_as('total', 'الحساب');
        $output = $crud->render();
        $this->_example_output($output);
    }

    function _callback_column_name_client($primary_key, $row) {

        return '<a href="' . base_url() . 'pay/clientdetails/' . $row->id . '">' . $row->name . '</a>';
    }

    function _callback_column_total_client($primary_key, $row) {
        $id = $row->id;
        $client = get_row('client', array('id' => $id));
        $invices = table('client_invoice', ['client_id' => $id]);        // الفواتير
        $total = $client->ini_credit;
        if ($invices != null) {
            foreach ($invices as $row) {
                $total += invoice_client_price($row->id);         // بيجمع اسعار الفواتير
            }
        }

        $total -= pay_client($id);
        if ($total > 0)
            $text = 'عليه';
        else
            $text = 'ليه';
        if ($total == 0)
            $text = '';
        return abs($total) . '  ' . $text;
    }
*/
    function clientdetails($client_id) {

        $data['client'] = get_row('client', array('id' => $client_id));
        if ($data['client'] == null)
            redirect(base_url() . '404');
        // فواتير البيع
        $invoices = query(" SELECT client_invoice.*,'invoice' as type
              from client_invoice 
where client_invoice.client_id = $client_id
");
        // فواتير المرتجع
        $back = query(" SELECT supplier_invoice.*,'back' as type
              from supplier_invoice  
where supplier_invoice.client_id = $client_id
");

        // الدفعات
        $pay = query("SELECT pay.*,'pay' as type from pay 
                 where pay.client_id = $client_id AND pay.category = 'client'
                ");

        $all_invoice = array_merge($invoices, $back);
        $all = array_merge($all_invoice, $pay);
        $orderByDate = array();
        foreach ($all as $key => $row) {
            $orderByDate[$key] = strtotime($row->date);
        }

        array_multisort($orderByDate, SORT_ASC, $all); //ترتيب تصاعدى حسب التاريخ
        //print_r($all);die;
        $data['data'] = $all;

        view('client_details', $data);
    }

    function supplierDetails($supplier_id) {

        $data['supplier'] = get_row('supplier', array('id' => $supplier_id));
        if ($data['supplier'] == null)
            redirect(base_url() . '404');
        // فواتير البيع
        $invoices = query(" SELECT supplier_invoice.*,'invoice' as type
              from supplier_invoice 
where supplier_invoice.supplier_id = $supplier_id
");


        // الدفعات
        $pay = query("SELECT pay.*,'pay' as type from pay 
                 where pay.supplier_id = $supplier_id AND pay.category = 'supplier'
                ");


        $all = array_merge($invoices, $pay);
        $orderByDate = array();
        foreach ($all as $key => $row) {
            $orderByDate[$key] = strtotime($row->date);
        }

        array_multisort($orderByDate, SORT_ASC, $all); //ترتيب تصاعدى حسب التاريخ

        $data['data'] = $all;

        view('supplier_details', $data);
    }
/*
    function supplier() {

        $crud = new grocery_CRUD();
        $crud->set_table('supplier');
        $crud->set_subject('حساب الموردين');
        $crud->columns('name', 'total');
        $crud->callback_column('total', array($this, '_callback_column_total_supplier'));
        $crud->callback_column('name', array($this, '_callback_column_name_supplier'));
        $crud->unset_operations();
        $crud->display_as('name', 'الاسم')
                ->display_as('total', 'الحساب');
        $output = $crud->render();
        $this->_example_output($output);
    }

    function _callback_column_total_supplier($primary_key, $row) {
        $id = $row->id;
        $supplier = get_row('supplier', array('id' => $id));
        $invices = table('supplier_invoice', ['supplier_id' => $id]);        // الفواتير
        $total = $supplier->ini_credit;
        if ($invices != null) {
            foreach ($invices as $row) {
                $total += invoice_supplier_price($row->id);         // بيجمع اسعار الفواتير
            }
        }
        $total -= pay_supplier($id);
        if ($total > 0)
            $text = 'ليه';
        else
            $text = 'عليه';
        if ($total == 0)
            $text = '';
        return abs($total) . '  ' . $text;
    }

    function _callback_column_name_supplier($primary_key, $row) {

        return '<a href="' . base_url() . 'pay/supplierDetails/' . $row->id . '">' . $row->name . '</a>';
    }
*/    
    
    public function show_receipt($id) {
      $re =   get_row('receipt',['id'=>$id]);
      if(empty($re))
          redirect (site_url());
      $pay = get_row('pay',['id'=>$re->pay_id]);
        view('show_receipt',array('result'=>$re,'pay'=>$pay));
    }

    public function receipt() {

        $crud = new grocery_CRUD();

        $crud->set_table('receipt');
        $crud->set_subject('ايصال');

        $crud->columns('id', 'name', 'desc', 'date',  'done');
        $crud->fields('name','mobile','serial','desc','price','deposit','date');
        
        $crud->callback_column('done', array($this, '_callback_column_done'));
        
        $crud->display_as('name', 'اسم العميل')
        ->display_as('desc', 'تفاصيل')
        ->display_as('mobile', 'الموبيل')
        ->display_as('price', 'السعر')
        ->display_as('deposit', 'الدفعة')
        ->display_as('serial', 'سريال')
        ->display_as('date', 'تاريخ الاستلام');

        $crud->unset_texteditor('desc');
        $crud->field_type('date_time', 'hidden');
        $crud->unset_edit();
        $crud->required_fields('name','mobile','price','deposit');
       
        $crud->callback_after_insert(array($this, 'user_insert_receipt'));
        $crud->callback_after_update(array($this, 'user_update_receipt'));
        $crud->callback_before_delete(array($this, 'user_delete_receipt'));
        
        $crud->add_action('طباعة', '', '','fa fa-print',array($this,'print_receipt'));
        
        $output = $crud->render();
        $this->load->view('crud', $output);
    }
    
    
    function print_receipt($primary_key , $row){
        
return site_url('pay/show_receipt').'/'.$row->id;

    }
            
    
    function _callback_column_done($primary_key, $row){
        if($row->done == 0)
        {
            return '<a class="btn btn-info" href="'.  base_url().'pay/receipt_done/'.$row->id.'" title="سيتم تسجيل باقى القيمه المطلوبه" >تسليم الان</a>';
        }else
            return 'تم التسليم';
    }

    function user_insert_receipt($post_array, $primary_key) {
        $info = array(
            "title" => 'اضافة ايصال',
            "table" => 'receipt',
            "table_id" => $primary_key,
            "user_id" => $this->tank_auth->get_user_id()
        );

        userlog($info);

        $this->General_model->add('client',array(
            'name'=>$post_array['name'],
            'mobile'=>$post_array['mobile']
            ));
        $client_id = $this->db->insert_id();
        
        $this->General_model->add('pay',array(
            'category'=>'income',
            'income_reson_id'=>1,
            'client_id'=>$client_id,
            'note'=>$post_array['desc'].'<br>'.' دفعه ايصال رقم '.$primary_key,
            'price'=>$post_array['deposit'],
            'date'=>date('Y-m-d H:i:s'),
            'add_user_id'=>$this->tank_auth->get_user_id()
            ));
        $pay_id = $this->db->insert_id();
        
        $this->db->where('id',$primary_key)->update('receipt',['pay_id'=>$pay_id,'client_id'=>$client_id]);
        return true;
    }

    function user_update_receipt($post_array, $primary_key) {
        $info = array(
            "title" => 'تعديل بيانات ايصال',
            "table" => 'receipt',
            "table_id" => $primary_key,
            "user_id" => $this->tank_auth->get_user_id()
        );

        userlog($info);

        return true;
    }

    function user_delete_receipt($primary_key) {
        
        $row = get_row('receipt',['id'=>$primary_key]);
        $this->db->where('id',$row->pay_id)->delete('pay');
        
        
        $info = array(
            "title" => 'حذف ايصال',
            "table" => 'receipt',
            "table_id" => $primary_key,
            "user_id" => $this->tank_auth->get_user_id()
        );

        userlog($info);

        return true;
    }
    
    function receipt_done($id) {
        $row = get_row('receipt', ['id'=>$id]);
        if(empty($row))
            redirect (base_url('pay/receipt'));
        $rest = $row->price - $row->deposit;
       if($rest > 0){ 
         $this->General_model->add('pay',array(
            'category'=>'income',
            'income_reson_id'=>1,
            'client_id'=>$row->client_id,
            'note'=>$row->desc.'<br>'.' باقى ايصال رقم '.$id,
            'price'=>$rest,
            'date'=>date('Y-m-d H:i:s'),
            'add_user_id'=>$this->tank_auth->get_user_id()
            ));
       }
         $this->db->where('id',$id)->update('receipt',['done'=>1,'done_date'=>date('Y-m-d H:i:s')]);
        redirect (base_url('pay/receipt/success/'.$id));
         
    }
    

    public function client_pay() {

        $crud = new grocery_CRUD();
        $crud->where('pay.category', 'client');

        $crud->set_table('pay');
        $crud->set_subject('دفعة عميل');

        $crud->columns('id', 'client_id', 'price', 'note', 'date', 'staff_id');

        $crud->display_as('id', 'ID');
        $crud->display_as('client_id', 'العميل');
        $crud->display_as('price', 'المبلغ');
        $crud->display_as('note', 'ملاحظات');
        $crud->display_as('date', 'التاريخ');
        $crud->display_as('staff_id', 'الموظف');

        $crud->display_as('add_date_time', 'تاريخ الاضافة');
        $crud->display_as('add_user_id', 'رقم المستخدم المضيف');
        $crud->display_as('edit_date_time', 'تاريخ آخر تعديل');
        $crud->display_as('edit_user_id', 'رقم المستخدم الذى عدل البيانات');

        $crud->unset_texteditor('note');

        $crud->field_type('category', 'hidden', 'client');
        $crud->field_type('supplier_id', 'hidden', '0');
        $crud->field_type('invoice_id', 'hidden', '0');
        $crud->field_type('outcome_reson_id', 'hidden', '0');
        $crud->field_type('income_reson_id', 'hidden', '0');

        $crud->field_type('add_date_time', 'invisible');
        $crud->field_type('add_user_id', 'invisible');
        $crud->field_type('edit_date_time', 'invisible');
        $crud->field_type('edit_user_id', 'invisible');

        $crud->set_relation('client_id', 'client', 'name');
        $crud->set_relation('staff_id', 'staff', 'name');

        $crud->set_rules('client_id', ' العميل', 'trim|required');
        $crud->set_rules('staff_id', ' الموظف', 'trim|required');
        $crud->set_rules('date', 'التاريخ', 'required');
        $crud->set_rules('price', 'المبلغ', 'trim|required');
        $crud->set_rules('note', 'ملاحظات', 'max_length[400]');

        $crud->callback_before_insert(array($this, 'add_date_user_id'));
        $crud->callback_before_update(array($this, 'update_date_user_id'));

        $crud->callback_after_insert(array($this, 'user_insert_client_pay'));
        $crud->callback_after_update(array($this, 'user_update_client_pay'));
        $crud->callback_after_delete(array($this, 'user_delete_client_pay'));



        $output = $crud->render();
        $this->load->view('crud', $output);
    }

    function user_insert_client_pay($post_array, $primary_key) {
        $info = array(
            "title" => 'اضافة دفعة عميل',
            "table" => 'pay',
            "table_id" => $primary_key,
            "user_id" => $this->tank_auth->get_user_id()
        );

        userlog($info);

        return true;
    }

    function user_update_client_pay($post_array, $primary_key) {
        $info = array(
            "title" => 'تعديل بيانات دفعة عميل',
            "table" => 'pay',
            "table_id" => $primary_key,
            "user_id" => $this->tank_auth->get_user_id()
        );

        userlog($info);

        return true;
    }

    function user_delete_client_pay($primary_key) {
        $info = array(
            "title" => 'حذف دفعة عميل',
            "table" => 'pay',
            "table_id" => $primary_key,
            "user_id" => $this->tank_auth->get_user_id()
        );

        userlog($info);

        return true;
    }

    public function supplier_pay() {

        $crud = new grocery_CRUD();
        $crud->where('pay.category', 'supplier');

        $crud->set_table('pay');
        $crud->set_subject('دفعة مورد');

        $crud->columns('id', 'supplier_id', 'price', 'note', 'date', 'staff_id');

        $crud->display_as('id', 'ID');
        $crud->display_as('supplier_id', 'المورد');
        $crud->display_as('price', 'المبلغ');
        $crud->display_as('note', 'ملاحظات');
        $crud->display_as('date', 'التاريخ');
        $crud->display_as('staff_id', 'الموظف');

        $crud->display_as('add_date_time', 'تاريخ الاضافة');
        $crud->display_as('add_user_id', 'رقم المستخدم المضيف');
        $crud->display_as('edit_date_time', 'تاريخ آخر تعديل');
        $crud->display_as('edit_user_id', 'رقم المستخدم الذى عدل البيانات');

        $crud->unset_texteditor('note');

        $crud->field_type('category', 'hidden', 'supplier');
        $crud->field_type('client_id', 'hidden', '0');
        $crud->field_type('invoice_id', 'hidden', '0');
        $crud->field_type('outcome_reson_id', 'hidden', '0');
        $crud->field_type('income_reson_id', 'hidden', '0');

        $crud->field_type('add_date_time', 'invisible');
        $crud->field_type('add_user_id', 'invisible');
        $crud->field_type('edit_date_time', 'invisible');
        $crud->field_type('edit_user_id', 'invisible');

        $crud->set_relation('supplier_id', 'supplier', 'name');
        $crud->set_relation('staff_id', 'staff', 'name');

        $crud->set_rules('supplier_id', ' العميل', 'trim|required');
        $crud->set_rules('staff_id', ' الموظف', 'trim|required');
        $crud->set_rules('date', 'التاريخ', 'required');
        $crud->set_rules('price', 'المبلغ', 'trim|required');
        $crud->set_rules('note', 'ملاحظات', 'max_length[400]');

        $crud->callback_before_insert(array($this, 'add_date_user_id'));
        $crud->callback_before_update(array($this, 'update_date_user_id'));

        $crud->callback_after_insert(array($this, 'user_insert_supplier_pay'));
        $crud->callback_after_update(array($this, 'user_update_supplier_pay'));
        $crud->callback_after_delete(array($this, 'user_delete_supplier_pay'));

        $output = $crud->render();
        $this->load->view('crud', $output);
    }

    function user_insert_supplier_pay($post_array, $primary_key) {
        $info = array(
            "title" => 'اضافة دفعة مورد',
            "table" => 'pay',
            "table_id" => $primary_key,
            "user_id" => $this->tank_auth->get_user_id()
        );

        userlog($info);

        return true;
    }

    function user_update_supplier_pay($post_array, $primary_key) {
        $info = array(
            "title" => 'تعديل بيانات دفعة مورد',
            "table" => 'pay',
            "table_id" => $primary_key,
            "user_id" => $this->tank_auth->get_user_id()
        );

        userlog($info);

        return true;
    }

    function user_delete_supplier_pay($primary_key) {
        $info = array(
            "title" => 'حذف دفعة مورد',
            "table" => 'pay',
            "table_id" => $primary_key,
            "user_id" => $this->tank_auth->get_user_id()
        );

        userlog($info);

        return true;
    }

    public function income_pay() {

        $crud = new grocery_CRUD();
        $crud->where('pay.category', 'income');

        $crud->set_table('pay');
        $crud->set_subject('مبالغ واردة');

        $crud->columns('id', 'income_reson_id', 'price', 'note', 'date', 'staff_id');

        $crud->display_as('id', 'ID');
        $crud->display_as('income_reson_id', 'سبب المبلغ الوارد');
        $crud->display_as('price', 'المبلغ');
        $crud->display_as('note', 'ملاحظات');
        $crud->display_as('date', 'التاريخ');
        $crud->display_as('staff_id', 'الموظف');

        $crud->display_as('add_date_time', 'تاريخ الاضافة');
        $crud->display_as('add_user_id', 'رقم المستخدم المضيف');
        $crud->display_as('edit_date_time', 'تاريخ آخر تعديل');
        $crud->display_as('edit_user_id', 'رقم المستخدم الذى عدل البيانات');

        $crud->unset_texteditor('note');

        $crud->field_type('category', 'hidden', 'income');
        $crud->field_type('client_id', 'hidden', '0');
        $crud->field_type('supplier_id', 'hidden', '0');
        $crud->field_type('invoice_id', 'hidden', '0');
        $crud->field_type('outcome_reson_id', 'hidden', '0');

        $crud->field_type('add_date_time', 'invisible');
        $crud->field_type('add_user_id', 'invisible');
        $crud->field_type('edit_date_time', 'invisible');
        $crud->field_type('edit_user_id', 'invisible');

        $crud->set_relation('income_reson_id', 'income_reson', 'name');
        $crud->set_relation('staff_id', 'staff', 'name');

        $crud->set_rules('income_reson_id', 'سبب المبلغ الوارد', 'trim|required');
        $crud->set_rules('staff_id', ' الموظف', 'trim|required');
        $crud->set_rules('date', 'التاريخ', 'required');
        $crud->set_rules('price', 'المبلغ', 'trim|required');
        $crud->set_rules('note', 'ملاحظات', 'max_length[400]');

        $crud->callback_before_insert(array($this, 'add_date_user_id'));
        $crud->callback_before_update(array($this, 'update_date_user_id'));

        $crud->callback_after_insert(array($this, 'user_insert_income_pay'));
        $crud->callback_after_update(array($this, 'user_update_income_pay'));
        $crud->callback_after_delete(array($this, 'user_delete_income_pay'));

        $output = $crud->render();
        $this->load->view('crud', $output);
    }

    function user_insert_income_pay($post_array, $primary_key) {
        $info = array(
            "title" => 'اضافة مبالغ واردة',
            "table" => 'pay',
            "table_id" => $primary_key,
            "user_id" => $this->tank_auth->get_user_id()
        );

        userlog($info);

        return true;
    }

    function user_update_income_pay($post_array, $primary_key) {
        $info = array(
            "title" => 'تعديل بيانات مبالغ واردة',
            "table" => 'pay',
            "table_id" => $primary_key,
            "user_id" => $this->tank_auth->get_user_id()
        );

        userlog($info);

        return true;
    }

    function user_delete_income_pay($primary_key) {
        $info = array(
            "title" => 'حذف مبالغ واردة ',
            "table" => 'pay',
            "table_id" => $primary_key,
            "user_id" => $this->tank_auth->get_user_id()
        );

        userlog($info);

        return true;
    }

    public function outcome_pay() {

        $crud = new grocery_CRUD();
        $crud->where('pay.category', 'outcome');

        $crud->set_table('pay');
        $crud->set_subject('مصاريف');

        $crud->columns('id', 'outcome_reson_id', 'price', 'note', 'date', 'staff_id');

        $crud->display_as('id', 'ID');
        $crud->display_as('outcome_reson_id', 'سبب المصاريف');
        $crud->display_as('price', 'المبلغ');
        $crud->display_as('note', 'ملاحظات');
        $crud->display_as('date', 'التاريخ');
        $crud->display_as('staff_id', 'الموظف');

        $crud->display_as('add_date_time', 'تاريخ الاضافة');
        $crud->display_as('add_user_id', 'رقم المستخدم المضيف');
        $crud->display_as('edit_date_time', 'تاريخ آخر تعديل');
        $crud->display_as('edit_user_id', 'رقم المستخدم الذى عدل البيانات');

        $crud->unset_texteditor('note');

        $crud->field_type('category', 'hidden', 'outcome');
        $crud->field_type('client_id', 'hidden', '0');
        $crud->field_type('supplier_id', 'hidden', '0');
        $crud->field_type('invoice_id', 'hidden', '0');
        $crud->field_type('income_reson_id', 'hidden', '0');

        $crud->field_type('add_date_time', 'invisible');
        $crud->field_type('add_user_id', 'invisible');
        $crud->field_type('edit_date_time', 'invisible');
        $crud->field_type('edit_user_id', 'invisible');

        $crud->set_relation('outcome_reson_id', 'outcome_reson', 'name');
        $crud->set_relation('staff_id', 'staff', 'name');

        $crud->set_rules('outcome_reson_id', 'سبب المصاريف', 'trim|required');
        $crud->set_rules('staff_id', ' الموظف', 'trim|required');
        $crud->set_rules('date', 'التاريخ', 'required');
        $crud->set_rules('price', 'المبلغ', 'trim|required');
        $crud->set_rules('note', 'ملاحظات', 'max_length[400]');

        $crud->callback_before_insert(array($this, 'add_date_user_id'));
        $crud->callback_before_update(array($this, 'update_date_user_id'));

        $crud->callback_after_insert(array($this, 'user_insert_outcome_pay'));
        $crud->callback_after_update(array($this, 'user_update_outcome_pay'));
        $crud->callback_after_delete(array($this, 'user_delete_outcome_pay'));

        $output = $crud->render();
        $this->load->view('crud', $output);
    }

    function user_insert_outcome_pay($post_array, $primary_key) {
        $info = array(
            "title" => 'اضافة مصاريف',
            "table" => 'pay',
            "table_id" => $primary_key,
            "user_id" => $this->tank_auth->get_user_id()
        );

        userlog($info);

        return true;
    }

    function user_update_outcome_pay($post_array, $primary_key) {
        $info = array(
            "title" => 'تعديل بيانات مصاريف',
            "table" => 'pay',
            "table_id" => $primary_key,
            "user_id" => $this->tank_auth->get_user_id()
        );

        userlog($info);

        return true;
    }

    function user_delete_outcome_pay($primary_key) {
        $info = array(
            "title" => 'حذف مصاريف',
            "table" => 'pay',
            "table_id" => $primary_key,
            "user_id" => $this->tank_auth->get_user_id()
        );

        userlog($info);

        return true;
    }

    public function outcome_reson() {
        $crud = new grocery_CRUD();

        $crud->set_table('outcome_reson');
        $crud->set_subject('اسباب مصاريف');

        $crud->columns('id', 'name');

        $crud->display_as('id', 'ID');
        $crud->display_as('name', 'السبب');

        $crud->unset_delete();

        $crud->set_rules('name', 'السبب', 'trim|required|max_length[200]');

        $crud->callback_after_insert(array($this, 'user_insert_outcome_reson'));
        $crud->callback_after_update(array($this, 'user_update_outcome_reson'));

        $output = $crud->render();
        $this->load->view('crud', $output);
    }

    function user_insert_outcome_reson($post_array, $primary_key) {
        $info = array(
            "title" => 'اضافة اسباب مصاريف',
            "table" => 'outcome_reson',
            "table_id" => $primary_key,
            "user_id" => $this->tank_auth->get_user_id()
        );

        userlog($info);

        return true;
    }

    function user_update_outcome_reson($post_array, $primary_key) {
        $info = array(
            "title" => 'تعديل بيانات اسباب مصاريف',
            "table" => 'outcome_reson',
            "table_id" => $primary_key,
            "user_id" => $this->tank_auth->get_user_id()
        );

        userlog($info);

        return true;
    }

    public function income_reson() {

        $crud = new grocery_CRUD();

        $crud->set_table('income_reson');
        $crud->set_subject('اسباب واردات');

        $crud->columns('id', 'name');

        $crud->display_as('id', 'ID');
        $crud->display_as('name', 'السبب');

        $crud->unset_delete();

        $crud->set_rules('name', 'السبب', 'trim|required|max_length[200]');

        $crud->callback_after_insert(array($this, 'user_insert_income_reson'));
        $crud->callback_after_update(array($this, 'user_update_income_reson'));

        $output = $crud->render();
        $this->load->view('crud', $output);
    }

    function user_insert_income_reson($post_array, $primary_key) {
        $info = array(
            "title" => 'اضافة اسباب واردات',
            "table" => 'income_reson',
            "table_id" => $primary_key,
            "user_id" => $this->tank_auth->get_user_id()
        );

        userlog($info);

        return true;
    }

    function user_update_income_reson($post_array, $primary_key) {
        $info = array(
            "title" => 'تعديل بيانات اسباب واردات',
            "table" => 'income_reson',
            "table_id" => $primary_key,
            "user_id" => $this->tank_auth->get_user_id()
        );

        userlog($info);

        return true;
    }

    //call back...............

    public function add_date_user_id($post_array) {
        $post_array['add_user_id'] = $this->session->userdata('user_id');
        $post_array['edit_user_id'] = 0;
        $post_array['edit_date_time'] = 0;
        return $post_array;
    }

    public function update_date_user_id($post_array) {
        $post_array['edit_user_id'] = $this->session->userdata('user_id');
        $post_array['edit_date_time'] = date('Y-m-d h:i:seg');

        return $post_array;
    }

}
