<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Invoice extends MY_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->helper('download');

        //ALTER TABLE `pay` ADD `invoice_id` INT NOT NULL DEFAULT '0' AFTER `client_id`;
        //غير الدات بيكر خليها بتاعت جى كويرى
    }

    function ajaxGetProduct() {
        $product = get_row('supplier_invoice_product', array('barcode' => $_GET['barcode']));
        if ($product == NULL)
            echo json_encode($data = array('status' => 2, 'msg' => 'المنتج غير موجود', 'color' => 'red'));
        else if ($product->sold == 1)
            echo json_encode($data = array('status' => 2, 'msg' => 'المنتج مباع من قبل', 'color' => 'red'));
        else
            echo json_encode($data = array('status' => 1, 'product_id' => $product->product_id, 'price' => $product->price, 'msg' => 'المنتج متاح', 'color' => 'green'));
    }

    function ajaxAddClient() {
        $info = array(
            'name' => $_GET['name'],
            'mobile' => $_GET['mobile'],
            'address' => $_GET['address']
        );
        $add = $this->General_model->add('client', $info);
        if ($add) {
            $client_id = $this->db->insert_id();
            echo json_encode($data = array('status' => 1, 'client_id' => $client_id, 'msg' => 'تمت اضافة العميل', 'color' => 'green'));
        } else {
            echo json_encode($data = array('status' => 1, 'msg' => 'خطا اثناء الاضافة', 'color' => 'red'));
        }
    }

    function ajaxCheck_number() {
        $mobile = $_GET['mobile'];
        $client = get_row('client', array('mobile' => $mobile));
        if ($client != null)
            $data = array('status' => 1, 'client_id' => $client->id, 'msg' => 'الرقم موجود', 'color' => 'green');
        else
            $data = array('status' => 2, 'msg' => 'الرقم غير موجود', 'color' => 'red');

        echo json_encode($data);
    }
    
    
      function add_halk() {
       

        if ($this->input->post()) {

            $client = 1;
            $barcode = $this->input->post('barcode');
            $product_id = $this->input->post('product_id');
            
           
 


            if (empty($product_id) || $product_id[0] == 0  ) {
                $data['error_msg'] = 'خطا لم تختار المنتجات ';
            }  else {

                $invoice = array(
                    'client_id' => $client,
                    'halk' => 1,
                    'add_user_id' => $this->tank_auth->get_user_id()
                );

                $this->db->insert('client_invoice', $invoice);         // اضافة الفاتورة
                $invoice_id = $this->db->insert_id();   //  الفاتورة المضافة  id       

                if ($product_id != null) {
                    $barcode_array = [];          // اراى لتجميع الباركود للطباعه
                    for ($i = 0; $i < count($product_id); $i++) {      // لوب على المنتجا
                        if ($product_id[$i] == null || $product_id[$i] == '' || $product_id[$i] == 0)
                            continue;

                        $inv_pro['client_invoice_id'] = $invoice_id;
                        $inv_pro['product_id'] = $product_id[$i];
                        $inv_pro['barcode'] = $barcode[$i];
                        $inv_pro['price'] = 0;
                        $inv_pro['halk'] = 1;
                        $this->db->insert('client_invoice_product', $inv_pro);                  // ادخال بيانات المنتج
                        //خلى المنتج مباع فى جدول منتجات الموردين 
                        $this->General_model->update_where('supplier_invoice_product', array('barcode' => $barcode[$i]), array('sold' => 1));
                    }


              
                    $data['msg'] = 'تم الحفظ  بنجاح';
                }
            }
        }

        $data['category'] = $this->General_model->get('category');

        view('add_halk', $data);
    }


    function addClientInvoice() {
        $data['client'] = $this->General_model->get('client');

        if ($this->input->post()) {

            $client = $this->input->post('client_id');
            $barcode = $this->input->post('barcode');
            $product_id = $this->input->post('product_id');
            $price = $this->input->post('price');
            $location = $this->input->post('location');
            $plus = $this->input->post('plus');
            $plus_note = $this->input->post('plus_note');
            $minus = $this->input->post('minus');
            $minus_note = $this->input->post('minus_note');
            $date = $this->input->post('date');


// المرتجعات
            $back_product = $this->input->post('back_product');
            $back_count = $this->input->post('back_count');
            $back_coast = $this->input->post('back_coast');
            $back_price = $this->input->post('back_price');




            if (empty($product_id) || $product_id[0] == 0  ) {
                $data['error_msg'] = 'خطا لم تختار المنتجات ';
            } elseif( empty($price) || $price[0] == 0){
                    $data['error_msg'] = 'خطا لم تكتب الاسعار ';
            } else {

                $invoice = array(
                    'client_id' => $client,
                    'plus' => $plus,
                    'plus_note' => $plus_note,
                    'minus' => $minus,
                    'minus_note' => $minus_note,
                    'date' => $date,
                    'location' => $location,
                    'add_user_id' => $this->tank_auth->get_user_id()
                );

                $this->db->insert('client_invoice', $invoice);         // اضافة الفاتورة
                $invoice_id = $this->db->insert_id();   //  الفاتورة المضافة  id       

                if ($product_id != null) {
                    $barcode_array = [];          // اراى لتجميع الباركود للطباعه
                    for ($i = 0; $i < count($product_id); $i++) {      // لوب على المنتجا
                        if ($product_id[$i] == null || $product_id[$i] == '' || $product_id[$i] == 0)
                            continue;

                        $inv_pro['client_invoice_id'] = $invoice_id;
                        $inv_pro['product_id'] = $product_id[$i];
                        $inv_pro['barcode'] = $barcode[$i];
                        $inv_pro['price'] = $price[$i];
                        $this->db->insert('client_invoice_product', $inv_pro);                  // ادخال بيانات المنتج
                        //خلى المنتج مباع فى جدول منتجات الموردين 
                        $this->General_model->update_where('supplier_invoice_product', array('barcode' => $barcode[$i]), array('sold' => 1));
                    }


                    // المرتجعات 
                    if ($back_product != null && !empty($back_product) && $back_product[0] != 0) {
                        $d = new DateTime($date);
                        $d->modify('+5 Second');
                        $backDate = $d->format('Y-m-d H:i:s');
                        $back_invoice = array(
                            'client_id' => $client,
                            'type' => 'client_back',
                            'date' => $backDate,
                            'client_invoice_id' => $invoice_id,
                            'add_user_id' => $this->tank_auth->get_user_id()
                        );

                        $this->db->insert('supplier_invoice', $back_invoice);         //  اضافة فاتورة المرتجع
                        $back_invoice_id = $this->db->insert_id();   //  الفاتورة المضافة  id       

                        $barcode_array = [];
                        for ($i = 0; $i < count($back_product); $i++) {      // لوب على المنتجا
                            for ($x = 1; $x <= $back_count[$i]; $x++) {          // لوب على عدد كل منتج
                                $back_inv_pro['supplier_invoice_id'] = $back_invoice_id;
                                $back_inv_pro['product_id'] = $back_product[$i];
                                $back_inv_pro['coast'] = $back_coast[$i];
                                $back_inv_pro['price'] = $back_price[$i];
                                $back_inv_pro['barcode'] = create_barcode($back_invoice_id, $back_product[$i], $x);      // انشاء الباركود
                                $this->db->insert('supplier_invoice_product', $back_inv_pro);                  // ادخال بيانات المنتج
                                array_push($barcode_array, $back_inv_pro['barcode'] . '.png');           // حفظ الباركود للطباعه
                            }
                        }
                        if ($_POST['print'] == 'yes') {   // لو عاوز يحمل الباركود
                            $ar = http_build_query($barcode_array);                              // تحويل الاراى لسترينج لارساله لفانكشن التحميل
                            echo '<script type="text/javascript" language="javascript"> window.open("' . base_url() . "invoice/zip/invoice=" . $back_invoice_id . "&" . $ar . '"); </script>';
                        }
                        // اضافه رقم فاتورة المرتجع الى فاتورة العميل
                        $this->db->where('id', $invoice_id)->update('client_invoice', array('supplier_invoice_id' => $back_invoice_id));
                    }



                    // الدفع 
                    if ($_POST['paid'] == 'yes') {
                        $d = new DateTime($date);
                        $d->modify('+5 Second');
                        $payDate = $d->format('Y-m-d H:i:s');
                        $this->General_model->add('pay', array(
                            'category' => 'client',
                            'client_id' => $client,
                            'invoice_id' => $invoice_id,
                            'price' => invoice_client_price($invoice_id),
                            'note' => 'كاش',
                            'date' => $payDate,
                            'staff_id' => $this->tank_auth->get_user_id(),
                            'add_user_id' => $this->tank_auth->get_user_id()
                        ));
                        $pay_id = $this->db->insert_id();
                        //اضافة رقم الدفعه الى فاتورة العميل
                        $this->db->where('id', $invoice_id)->update('client_invoice', array('pay_id' => $pay_id));
                    }
                  //   echo '<script type="text/javascript" language="javascript"> window.open("' . base_url() . '"invoice/clinetShowInvoice/'.$invoice_id.'"); </script>';
                   // $this->clinetShowInvoice($invoice_id);   
                   redirect(base_url().'invoice/clinetShowInvoice/'.$invoice_id,'blank');

                    $data['msg'] = 'تم الحفظ  بنجاح';
                }
            }
        }

        $data['category'] = $this->General_model->get('category');

        view('addClientInvoice', $data);
    }

    function editClientInvoice($invoice_id = 0) {


        $data['client'] = $this->General_model->get('client');
        $pay = get_row('pay', ['invoice_id' => $invoice_id, 'category' => 'client']);



        if ($this->input->post()) {

            $invoice_id = $this->input->post('id');
            $client = $this->input->post('client_id');
            $barcode = $this->input->post('barcode');
            $product_id = $this->input->post('product_id');
            $price = $this->input->post('price');
            $location = $this->input->post('location');
            $plus = $this->input->post('plus');
            $plus_note = $this->input->post('plus_note');
            $minus = $this->input->post('minus');
            $minus_note = $this->input->post('minus_note');
            $date = $this->input->post('date');
            $staff = $this->input->post('staff_id');

// المرتجعات
            $back_product = $this->input->post('back_product');
            $back_count = $this->input->post('back_count');
            $back_coast = $this->input->post('back_coast');
            $back_price = $this->input->post('back_price');



            if (empty($product_id) || $product_id[0] == 0) {
                $data['error_msg'] = 'خطا لم تختار المنتجات ';
            } else {


                $current_invoice = get_row('client_invoice', array('id' => $invoice_id));

                $invoice_pro = $this->General_model->get('client_invoice_product', array('client_invoice_id' => $invoice_id));
                $bar_pro = [];
                foreach ($invoice_pro as $row) {
                    array_push($bar_pro, "'" . $row->barcode . "'");
                }

                // رجع المنتجات غير مباعه فى جدول منتجات الموردين
                $this->db->query("UPDATE `supplier_invoice_product` SET `sold` = 0 WHERE `barcode` IN (" . implode(',', $bar_pro) . ")");
                // lastq();
                $this->General_model->delete_where('client_invoice_product', array('client_invoice_id' => $invoice_id));  // مسج المنتجات القديمه

                $invoice = array(
                    'client_id' => $client,
                    'plus' => $plus,
                    'plus_note' => $plus_note,
                    'minus' => $minus,
                    'minus_note' => $minus_note,
                    'date' => $date,
                    'location' => $location,
                    'edit_date_time' => date('Y-m-d H:i:s'),
                    'edit_user_id' => $this->tank_auth->get_user_id()
                );

                $this->General_model->update_where('client_invoice', array('id' => $invoice_id), $invoice);         // تعديل الفاتورة

                if ($product_id != null) {
                    $barcode_array = [];
                    for ($i = 0; $i < count($product_id); $i++) {      // لوب على المنتجا
                        if ($product_id[$i] == null || $product_id[$i] == '' || $product_id[$i] == 0)
                            continue;
                        $inv_pro['client_invoice_id'] = $invoice_id;
                        $inv_pro['product_id'] = $product_id[$i];
                        $inv_pro['barcode'] = $barcode[$i];
                        $inv_pro['price'] = $price[$i];
                        $this->db->insert('client_invoice_product', $inv_pro);                  // ادخال بيانات المنتج
                        //خلى المنتج مباع فى جدول منتجات الموردين 
                        $this->General_model->update_where('supplier_invoice_product', array('barcode' => $barcode[$i]), array('sold' => 1));
                    }



                    // المرتجعات 
                    if ($current_invoice->supplier_invoice_id != 0) {
                        $this->General_model->delete_where('supplier_invoice', array('id' => $current_invoice->supplier_invoice_id));
                        $this->General_model->delete_where('supplier_invoice_product', array('supplier_invoice_id' => $current_invoice->supplier_invoice_id));
                    }
                    if ($back_product != null && !empty($back_product) && $back_product[0] != 0) {
                        $d = new DateTime($date);
                        $d->modify('+5 Second');
                        $backDate = $d->format('Y-m-d H:i:s');
                        $back_invoice = array(
                            'client_id' => $client,
                            'type' => 'client_back',
                            'date' => $backDate,
                            'client_invoice_id' => $invoice_id,
                            'add_user_id' => $this->tank_auth->get_user_id()
                        );

                        $this->db->insert('supplier_invoice', $back_invoice);         //  اضافة فاتورة المرتجع
                        $back_invoice_id = $this->db->insert_id();   //  الفاتورة المضافة  id       

                        $barcode_array = [];
                        for ($i = 0; $i < count($back_product); $i++) {      // لوب على المنتجا
                            for ($x = 1; $x <= $back_count[$i]; $x++) {          // لوب على عدد كل منتج
                                $back_inv_pro['supplier_invoice_id'] = $back_invoice_id;
                                $back_inv_pro['product_id'] = $back_product[$i];
                                $back_inv_pro['coast'] = $back_coast[$i];
                                $back_inv_pro['price'] = $back_price[$i];
                                $back_inv_pro['barcode'] = create_barcode($back_invoice_id, $back_product[$i], $x);      // انشاء الباركود
                                $this->db->insert('supplier_invoice_product', $back_inv_pro);                  // ادخال بيانات المنتج
                                array_push($barcode_array, $back_inv_pro['barcode'] . '.png');           // حفظ الباركود للطباعه
                            }
                        }
                        if ($_POST['print'] == 'yes') {   // لو عاوز يحمل الباركود
                            $ar = http_build_query($barcode_array);                              // تحويل الاراى لسترينج لارساله لفانكشن التحميل
                            echo '<script type="text/javascript" language="javascript"> window.open("' . base_url() . "invoice/zip/invoice=" . $back_invoice_id . "&" . $ar . '"); </script>';
                        }
                        // اضافه رقم فاتورة المرتجع الى فاتورة العميل
                        $this->db->where('id', $invoice_id)->update('client_invoice', array('supplier_invoice_id' => $back_invoice_id));
                    }





                   
                      // الدفع 
                    if($current_invoice->pay_id != 0)   // لو فى دفعه يمسحها 
                        $this->db->where('id',$current_invoice->pay_id)->delete('pay');
                    
                    $this->db->where('id', $invoice_id)->update('client_invoice', array('pay_id' => 0));
                    
                    
                    if ($_POST['paid'] == 'yes') {
                        $d = new DateTime($date);
                        $d->modify('+5 Second');
                        $payDate = $d->format('Y-m-d H:i:s');
                        $this->General_model->add('pay', array(
                            'category' => 'client',
                            'client_id' => $client,
                            'invoice_id' => $invoice_id,
                            'price' => invoice_client_price($invoice_id),
                            'note' => 'كاش',
                            'date' => $payDate,
                            'staff_id' => $this->tank_auth->get_user_id(),
                            'add_user_id' => $this->tank_auth->get_user_id()
                        ));
                        $pay_id = $this->db->insert_id();
                        //اضافة رقم الدفعه الى فاتورة العميل
                        $this->db->where('id', $invoice_id)->update('client_invoice', array('pay_id' => $pay_id));
                    }
                   
                 


                    $data['msg'] = 'تم الحفظ  بنجاح';
                }
            }
        }

        if ($invoice_id == 0)
            redirect(base_url() . '404');

        $data['invoice'] = get_row('client_invoice', array('id' => $invoice_id));
        if ($data['invoice'] == null)
            redirect(base_url() . '404');

        $data['invoice_product'] = $this->General_model->get('client_invoice_product', array('client_invoice_id' => $invoice_id));

// فاتورة المرتجع
        if ($data['invoice']->supplier_invoice_id != 0) {
            $supplier_invoice_id = $data['invoice']->supplier_invoice_id;
            $data['back_invoice'] = get_row('supplier_invoice', array('id' => $supplier_invoice_id));
            $data['back_invoice_product'] = $this->db->query(" SELECT supplier_invoice_product.*,count(`product_id`) as count from supplier_invoice_product where supplier_invoice_id = $supplier_invoice_id group by `product_id` ")->result();

            // $this->General_model->get('supplier_invoice_product', array('supplier_invoice_id' => $data['invoice']->supplier_invoice_id));
        }
        $data['category'] = $this->General_model->get('category');
        view('editClientInvoice', $data);
    }

    function addSupplierInvoice() {
        $data['supplier'] = $this->General_model->get('supplier');
        /* $data['product'] =   query('SELECT `product`.*,`category`.`name` as  `category` 
          from product
          LEFT JOIN `category` ON `product`.`category_id` = `category`.`id`

          '); */
        $data['category'] = $this->General_model->get('category');
        $data['staff'] = $this->General_model->get('staff');



        if ($this->input->post()) {

            $supplier = $this->input->post('supplier_id');
            $product = $this->input->post('product');
            $count = $this->input->post('count');
            $price = $this->input->post('price');
            $coast = $this->input->post('coast');
            $pro_plus = $this->input->post('pro_plus');
            $pro_plus_note = $this->input->post('pro_plus_note');
            $plus = $this->input->post('plus');
            $plus_note = $this->input->post('plus_note');
            $minus = $this->input->post('minus');
            $minus_note = $this->input->post('minus_note');
            $date = $this->input->post('date');
            $staff = $this->input->post('staff_id');




            if (empty($count) || $count[0] == 0 || empty($product) || $product[0] == 0) {
                $data['error_msg'] = 'خطا لم تختار المنتجات او الكميات';
            } else {



                $invoice = array(
                    'supplier_id' => $supplier,
                    'plus' => $plus,
                    'plus_note' => $plus_note,
                    'minus' => $minus,
                    'minus_note' => $minus_note,
                    'date' => $date,
                    'staff_id' => $staff,
                    'add_user_id' => $this->tank_auth->get_user_id()
                );
               
                if ($_FILES['file']) {
                    $file = $this->do_upload('file');
                    if ($file != false)
                        $invoice['file'] = $file;
                }


                $this->db->insert('supplier_invoice', $invoice);         // اضافة الفاتورة
                $invoice_id = $this->db->insert_id();   //  الفاتورة المضافة  id       
                if ($product != null) {
                    $barcode_array = [];
                    for ($i = 0; $i < count($product); $i++) {      // لوب على المنتجا
                        for ($x = 1; $x <= $count[$i]; $x++) {          // لوب على عدد كل منتج
                            $inv_pro['supplier_invoice_id'] = $invoice_id;
                            $inv_pro['product_id'] = $product[$i];
                            $inv_pro['coast'] = $coast[$i];
                            $inv_pro['price'] = $price[$i];
                            $inv_pro['plus'] = $pro_plus[$i];
                            $inv_pro['plus_note'] = $pro_plus_note[$i];
                            if($_POST['show_price'] == 'yes')
                            $inv_pro['barcode'] = create_barcode($invoice_id, $product[$i], $x,$price[$i]);      // انشاء الباركود
                           else
                                $inv_pro['barcode'] = create_barcode($invoice_id, $product[$i], $x);      // انشاء الباركود
                            $this->db->insert('supplier_invoice_product', $inv_pro);                  // ادخال بيانات المنتج
                            array_push($barcode_array, $inv_pro['barcode'] . '.png');           // حفظ الباركود للطباعه
                        }
                    }
                    if ($_POST['print'] == 'yes') {   // لو عاوز يحمل الباركود
                        $ar = http_build_query($barcode_array);                              // تحويل الاراى لسترينج لارساله لفانكشن التحميل
                        echo '<script type="text/javascript" language="javascript"> window.open("' . base_url() . "invoice/zip/invoice=" . $invoice_id . "&" . $ar . '"); </script>';
                    }

                    // الدفع 
                    if ($_POST['paid'] == 'yes') {

                        $this->General_model->add('pay', array(
                            'category' => 'supplier',
                            'supplier_id' => $supplier,
                            'invoice_id' => $invoice_id,
                            'price' => invoice_supplier_price($invoice_id),
                            'note' => 'كاش',
                            'date' => $date,
                            'staff_id' => $staff,
                            'add_user_id' => $this->tank_auth->get_user_id()
                        ));
                        $pay_id = $this->db->insert_id();
                        //اضافة رقم الدفعه الى فاتورة العميل
                        $this->db->where('id', $invoice_id)->update('supplier_invoice', array('pay_id' => $pay_id));
                    }





                    $data['msg'] = 'تم الحفظ  بنجاح';
                }
            }
        }

        view('addSupplierInvoice', $data);
    }

    function addBackInvoice() {
        $data['client'] = $this->General_model->get('client');

        $data['category'] = $this->General_model->get('category');
        $data['staff'] = $this->General_model->get('staff');



        if ($this->input->post()) {

            $client = $this->input->post('client_id');
            $product = $this->input->post('product');
            $count = $this->input->post('count');
            $price = $this->input->post('price');
            $coast = $this->input->post('coast');
            $date = $this->input->post('date');
            $staff = $this->input->post('staff_id');




            if (empty($count) || $count[0] == 0 || empty($product) || $product[0] == 0) {
                $data['error_msg'] = 'خطا لم تختار المنتجات او الكميات';
            } else {



                $invoice = array(
                    'client_id' => $client,
                    'type' => 'client_back',
                    'date' => $date,
                    'staff_id' => $staff,
                    'add_user_id' => $this->tank_auth->get_user_id()
                );


                $this->db->insert('supplier_invoice', $invoice);         // اضافة الفاتورة
                $invoice_id = $this->db->insert_id();   //  الفاتورة المضافة  id       
                if ($product != null) {
                    $barcode_array = [];
                    for ($i = 0; $i < count($product); $i++) {      // لوب على المنتجا
                        for ($x = 1; $x <= $count[$i]; $x++) {          // لوب على عدد كل منتج
                            $inv_pro['supplier_invoice_id'] = $invoice_id;
                            $inv_pro['product_id'] = $product[$i];
                            $inv_pro['coast'] = $coast[$i];
                            $inv_pro['price'] = $price[$i];
                            $inv_pro['barcode'] = create_barcode($invoice_id, $product[$i], $x);      // انشاء الباركود
                            $this->db->insert('supplier_invoice_product', $inv_pro);                  // ادخال بيانات المنتج
                            array_push($barcode_array, $inv_pro['barcode'] . '.png');           // حفظ الباركود للطباعه
                        }
                    }
                    if ($_POST['print'] == 'yes') {   // لو عاوز يحمل الباركود
                        $ar = http_build_query($barcode_array);                              // تحويل الاراى لسترينج لارساله لفانكشن التحميل
                        echo '<script type="text/javascript" language="javascript"> window.open("' . base_url() . "invoice/zip/invoice=" . $invoice_id . "&" . $ar . '"); </script>';
                    }


                    $data['msg'] = 'تم الحفظ  بنجاح';
                }
            }
        }

        view('addBackInvoice', $data);
    }

    function editSupplierInvoice($invoice_id = 0) {

        if ($this->input->post()) {

            $invoice_id = $this->input->post('id');
            $supplier = $this->input->post('supplier_id');
            $product = $this->input->post('product');
            $count = $this->input->post('count');
            $price = $this->input->post('price');
            $coast = $this->input->post('coast');
            $pro_plus = $this->input->post('pro_plus');
            $pro_plus_note = $this->input->post('pro_plus_note');
            $plus = $this->input->post('plus');
            $plus_note = $this->input->post('plus_note');
            $minus = $this->input->post('minus');
            $minus_note = $this->input->post('minus_note');
            $date = $this->input->post('date');
            $staff = $this->input->post('staff_id');

            $old_invoice = get_row('supplier_invoice', array('id' => $invoice_id));


            if (empty($count) || $count[0] == 0 || empty($product) || $product[0] == 0) {
                $data['error_msg'] = 'خطا لم تختار المنتجات او الكميات';
            } else {



                $invoice = array(
                    'supplier_id' => $supplier,
                    'plus' => $plus,
                    'plus_note' => $plus_note,
                    'minus' => $minus,
                    'minus_note' => $minus_note,
                    'date' => $date,
                    'staff_id' => $staff,
                    'edit_date_time' => date('Y-m-d H:i:s'),
                    'edit_user_id' => $this->tank_auth->get_user_id()
                );

                if ($_FILES['file']) {
                    $file = $this->do_upload('file');
                    if ($file != false) {
                    
                    $filename = 'assets/uploads/files/' . $old_invoice->file;    
                    if(is_file($filename))
                    unlink('assets/uploads/files/' . $old_invoice->file); //امسح الصوره القديمه 
                        
                    $invoice['file'] = $file;
                    }
                }


                $this->General_model->update_where('supplier_invoice', array('id' => $invoice_id), $invoice);         // تعديل الفاتورة

                $this->General_model->delete_where('supplier_invoice_product', array('supplier_invoice_id' => $invoice_id));  // مسج المنتجات القديمه

                if ($product != null) {
                    $barcode_array = [];
                    for ($i = 0; $i < count($product); $i++) {      // لوب على المنتجات
                        for ($x = 1; $x <= $count[$i]; $x++) {          // لوب على عدد كل منتج
                            $inv_pro['supplier_invoice_id'] = $invoice_id;
                            $inv_pro['product_id'] = $product[$i];
                            $inv_pro['coast'] = $coast[$i];
                            $inv_pro['price'] = $price[$i];
                            $inv_pro['plus'] = $pro_plus[$i];
                            $inv_pro['plus_note'] = $pro_plus_note[$i];
                            //$inv_pro['barcode'] = create_barcode($invoice_id, $product[$i], $x);      // انشاء الباركود
                            if($_POST['show_price'] == 'yes')
                            $inv_pro['barcode'] = create_barcode($invoice_id, $product[$i], $x,$price[$i]);      // انشاء الباركود
                           else
                                $inv_pro['barcode'] = create_barcode($invoice_id, $product[$i], $x);      // انشاء الباركود

                            $this->db->insert('supplier_invoice_product', $inv_pro);                  // ادخال بيانات المنتج
                            array_push($barcode_array, $inv_pro['barcode'] . '.png');           // حفظ الباركود للطباعه
                        }
                    }
                    if ($_POST['print'] == 'yes') {   // لو عاوز يحمل الباركود
                        $ar = http_build_query($barcode_array);                              // تحويل الاراى لسترينج لارساله لفانكشن التحميل
                        echo '<script type="text/javascript" language="javascript"> window.open("' . base_url() . "invoice/zip/invoice=" . $invoice_id . "&" . $ar . '"); </script>';
                    }

                        // الدفع 
                    if($old_invoice->pay_id != 0)   // لو فى دفعه يمسحها 
                        $this->db->where('id',$old_invoice->pay_id)->delete('pay');
                    
                    $this->db->where('id', $invoice_id)->update('supplier_invoice', array('pay_id' => 0));
                    
                    
                    if ($_POST['paid'] == 'yes') {
                        $d = new DateTime($date);
                        $d->modify('+5 Second');
                        $payDate = $d->format('Y-m-d H:i:s');
                        $this->General_model->add('pay', array(
                            'category' => 'supplier',
                            'client_id' => $supplier,
                            'invoice_id' => $invoice_id,
                            'price' => invoice_supplier_price($invoice_id),
                            'note' => 'كاش',
                            'date' => $payDate,
                            'staff_id' => $this->tank_auth->get_user_id(),
                            'add_user_id' => $this->tank_auth->get_user_id()
                        ));
                        $pay_id = $this->db->insert_id();
                        //اضافة رقم الدفعه الى فاتورة المورد
                        $this->db->where('id', $invoice_id)->update('supplier_invoice', array('pay_id' => $pay_id));
                    }


                    $this->General_model->update_where('pay', ['invoice_id' => $invoice_id, 'category' => 'supplier'], array(
                        'supplier_id' => $supplier,
                        'invoice_id' => $invoice_id,
                        'price' => invoice_supplier_price($invoice_id),
                        'date' => $date,
                        'edit_date_time' => date('Y-m-d H:i:s'),
                        'edit_user_id' => $this->tank_auth->get_user_id()
                    ));





                    $data['msg'] = 'تم الحفظ  بنجاح';
                }
            }
        }
        if ($invoice_id == 0)
            redirect(base_url() . '404');

        $data['invoice'] = get_row('supplier_invoice', array('id' => $invoice_id));
        if ($data['invoice'] == null)
            redirect(base_url() . '404');



        $data['invoice_product'] = $this->db->query(" SELECT supplier_invoice_product.*,count(`product_id`) as count from supplier_invoice_product where supplier_invoice_id = $invoice_id group by `product_id` ")->result();
        $data['supplier'] = $this->General_model->get('supplier');
        $data['category'] = $this->General_model->get('category');
        $data['staff'] = $this->General_model->get('staff');


        view('editSupplierInvoice', $data);
    }

    function deleteSupplierInvoice($id) {
        if ($id == 0)
            redirect(base_url() . '404');
        // التاكد ان الفاتورة موجوده
        $invoice = get_row('supplier_invoice', array('id' => $id));
        if (invoice == null)
            redirect(base_url() . '404');

        if ($invoice->pay_id != 0)
            $delete_pay = $this->db->where('id', $invoice->pay_id)->delete('pay'); // حذف الدفعات
        $delete1 = $this->db->where('supplier_invoice_id', $id)->delete('supplier_invoice_product');  // حذف المنتجات
        $delete2 = $this->db->where('id', $id)->delete('supplier_invoice');            // حذف الفاتورة

        if ($delete1 && $delete2)
            redirect(base_url() . 'invoice/SupplierInvoice?msg=تم الحذف');
        else
            redirect(base_url() . 'invoice/SupplierInvoice?errot_msg=خطا اثناء الحذف');
    }

    function deleteClientInvoice($id) {
        if ($id == 0)
            redirect(base_url() . '404');

        // التاكد ان الفاتورة موجوده
        $invoice = get_row('client_invoice', array('id' => $id));
        if (invoice == null)
            redirect(base_url() . '404');

        if ($invoice->pay_id != 0)
            $delete_pay = $this->db->where('id', $invoice->pay_id)->delete('pay'); // حذف الدفعات

        $delete1 = $this->db->where('client_invoice_id', $id)->delete('client_invoice_product');
        $delete2 = $this->db->where('id', $id)->delete('client_invoice');
        if ($delete1 && $delete2)
            redirect(base_url() . 'invoice/clientInvoice?msg=تم الحذف');
        else
            redirect(base_url() . 'invoice/clientInvoice?errot_msg=خطا اثناء الحذف');
    }
    
     function download_barcode($invoice_id) {
         $product = table('supplier_invoice_product',['supplier_invoice_id'=>$invoice_id]);
         
         if($product == null)
             redirect (base_url());
         $arr_barcode=[];
         foreach ($product as $value) {
            // print_r($value);die;
              array_push($arr_barcode, $value->barcode . '.png');           // حفظ الباركود للطباعه
         }
        
        if ($arr_barcode != null) {
            $zip = new ZipArchive();
            $zip->open('zip/barcode.zip', ZIPARCHIVE::OVERWRITE);
             
            $invoice = $arr_barcode['invoice'];
            unset($arr_barcode['invoice']);
            for ($i = 0; $i < count($arr_barcode); $i++) {
               // echo $arr_barcode[$i];
                $zip->addFile('barcode/' . $arr_barcode[$i], $arr_barcode[$i]);
            }
 
            $zip->close();
            $file = base_url() . 'zip/barcode.zip';
            header("Content-Description: File Transfer");
            header("Content-Type: application/octet-stream");

            header("Content-Disposition: attachment; filename='" . basename($file) . "'");

            readfile($file);
            closedir();
        }
    }

    function zip($output) {

        parse_str($output, $arr_barcode);
        // print_r(  $output);die;

        if ($arr_barcode != null) {
            $zip = new ZipArchive();
            $zip->open('zip/barcode.zip', ZIPARCHIVE::OVERWRITE);
            //   print_r($arr_barcode);die;
            $invoice = $arr_barcode['invoice'];
            unset($arr_barcode['invoice']);
            for ($i = 0; $i < count($arr_barcode); $i++) {
                //echo $arr_barcode[$i];
                $zip->addFile('barcode/' . $arr_barcode[$i], $arr_barcode[$i]);
            }

            $zip->close();
            $file = base_url() . 'zip/barcode.zip';
            header("Content-Description: File Transfer");
            header("Content-Type: application/octet-stream");

            header("Content-Disposition: attachment; filename='" . basename($file) . "'");

            readfile($file);
            closedir();
        }
    }

// عرض الفواتير    

    
    
    public function view_one_halk($invoice_id) {

        // فاتورة العميل
        $invoice = query(" SELECT client_invoice.*,client.name as client_name,
            users1.username as add_by,users2.username as edit_by,
            client.mobile as mobile,client.address as address,client.date_time as date_time
            from client_invoice
           LEFT JOIN client ON client_invoice.client_id = client.id
             LEFT JOIN users as users1 ON users1.id = client_invoice.add_user_id
            LEFT JOIN users as users2 ON users2.id = client_invoice.edit_user_id
           where client_invoice.id = $invoice_id
");

        if ($invoice == null)
            redirect(base_url());
        $data['invoice'] = $invoice[0];

        // منتجات فاتورة العميل
        $data['product'] = query(" SELECT client_invoice_product.*,
               product.name as name,product.core as core,
               product.hard as hard,product.ram as ram,product.viga as viga
               from client_invoice_product
               LEFT JOIN product ON client_invoice_product.product_id = product.id
where client_invoice_id = $invoice_id ");

 
        view('view_one_halk', $data);
    }

    
     public function show_halk() { 
        $this->load->model('client_invoice_mod');
        $data = array('');
        $this->db->where('halk',1);
        $invoice = $this->client_invoice_mod->get_all();
        if ($invoice != NULL) {
            $data['invoice'] = $invoice;
            $data['client_name'] = function($c_id) {
                return $this->client_invoice_mod->get_client_name_by_id($c_id);
            };
        }

        $this->load->view('show_halk', $data);
    }


    public function clientInvoice() {  // فواتير العملاء
        $this->load->model('client_invoice_mod');
        $data = array('');
        $this->db->where('halk',0);
        $invoice = $this->client_invoice_mod->get_all();
        if ($invoice != NULL) {
            $data['invoice'] = $invoice;
            $data['client_name'] = function($c_id) {
                return $this->client_invoice_mod->get_client_name_by_id($c_id);
            };
        }

        $this->load->view('showClient_invoice', $data);
    }

    public function SupplierInvoice() {   // فواتير الموردين
        $this->load->model('supplier_invoice_mod');
        $data = array('');
        $invoice = $this->supplier_invoice_mod->get_all($option = array('col' => 'type', 'val' => 'supplier'));
        if ($invoice != NULL) {
            $data['invoice'] = $invoice;
            $data['supplier_name'] = function($s_id) {
                return $this->supplier_invoice_mod->get_supplier_name_by_id($s_id);
            };
        }

        $this->load->view('showSupplier_invoice', $data);
    }

    public function backShowInvoice($invoice_id) {
        $invoice = query(" SELECT supplier_invoice.*,client.name as client_name,
            client.mobile as mobile,client.address as address,client.date_time as date_time
            from supplier_invoice
           LEFT JOIN client ON supplier_invoice.client_id = client.id
           where supplier_invoice.id = $invoice_id
");

        if ($invoice == null)
            redirect(base_url());
        $data['invoice'] = $invoice[0];
        $data['product'] = query(" SELECT supplier_invoice_product.*,
               count(`product_id`) as count,
               product.name as name,product.core as core,
               product.hard as hard,product.ram as ram,product.viga as viga
               from supplier_invoice_product
               LEFT JOIN product ON supplier_invoice_product.product_id = product.id
where supplier_invoice_id = $invoice_id group by `product_id`");

        view('backShowInvoice', $data);
    }

    public function supplierShowInvoice($invoice_id) {
        $invoice = query(" SELECT supplier_invoice.*,supplier.name as supplier_name,
            users1.username as add_by,users2.username as edit_by,
            supplier.mobile as mobile,supplier.address as address,supplier.date_time as date_time
            from supplier_invoice
            LEFT JOIN users as users1 ON users1.id = supplier_invoice.add_user_id
            LEFT JOIN users as users2 ON users2.id = supplier_invoice.edit_user_id
           LEFT JOIN supplier ON supplier_invoice.supplier_id = supplier.id
           where supplier_invoice.id = $invoice_id
");

        if ($invoice == null)
            redirect(base_url());
        $data['invoice'] = $invoice[0];
        $data['product'] = query(" SELECT supplier_invoice_product.*,
               count(`product_id`) as count,
               product.tiny_name as name,product.core as core,
               product.hard as hard,product.ram as ram,product.viga as viga
               from supplier_invoice_product
               LEFT JOIN product ON supplier_invoice_product.product_id = product.id
where supplier_invoice_id = $invoice_id group by `product_id`");

        view('supplierShowInvoice', $data);
    }

    public function supplierShowInvoice2($invoice_id) {
        $invoice = query(" SELECT supplier_invoice.*,supplier.name as supplier_name,supplier.ini_credit,
            users1.username as add_by,users2.username as edit_by,
            supplier.mobile as mobile,supplier.address as address,supplier.date_time as date_time
            from supplier_invoice
            LEFT JOIN users as users1 ON users1.id = supplier_invoice.add_user_id
            LEFT JOIN users as users2 ON users2.id = supplier_invoice.edit_user_id
           LEFT JOIN supplier ON supplier_invoice.supplier_id = supplier.id
           where supplier_invoice.id = $invoice_id
");

        if ($invoice == null)
            redirect(base_url());
        
        
        //------------------------------------------------
        $total = $invoice[0]->ini_credit;
        $invoices = table('supplier_invoice', ['supplier_id' => $invoice[0]->supplier_id]);        // الفواتير
        if ($invoices != null) {
            foreach ($invoices as $row) {
                $total += invoice_supplier_price($row->id);         // بيجمع اسعار الفواتير
            }
        }
        
        $total -= pay_supplier($invoice[0]->supplier_id);
        if ($total > 0)
            $text = 'ليه';
        else
            $text = 'عليه';
        if ($total == 0)
            $text = '';
        $total = abs($total);
        
        $invoice[0]->total = $total;        
        //print_r($invoice);die;
        
        //---------------------------------------------------------------
        $data['invoice'] = $invoice[0];
        $data['product'] = query(" SELECT supplier_invoice_product.*,
               count(`product_id`) as count,
               product.tiny_name as name,product.core as core,
               product.hard as hard,product.ram as ram,product.viga as viga
               from supplier_invoice_product
               LEFT JOIN product ON supplier_invoice_product.product_id = product.id
where supplier_invoice_id = $invoice_id group by `product_id`");

        view('test_1', $data);
    }

    public function clinetShowInvoice($invoice_id) {

        // فاتورة العميل
        $invoice = query(" SELECT client_invoice.*,client.name as client_name,
            users1.username as add_by,users2.username as edit_by,
            client.mobile as mobile,client.address as address,client.date_time as date_time
            from client_invoice
           LEFT JOIN client ON client_invoice.client_id = client.id
             LEFT JOIN users as users1 ON users1.id = client_invoice.add_user_id
            LEFT JOIN users as users2 ON users2.id = client_invoice.edit_user_id
           where client_invoice.id = $invoice_id
");

        if ($invoice == null)
            redirect(base_url());
        $data['invoice'] = $invoice[0];

        // منتجات فاتورة العميل
        $data['product'] = query(" SELECT client_invoice_product.*,
               product.tiny_name as name,product.core as core,
               product.hard as hard,product.ram as ram,product.viga as viga
               from client_invoice_product
               LEFT JOIN product ON client_invoice_product.product_id = product.id
where client_invoice_id = $invoice_id ");


        if ($data['invoice']->supplier_invoice_id != 0) {   // لو فيه مرتجع
            $back_invoice_id = $data['invoice']->supplier_invoice_id;
            $back_product = query(" SELECT supplier_invoice_product.*,
               count(`product_id`) as count,
               product.name as name,product.core as core,
               product.hard as hard,product.ram as ram,product.viga as viga
               from supplier_invoice_product
               LEFT JOIN product ON supplier_invoice_product.product_id = product.id
where supplier_invoice_id = $back_invoice_id group by `product_id`");
            $data['back_product'] = $back_product;
        }
        //   print_r($data['back_product']);die;

        view('clientShowInvoice', $data);
    }

    
        public function clinetShowInvoice2($invoice_id) {

        // فاتورة العميل
        $invoice = query(" SELECT client_invoice.*,client.name as client_name,client.ini_credit,
            users1.username as add_by,users2.username as edit_by,
            client.mobile as mobile,client.address as address,client.date_time as date_time
            from client_invoice
           LEFT JOIN client ON client_invoice.client_id = client.id
             LEFT JOIN users as users1 ON users1.id = client_invoice.add_user_id
            LEFT JOIN users as users2 ON users2.id = client_invoice.edit_user_id
           where client_invoice.id = $invoice_id
");

        if ($invoice == null)
            redirect(base_url());
        $data['invoice'] = $invoice[0];

        // منتجات فاتورة العميل
        $data['product'] = query(" SELECT client_invoice_product.*,
               product.tiny_name as name,product.core as core,
               product.hard as hard,product.ram as ram,product.viga as viga
               from client_invoice_product
               LEFT JOIN product ON client_invoice_product.product_id = product.id
               LEFT JOIN supplier_invoice_product ON client_invoice_product.product_id = supplier_invoice_product.id
where client_invoice_id = $invoice_id ");

        //print_r($data);die;
        if ($data['invoice']->supplier_invoice_id != 0) {   // لو فيه مرتجع
            $back_invoice_id = $data['invoice']->supplier_invoice_id;
            $back_product = query(" SELECT supplier_invoice_product.*,
               count(`product_id`) as count,
               product.name as name,product.core as core,
               product.hard as hard,product.ram as ram,product.viga as viga
               from supplier_invoice_product
               LEFT JOIN product ON supplier_invoice_product.product_id = product.id
where supplier_invoice_id = $back_invoice_id group by `product_id`");
            $data['back_product'] = $back_product;
        }
        //   print_r($data['back_product']);die;
        
        
        
         //------------------------------------------------
        $total = $invoice[0]->ini_credit;
        $invoices = table('client_invoice', ['client_id' => $invoice[0]->client_id]);        // الفواتير
        if ($invoices != null) {
            foreach ($invoices as $row) {
                $total += invoice_client_price($row->id);         // بيجمع اسعار الفواتير
            }
        }
        
        $total -= pay_client($invoice[0]->client_id);
        if ($total > 0)
            $text = 'عليه';
        else
            $text = 'ليه';
        if ($total == 0)
            $text = '';
        $total = abs($total);
        
        $invoice[0]->total = $total;        
        //print_r($invoice);die;
        
        //---------------------------------------------------------------
        
        $data['invoice'] = $invoice[0];

        view('test_2', $data);
    }

    
    public function ClientbackInvoice() {    //فواتير المرتجع
        $this->load->model('supplier_invoice_mod');
        $data = array('');
        $invoice = $this->supplier_invoice_mod->get_all($option = array('col' => 'type', 'val' => 'client_back'));
        if ($invoice != NULL) {
            $data['invoice'] = $invoice;
            $data['client_name'] = function($c_id) {
                return $this->supplier_invoice_mod->get_client_name_by_id($c_id);
            };
        }

        $this->load->view('showClientback_invoice', $data);
    }

}




