<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Api extends public_Controller {

    function __construct() {
        parent::__construct();

        header("Access-Control-Allow-Origin:*");
        header("Access-Control-Allow-Credentials: true");
        header("Access-Control-Allow-Headers: origin, content-type, accept, Set-Cookie");
        header("Access-Control-Allow-Methods: GET, POST, OPTIONS");
        header('Access-Control-Max-Age: 166400');
        
        $this -> load -> model('home_model');

    }
	
	
function check(){
	$x = $_GET['x'];
	$y = $_GET['y'];
	$page = 3;
	$result = query("
	SELECT * FROM `glyphs_1260` WHERE `page_number` = $page AND `min_x` <= $x AND `max_x` >= $x AND `min_y` <= $y AND `max_y` >= $y  
	");
	//lastq();
	 print_r($result);die;
}

  function send_gest_order(){
     	
	  $data = $this->input->post();	
	 
	  unset($data['notes']);	
     // print_r($data);die;
        
        $this -> form_validation -> set_rules('name','الاسم','trim|required');
        $this -> form_validation -> set_rules('phone','رقم الهاتف','trim|required|alpha_numeric');
        $this -> form_validation -> set_rules('address','العنوان','trim|required|');
        $this -> form_validation -> set_rules('email','البريد الاكتروني','trim|required|valid_email|is_unique[member.email]');

        $this->form_validation->set_error_delimiters('<p>', '</p>');

        $errors = false;
        if ($this->form_validation->run() == FALSE)
        {
            $errors .= validation_errors();
        }
        else
        {
        	$data['member_group_id'] = 2;
        	
            $add = $this->gm->add('member',$data);
            
            if($add){
                	
                $member_id = $this->db->insert_id();
            	
				$this->gm->update_where('member',array('member_id'=>$member_id),array('username' =>'guest'.$member_id));
				
                $this->gm->add('member_profiles',['member_id' => $member_id]);
              
			     $member = get_row('member',['member.member_id' => $member_id],'member_profiles');
            	
                 return json(true,$member);
            }
        }


//        var_dump($result);die;
        return json(false,$errors);
	
  }
	
	
	
	
function check_coupon(){
	$coupon_code = $this->input->post('coupon_code');
	$coupon_password = $this->input->post('coupon_password');
	$member = $this->input->post('member');

	if(empty($coupon_code))
	return json(false,'الكود مطلوب');
	if(empty($coupon_password))
	return json(false,'كلمة السر مطلوبه');

	$coupon = get_row('coupon',['code'=>$coupon_code,'password'=>$coupon_password]);  //lastq();
	
	if(!empty($coupon)){
	// if($coupon->password != $coupon_password )
	// return json(false,'كلمة السر خطا');
    if($coupon->is_active == 0)
	return json(FALSE,'هذا الكوبون غير صالح');
	else if( strtotime(($coupon->expire_date)) < strtotime(date('Y-m-d')))
	return json(false,'هذا الكوبون منتهى الصلاحيه');
	if($coupon->usage == 1 )
	return json(false,'هذا الكوبون مستخدم من قبل');
	
	else{
		$new_credit = $member['credit'] + $coupon->price; 	
		$this->gm->update_where('member',array('member_id'=>$member['member_id']),array('credit'=>$new_credit));
		
		$data['new_member_data'] = get_row('member',array('member.member_id'=>$member['member_id']),'member_profiles');		
       
	   $this->gm->update_where('coupon',array('coupon_id'=>$coupon->coupon_id),array("usage"=>1));
	   
	    $data['copon_price'] =  $coupon->price;
	   
		return json(true,$data);
	}
	}else{
		return json(false,'كوبون غير موجود');
	}
	
}

	
	
	
	function contact(){
		
	    $result = array();
		$result['facebook'] = get_setting('facebook');
		$result['twitter'] = get_setting('twitter');
		$result['instagram'] = get_setting('instagram');
		$result['google'] = get_setting('google');
		
		
		$result['phone'] = get_setting('phone');
		$result['address'] = get_setting('address');
		$result['email'] = get_setting('email');
			
		echo json_encode($result);
		
	}
	
	
	
function rank() {   
       
            $check = get_row('rank', array('post_id' => $this->input->get('post_id'), 'member_id' => $_GET['member_id']));
            if ($check != null)
                echo json(false,'لقد قيمت المنتج بالفعل');
            else {
                $data['post_id'] = $this->input->get('post_id');
                $data['value'] = $this->input->get('value');
				$data['member_id'] =  $_GET['member_id'];
                 $this->General_model->add('rank', $data);
				unset($data['value']);
				$data['content'] = $this->input->get('comment');
				$data['rank_id'] = $this->db->insert_id();
                $this->gm->add('comment',$data);
                $get = $this->General_model->get('rank', array('post_id' => $data['post_id']));
                $this->db->select_sum('value');
                $get_sum = $this->General_model->get('rank', array('post_id' => $data['post_id']));
                if ($get != null) {
                    $new_rank = $get_sum[0]->value / count($get);
                    $this->General_model->update_where('post', ['post_id'=>$data['post_id']], array('rank' => $new_rank));
                    echo json(true, 'شكرا لتقييمك');
                }
            }
        
    }






	
	
	function check_category(){
		
		//print_r($_POST);die;
		
		$id = $this->input->post('id');
		$cat = get_row('category',array('category.category_id'=>$id),'category_lang');
		
		if($cat){
		$data['cat'] = $cat;	
		$data['categories'] = $this->gm->get_join_active('category', ['module_title' => 'product','parent_category_id' => $id]);
		if(!empty($data['categories'])){
		foreach($data['categories'] as $key =>$value){
			$data['categories'][$key]->count_prod =  count($this->gm->get_join_active('post',['module_title'=>'product','category_id'=>$value->category_id]));
		}}
		
		$data['products'] =  $this->gm->get_join_active('post',['module_title'=>'product','category_id'=>$id],'post_lang');	
		if(!empty($data['products'])){
		foreach ($data['products'] as $key => $value) {
			$data['products'][$key]->rank_stars = get_rank_app($value->post_id);
		}
		
	    }
  		if ($data)
            echo json(true, $data);
        else
            echo json(false, $data);
            }else{
            	
			  $data['categories'] = $this->gm->get_join_active('category', ['module_title' => 'product']);
			  if(!empty($data['categories'])){
				foreach($data['categories'] as $key =>$value){
					$data['categories'][$key]->count_prod =  count($this->gm->get_join_active('post',['module_title'=>'product','category_id'=>$value->category_id]));
				}}
			  
			  if ($data)
            echo json(true, $data);
           else
            echo json(false, $data);
				
            }
			
		}
		
		
		
	
	
	
	
	function about(){
		
        $result = get_row('pages',array('id'=>1));
       	
        $result->content = strip_tags($result->content);

        echo json_encode($result);
	
        
        }
	
        
        
	
	function most_sell(){
		
		$data = array();
		
	   $data['most_sell'] =$this->gm->get_join_active_home('post',['module_title'=>'product','most_sell'=>1],'post_lang');
		if(!empty($data['most_sell'])){
		foreach ($data['most_sell'] as $key => $value) {
			$data['most_sell'][$key]->rank_stars = get_rank_app($value->post_id);
		}
		}
		
		if ($data)
            echo json(true, $data);
        else
            echo json(false, $data);
		
	}
	


    function home() {
    	
        $this->db->limit('6');	
		$this->db->order_by('post.post_id','desc');	
        $data['foods'] =$this->gm->get_join_active_home('post',['module_title'=>'product'],'post_lang');
		if(!empty($data['foods'])){
		foreach ($data['foods'] as $key => $value) {
			$data['foods'][$key]->rank_stars = get_rank_app($value->post_id);
		}
		}
		
		
        $data['category'] = $this->gm->get_join_active('category', ['module_title' => 'product','parent_category_id' => 0]);
		
		$data['slider'] = $this->gm->get_join_active('post', ['module_title' => 'slider']);
		
		$data['currency'] = get_setting('currency');
		
		$points = get_setting('points');
		
		$ex_points = explode("=", $points);
		
		$data['min_points'] = $ex_points[0];
		
		// print_r($data);die();
		 
        if ($data)
            echo json(true, $data);
        else
            echo json(false, $data);

    }


function product(){
	 $category_id = $this->input->get('category_id');
	 //echo $category_id;die;
	 $result = $this->gm->get_join_active('post',['module_title'=>'product','category_id'=>$category_id],'post_lang');
	  //print_r($result);die;
	  if ($result)
            echo json(true, $result);
        else
            echo json(false);
}


 

    function register(){

        $data = $this->input->post();
        $this -> form_validation -> set_rules('name','الاسم','trim|required');
        $this -> form_validation -> set_rules('phone','رقم الهاتف','trim|required|alpha_numeric');
        $this -> form_validation -> set_rules('address','العنوان','trim|required|');
        $this -> form_validation -> set_rules('email','البريد الاكتروني','trim|required|valid_email|is_unique[member.email]');
        $this->form_validation->set_rules('password', 'الباسورد', 'trim|required|min_length[6]|alpha_dash');
        $this->form_validation->set_rules('confirm_password', 'تاكيد الباسورد', 'trim|required|matches[password]');
        $this -> form_validation -> set_rules('username','اسم المستخدم','trim|required|min_length[4]||is_unique[member.username]');


        $this->form_validation->set_error_delimiters('<p>', '</p>');




        $result = false;
        if ($this->form_validation->run() == FALSE)
        {
            $result .= validation_errors();
        }
        else
        {
            unset($data['confirm_password']);
            $data['password'] = $this->member_auth->hashPassword($data['password']);

            $result = $this->gm->add('member',$data);
			
            if($result){
                $member_id = $this->db->insert_id();
                $this->gm->add('member_profiles',['member_id' => $member_id]);

             $member = $this->db->where(['member.member_id' => $member_id])->join('member_profiles', 'member_profiles.member_id = member.member_id')->get('member')->result_array();;
               // $member = $this->db->
                $result = true;
				
                 return json($result,$member);
            }
        }


//        var_dump($result);die;
        return json(false,$result);
    }



    function login(){

        $data = $_POST;
        if(!empty ($_POST['name']) && !empty($_POST['password'])){ 
		 
        $data['login_by_email'] = $this -> config -> item('login_by_email', 'tank_auth');
        $data['login_by_username'] = ($this -> config -> item('login_by_username', 'tank_auth') AND $this -> config -> item('use_username', 'tank_auth'));

        $log =   $this -> member_auth -> login($data['name'], $data['password'],0, $data['login_by_username'], $data['login_by_email']);
        
        if($log == 1){
            $member_id = $this->session->userdata('member_id');
            $member = get_row('member',['member.member_id' => $member_id],'member_profiles');
		    return json(true,$member);
        } else {
            $errors = $this -> member_auth -> get_error_message();
            foreach ($errors as $k => $v)
                $data['errors'][$k] = $this -> lang -> line($v);

            return json(false,$data['errors']);
        }
		
		}else{
			 $data['errors'] = 'empty';
        	 return json(false,$data['errors']);
        }
    }


 
    function editAccount(){

        //      (1) insert from post
        //      (2) upload images

        // status for result
        $data_ok = false;
        $image_ok = false;

     //   (1)
        $data = $this->input->post();
		//

//        (2)


        $uniqe_email = '';
        $uniqe_username = '';
        if(!empty($data)){
        $unique_email = '';
		$unique_username = '';	
        
        $member = get_row('member',array('member_id ' => $data['member_id']));
		
		if(!empty($member) && $member->email != $data['email']){
		 	$data_update['email'] = $data['email'];
			$uniqe_email = '|is_unique[member.email]';
		}
		if(!empty($member) && $member->username != $data['username']){
		 	$data_update['username'] = $data['username'];
			$uniqe_username = '|is_unique[member.username]';
		}	
		
		
	

        $this -> form_validation -> set_rules('name','الاسم','trim|required');
        $this -> form_validation -> set_rules('phone','رقم الهاتف','trim|required|alpha_numeric');
        $this -> form_validation -> set_rules('address','العنوان الرئيسي','trim|required|');
        $this -> form_validation -> set_rules('email','البريد الاكتروني','trim|required|valid_email'.$uniqe_email);
        $this -> form_validation -> set_rules('username','اسم المستخدم','trim|required|min_length[4]'.$uniqe_username);

        $this->form_validation->set_error_delimiters('<p>', '</p>');
     
        $msg = 'لم يتغير شىء';
        if ($this->form_validation->run() == FALSE)
        {
        	
            $msg = validation_errors();
        } else{
        	
           $data_update['name'] = $data['name'];
		   $data_update['phone'] = $data['phone'];
		   $data_update['address'] = $data['address'];
		   if(!empty($data['address2']))
		   $data_update['address2'] = $data['address2'];
		   if(!empty($data['address3']))
		   $data_update['address3'] = $data['address3'];
		    
           $data_ok = $this->gm->update_where('member',array('member_id'=>$data['member_id']),$data_update);
			
            if($data_ok === 1){
                $data_ok = true;
            }
				
        

            if (isset($_FILES['image'])) {
            	 $media['member_id'] = $data['member_id'];
                $image = $this->do_upload('image');
                if ($image != false){
                    $media['image'] = $image;
				$image_ok = $this->gm->update_where('member_profiles',[
                'member_id' => $media['member_id']
            ],$media);	
			if($image_ok){
                $image_ok = true;
            }
				}
               
            }
        }



       
}
       
         
         
        $member = get_row('member',['member.member_id' => $data['member_id']],'member_profiles');


if($data_ok || $image_ok)
 return json(true,$member);
if($data_ok == false && $image_ok == false)
	 return json(false,$msg);



    }



 function change_password() {
 	
	    $data = $this->input->post();
		//print_r($data);die;
          $member = get_row('member',[
            'member.member_id' => $data['member_id']],'member_profiles');
         $msg = 'قم بتسجيل الدخول اولا';
		
		if (empty($member)) {// not logged in or not activated
		 
			return json(false,$msg);
		
		} else {
		
			$this -> form_validation -> set_rules('old_password', lang('old_password'), 'trim|required');
			$this -> form_validation -> set_rules('new_password', lang('new_password'), 'trim|required|min_length[' . $this ->config -> item('password_min_length', 'tank_auth') . ']|max_length[' . $this -> config -> item ('password_max_length', 'tank_auth') . ']|alpha_dash');
			$this -> form_validation -> set_rules('confirm_new_password', lang('confirm_password'),'trim|required|matches[new_password]');
				
			$this -> form_validation -> set_error_delimiters('<p>', '</p>');	

			$data['errors'] = array();


			if ($this -> form_validation -> run()) {// validation ok
			//die('sadsadsadasasd');
				if ($this -> member_auth -> change_password($data['member_id'],$data['old_password'],$data['new_password'])) {// success
					$member = get_row('member',['member.member_id' => $data['member_id']],'member_profiles');
					
					
					return json(true,$member);
				
				} else {// fail
				 
					$errors = $this -> member_auth -> get_error_message();
                    foreach ($errors as $k => $v){
						$data['errors'][$k] = $this -> lang -> line($v);
					if(!empty($data['errors'][$k]))
				   return json(false,$data['errors'][$k]);
				   }
     				
				}
			}else{
	
				$data['errors'] =  validation_errors();
				return json(false,$data['errors']);
			}

        
     // print_r($data);die;
			
	}

}
   
 


     function details() {

        $data = $this->input->post();


        $id = $this -> get_id_by_slug($data['slug'], 'post');
        
        $result = $this->gm->get_posts_with_option('post',['post.post_id'=>$id]);
         $result = $result[0];
         
           $data['result'] = $result;
           if(isset($result->field_option[0]->field_value)){
           $data['location'] = $result->field_option[0]->field_value;
               $ex = explode(',', $data['location'] ) ;
               $data['lat'] = $ex[0];
               $data['lng'] = $ex[1];
           }
           else{
           $data['location'] = '';
           $data['lat'] = "";
           $data['lng'] = '';
           }
		   // print_r($result);die;
           $post_owner = get_row('member',['member_id' => $result['member_id']]);
		   // print_r($post_owner);die;
           $data['post_owner']['name'] = $post_owner->name; 
           $data['post_owner']['phone'] = $post_owner->phone; 
           $data['post_owner']['email'] = $post_owner->email;
           $data['post_owner']['member_id'] = $post_owner->member_id; 
         
            $is_apply = get_row('apply',['member_id'=>$data['member_id'],'post_id'=>$id]);
            $data['all_apply'] = query("SELECT apply.*,member.name FROM `apply` 
                                            JOIN member on member.member_id = apply.member_id
                                            WHERE apply.post_id = $id");
          if(!empty($is_apply))
                $data['is_apply'] = true;
            else
                $data['is_apply'] = false;
                
                if(!empty($data))
                    return json(true,$data);
                else
                    return json(false,$data);
         
           
    }


    function Profile(){
        	
        $member_data = $this->input->get('member_data');
		$member_id = $member_data->member_id;
        // print_r($member_data);die;
        $member = get_row('member',['member.member_id'=>$member_id],'member_profiles');
		if(!empty($member)){
        $result['image'] = get_image($member->image);
        $result['name'] = $member->name;
		$result['username'] = $member->username;
        $result['email'] = $member->email;
        $result['phone'] = $member->phone;
		$result['address'] = $member->address;
		$result['address2'] = $member->address2;
		$result['address3'] = $member->address3;
		}
        echo json_encode($result);
        
    }

    
    
   function send_order(){
   				
   		$member = $this->input->post('member');	
   		$cart = $this->input->post('products');	
   		$total = $this->input->post('total');
   		$address = $this->input->post('address');
		$notes = $this->input->post('notes');
		$lat = $this->input->post('lat');
		$lng = $this->input->post('lng');
		if(empty($notes))
		$notes = '';
		//print_r($cart);die;	
		
		
			
   		if($member == null ){
   			return json(false,array(''));
   		}elseif($cart == null){
   			return json(false,array(''));
		}else{
	    
		// if(!empty($copon['copon_code'])){
// 			
		// $copon_data = get_row('coupon',array("code" => $copon['copon_code']));
// 		
		// if(!empty($copon_data)){
			// $copon_id = $copon_data->coupon_id;
		// }else{
			// $copon_id = 0;
// 		
		// }
// 		
		// }else{
				// $copon_id = 0;
		// }
		
		if(empty($address))
			$address = $member['address'];
		
		$order_data = array(
		'member_id' => $member['member_id'],
		'address' => $address,
		'member_location' => $lat.','.$lng,
		'notes' =>$notes,
		'total' => $total 
		);
		
		$add_order = $this->gm->add('order',$order_data);
		
		if($add_order){
                	
                $order_id = $this->db->insert_id();
			   /* if($copon_id !=0)
				$this->gm->update_where('coupon',array('coupon_id'=>$copon_id),array("usage"=>1));
				*/
				
			    foreach($cart as $key => $value){
			 
			    $order_product_data = array(
				'order_id' => $order_id,
				'post_id' => $value['product_id'],
				'quantity' => $value['count'],
				'price' => $value['price'] 
				);	
				
			    $this->gm->add('order_post',$order_product_data);
				
              }
				
				
				
		$member_credit_used = 0;
		$rest_member_credit = 0;

	if($member['credit'] > 0){
		
		if($member['credit'] >= $total ){
		$member_credit_used = $total;
		$rest_member_credit = $member['credit'] - $total;
		
		}else{
			$member_credit_used = $member['credit'];
			$rest_member_credit = 0;
		}
		
		$this -> gm -> update_where('member', ['member_id' => $member['member_id']], ['credit' => $rest_member_credit]);
		$this -> gm -> update_where('order', ['order_id' => $order_id], ['member_credit_used' => $member_credit_used]);
		
			}
				$member_update = get_row('member',array('member.member_id'=>$member['member_id']),'member_profiles');	
				
			    
				$suc_msg = order_msg();
				$cart = '';
				$total = 0;
        	    return json(true,array('time_msg'=>$suc_msg,'member_update'=>$member_update));
            }else{
            	return json(false,array(''));
            }
		
       			
		}	
   				
   			
   			
   	    return json(false,array(''));
   	
   }




  function last_orders(){
		
	$member = $this->input->post('member');		
	if($member != null){
		$member_id = $member['member_id'];
	$this->db->order_by('order.order_id','desc');
	$member_orders = table('order',['member_id'=>$member_id]);
	if(!empty($member_orders)){
		foreach($member_orders as $key => $value){
			$date = new DateTime($member_orders[$key]->created_date);
			$member_orders[$key]->created_date = $date->format('Y-m-d');

			if($member_orders[$key]->status == 'Pending' && $member_orders[$key]->is_read == 1){
				$member_orders[$key]->status = '<span style="color:#44121f;" ><i class="fa fa-clock-o" aria-hidden="true"></i> قيد الإنتظار </span>';
			}elseif($member_orders[$key]->status == 'Pending' && $member_orders[$key]->is_read == 0){
				$member_orders[$key]->status = '<span style="color:green;" ><i class="fa fa-star fa-spin"></i> تم الإستقبال وجارى التنفيذ </span>';
			}elseif($member_orders[$key]->status == 'Done'){
				$member_orders[$key]->status = '<span style="color:gray;" ><i class="fa fa-check-circle-o" aria-hidden="true"></i> تم التنفيذ </span>';
			}elseif($member_orders[$key]->status == 'Rejected'){
				$member_orders[$key]->status = '<span style="color:red;" ><i class="fa fa-window-close" aria-hidden="true"></i> تم الرفض من المسؤل </span>';
			}elseif($member_orders[$key]->status == 'Canceled'){
				$member_orders[$key]->status = '<span style="color:orange;" ><i class="fa fa-exclamation-circle" aria-hidden="true"></i> تم إلغاء الطلب </span>';
			}
			
			
     	}
	}
	
	/*foreach($member_orders as $key => $value){
		
	$member_orders[$key]->order_posts = table('order_post',['order_id'=>$value->order_id]);
	
	foreach($member_orders[$key]->order_posts as $key2 => $value2){
    $product_id = $value2->post_id;	
    $member_orders[$key]->order_posts[$key2]->product_info = $this->gm->get_join_active('post',['module_title'=>'product','post.post_id'=>$product_id],'post_lang'); 	
	}
		
    } */
    
	//print_r($member_orders);die;
	
   return json(true,$member_orders);
	
	}else{
		return json(false,array(''));
	}
	
			
  }




function order_details(){
	
	$order_id = $this->input->post('order_id');
	
	$order = get_row('order',array('order_id'=>$order_id));
		
	$details = table('order_post',['order_id'=>$order_id]);
	
	$result = [];
	
	if(!empty($details)){
	foreach($details as $key => $value){
    $product_id = $value->post_id;	
    $details[$key]->product_info = get_row('post',['module_title'=>'product','post.post_id'=>$product_id,'lang'=>'ar'],'post_lang');
	 	
	}
	
	$result['details'] = $details;
	$result['order'] = $order;
	
	//print_r($result);die;	   
	
	return json(true,$result);
		
	}else{		
	return json(false,'');		
	}
	
	return json(false,array(''));
	
}
	
	
	

function complain(){
	
	$data = $this->input->post();
	
        $this -> form_validation -> set_rules('name','الاسم','trim|required');
        $this -> form_validation -> set_rules('phone','رقم الهاتف','trim|required|alpha_numeric');
        $this -> form_validation -> set_rules('email','البريد الاكتروني','trim|required|valid_email');
        $this -> form_validation -> set_rules('content','الرسالة','trim|required');

        $this->form_validation->set_error_delimiters('<p>', '</p>');

        $result = false;
		
        if ($this->form_validation->run() == FALSE)
        {
            $result .= validation_errors();
        }
        else
        {
        	$data['type'] = "problem";
   
            $result = $this->gm->add('contact',$data);
			
            if($result){
               
                 return json(true,array());
            }
        }


//        var_dump($result);die;
        return json(false,$result);
}	
	

  

function search_post(){
	
	$data = array();
	
    $data = $this->input->post();
	
	//print_r($data);die;
	
	if(!empty($data['search'])){
		
		 $data['search'] = trim($data['search']); 
		  
		$this->db->like('title', $data['search']);
		$result['search_result'] =  $this->gm->get_join_active('post',['module_title'=>'product'],'post_lang');	
		
		//echo lastq();die;
		
		if(!empty($result['search_result'])){
		foreach ($result['search_result'] as $key => $value) {
			$result['search_result'][$key]->rank_stars = get_rank_app($value->post_id);
		}
		
	    }
  		if (!empty($result['search_result']))
            return json(true, $result);
        else
            return json(false, array());
		
	}else{
		
		  return json(false,array());
	}
	
}









function cancel_order(){
	
	$data = $this->input->post();
	
	$order_id = $data['order_id'];
	
	$order = get_row('order',array('order_id'=>$order_id));
	
	if(!empty($order)){
		
	if ($order -> status == 'Pending' && $order -> is_read == 1) {
			
		if($order->member_credit_used > 0){
			$member = get_row('member',['member_id'=>$order->member_id]);
			$new_credit = $member->credit + $order->member_credit_used;
			$this -> gm -> update_where('member', array('member_id' => $member->member_id), array('credit' => $new_credit));
			$this -> gm -> update_where('order', array('order_id' => $order_id), array('member_credit_used' => 0));
		}
		
		
		$this->gm->update_where('order',array('order_id'=>$order_id),array('status'=>'Canceled'));
		
		$member_update = get_row('member',array('member.member_id'=>$order->member_id),'member_profiles');	
		
		
		return json(1,array('suc_msg' => 'تم الغاء الطلب','member_update'=>$member_update)); 
		
		
	}else{
		return json(2,array('error'=>'لايمكن الغاء هذا الطلب تبعا لحالته'));
	}
		
	}else{
		return json(3,array('error'=>'هذا الطلب غير موجود'));
	}
	
}




function use_points(){
	
	$data = array();	
		
	$points = $_POST['points'];
	$member = $_POST['member'];
		
	$member_id = $member['member_id'];
	
	$member_check = get_row('member',array('member_id'=>$member_id));
	 
	$member_points  = $member_check->points_credit;
	$member_credit = $member_check->credit;
	//echo $points;die;
	
    if($member_points >= $points){
    	    
    $data['money'] = count_points_value($points);
	
	if($data['money'] > 0){
		
	$new_points_credit = $member_points - $points;
	$new_credit = $member_credit + $data['money'];
	
	$this->gm->update_where('member',array('member_id'=>$member_id),array('points_credit'=>$new_points_credit,'credit'=>$new_credit));
	
	$data['member_update'] = get_row('member',array('member.member_id'=>$member_id),'member_profiles');	
	
	return json(1,$data);
	
	}else{
		return json(2,$data['money']);
	}
	}else{
		return json(3,array());
	}
	
}






}