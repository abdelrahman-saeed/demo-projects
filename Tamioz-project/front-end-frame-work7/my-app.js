//mainView.router.loadPage('news.html'); 
// Initialize your app
var myApp = new Framework7();

// Export selectors engine
var $$ = Dom7;

// Add view
var mainView = myApp.addView('.view-main', {
    // Because we use fixed-through navbar we can enable dynamic navbar
    dynamicNavbar: true
});

  mainView.hideNavbar(); 


//online_domain
//var url = "http://apps.stoptrainingnow.com/api/";
//var images_url = 'http://apps.stoptrainingnow.com/assets/uploads/image/';

//work_domain
var url = "http://tamiozback.amrbdr.com/api/";
var images_url = 'http://tamiozback.amrbdr.com/assets/uploads/image/';


myApp.onPageInit('courses', function (page) {
  
$$('.speed-dial-buttons a').click(function() {
    $$('.speed-dial').removeClass('.speed-dial_opened');
  });

});



myApp.onPageInit('signup-tc', function (page) {
    // run createContentPage func after link was clicked
  country_city('autocomplete-wanted-country','autocomplete-wanted-city');

});





// myApp.onPageInit('trainee-cources', function (page) {
    // // run createContentPage func after link was clicked
//   
// 
// });


function country_city(country_id,city_id){
  // Fruits data demo array
  var country = ('السعودية مصر الكويت البحرين الامارات').split(' ');

  // auto complete
  var autocompleteDropdownSimple = myApp.autocomplete({
      input: '#'+country_id,
      openIn: 'dropdown',
      source: function (autocomplete, query, render) {
          var results = [];
          if (query.length === 0) {
              render(results);
              return;
          }
          // Find matched items
          for (var i = 0; i < country.length; i++) {
              if (country[i].toLowerCase().indexOf(query.toLowerCase()) >= 0) results.push(country[i]);
          }
          // Render items by passing array with result items
          render(results);
      }
    });


    var city = ('المدينة مكه القصيم حفرالباطن').split(' ');

    // auto complete
    var autocompleteDropdownSimple = myApp.autocomplete({
        input: '#'+city_id,
        openIn: 'dropdown',
        source: function (autocomplete, query, render) {
            var results = [];
            if (query.length === 0) {
                render(results);
                return;
            }
            // Find matched items
            for (var i = 0; i < city.length; i++) {
                if (city[i].toLowerCase().indexOf(query.toLowerCase()) >= 0) results.push(city[i]);
            }
            // Render items by passing array with result items
            render(results);
        }
      });

}



// Generate dynamic page
var dynamicPageIndex = 0;
function createContentPage() {
	mainView.router.loadContent(
        '<!-- Top Navbar-->' +
        '<div class="navbar">' +
        '  <div class="navbar-inner">' +
        '    <div class="left"><a href="#" class="back link"><i class="icon icon-back"></i><span>Back</span></a></div>' +
        '    <div class="center sliding">Dynamic Page ' + (++dynamicPageIndex) + '</div>' +
        '  </div>' +
        '</div>' +
        '<div class="pages">' +
        '  <!-- Page, data-page contains page name-->' +
        '  <div data-page="dynamic-pages" class="page">' +
        '    <!-- Scrollable page content-->' +
        '    <div class="page-content">' +
        '      <div class="content-block">' +
        '        <div class="content-block-inner">' +
        '          <p>Here is a dynamic page created on ' + new Date() + ' !</p>' +
        '          <p>Go <a href="#" class="back">back</a> or go to <a href="services.html">Services</a>.</p>' +
        '        </div>' +
        '      </div>' +
        '    </div>' +
        '  </div>' +
        '</div>'
    );
	return;
}



//////////////////////////ajax//////////////////////////////////////////



function rendar_home(rout = 1){

var member = JSON.parse(localStorage.getItem('member'));
 //console.log(member);
if(member != null){
    if(rout == 1)
	mainView.router.loadPage('trainee-cources.html');

	$(".is_login").show();
	$(".not_login").hide();
	
	if(member.member_group_id == 1){
		$(".is_center").show();
		$(".is_trainer").hide();
		$(".is_trainee").hide();
        $(".member_test").hide();
        $(".member_result").hide();
		
	}else if(member.member_group_id == 2){
		$(".is_center").hide();
		$(".is_trainer").show();
		$(".is_trainee").hide();
        if(member.last_member_result_id != 0 && member.last_member_result_id != null && member.last_member_result_id != ''){
            $(".member_test").hide();
            $('.member_result a').attr('id',member.last_member_result_id);
            $(".member_result").show();
        }else {
            $(".member_test").show();
            $(".member_result").hide();
        }
	}else{
	    //console.log($(".member_test"));
		$(".is_center").hide();
		$(".is_trainer").hide();
		$(".is_trainee").show();
		if(member.last_member_result_id != 0 && member.last_member_result_id != null && member.last_member_result_id != ''){
            $(".member_test").hide();
            $('.member_result a').attr('id',member.last_member_result_id);
            $(".member_result").show();
        }else {
            $(".member_test").show();
            $(".member_result").hide();
        }


	}
	
	
member.image = member.image ? images_url + member.image : 'img/person.png';	
$("#prof_img").html('<img style="border-radius: 50%;height: 110px;width: 120px;" src="'+member.image+'" alt="">');	
$("#prof_name").html(member.name);	

if(member.country_id != null && member.country_id != ''){
get_city(member.country_id);

}
			
}else{
	$(".is_login").hide();
	$(".not_login").show();
}
	

 check_latlng(); 
	
	
}


$(document).ready(function () {
 
  var member = JSON.parse(localStorage.getItem('member'));  
   
   rendar_home();
    
    if(member != null && member.member_group_id == 1){
    	// الاشعارات
     $.ajax({
	        type: "post",
	        url: url + "count_nots",
	        data:{'member_id':member.member_id},
	        dataType: "JSON",    
	        success: function (result) {
	        	  $("#count_nots").html(result.msg);
	        	  if(result.msg > 0)
	        	   myApp.alert('<b style="color:#36a5a4;">لديك عدد <div class="chip" style="background-color:#01a686;border-radius: 50%;padding: 12px 8px;"> '+result.msg+'</div> من الإشعارات الجديده</b><br><br><a style="color:#01a686;" href="show-nots.html" ><i class="fa fa-bell"></i>   رؤية الإشعارات  <i class="fa fa-bell"></i></a>','');
	            }               
       });
    }
    
    
    // رابط تقييم التطبيق
     $.ajax({
	        type: "post",
	        url: url + "app_ev",
	        dataType: "JSON",    
	        success: function (result) {
	        	  if(result != null && result.msg != null && result.msg != 0){
	        	   $("#app_ev").attr("href", result.msg);
	        	  }
	            }               
       });
    
    
    // المؤهلات الدراسيه	
 $.ajax({
        type: "post",
        url: url + "get_education",
        dataType: "JSON",    
        success: function (result) {
            
            if (result.status == true) {
               
            if(result != null && result.msg != null){
         
              localStorage.setItem('education', JSON.stringify(result.msg));
            }}}               
    });
    
    
// التخصصات    
  $.ajax({
        type: "post",
        url: url + "get_specialist",
        dataType: "JSON",    
        success: function (data) {
            
            if (data.status == true) {
               
            if(data != null && data.msg != null){
         
              localStorage.setItem('specialist', JSON.stringify(data.msg));
            }}}               
    });
    
    

  // الدول	
 $.ajax({  
        type: "post",
        url: url + "get_country",
        dataType: "JSON",    
        success: function (result) {
            
            if (result.status == true) {
               
            if(result != null && result.msg != null){
         
              localStorage.setItem('country', JSON.stringify(result.msg));
            }}}               
    });
     

if(member == null || member.country_id == null){ 
  // المدن	
 $.ajax({  
        type: "post",
        url: url + "get_city",
        dataType: "JSON",    
        success: function (result) {
            
            if (result.status == true) {
               
            if(result != null && result.msg != null){
         
              localStorage.setItem('city', JSON.stringify(result.msg));
            }}}               
    });

}    
    
});



function get_city(country_id){   
	var member = JSON.parse(localStorage.getItem('member'));  
	
	$.ajax({  
        type: "post",
        data:{'country_id':country_id},
        url: url + "get_city",
        dataType: "JSON",     
        success: function (result) {
            
            if (result.status == true) {
               
            if(result != null && result.msg != null){
            localStorage.setItem('city', JSON.stringify(result.msg));	
           $(".city_select").html(''); 
         for(var i in result.msg){
              	
              	var item = result.msg[i];
              	if(item.arabic == '' || item.arabic == null)
              	continue;
              	 
              	  
              	if(i == 0)
              	 $(".city_select").append('<option value="'+item.id+'" selected>'+item.arabic+'</option>'); 
              	else 
              	 $(".city_select").append('<option value="'+item.id+'">'+item.arabic+'</option>');
              	
             } 
              //localStorage.setItem('country', JSON.stringify(result.msg));
            }}}               
    });
}



myApp.onPageInit('home', function (page) {   

     rendar_home();

});


myApp.onPageInit('index', function (page) {   

     rendar_home();

});

myApp.onPageInit('about-app', function (page) {   
$('.navbar-inner').css('background-color','#f2a000');
    $.ajax({
        type: "get",
        url: url + "about",
        dataType: "JSON",
        beforeSend: function () {
            myApp.showIndicator();
        },
        success: function (data) {
            data.image = data.image ? images_url + data.image : 'img/mylogo1.png';
            //alert(data.content);
            myApp.hideIndicator();
            var aboutTemplate = $('#about_script').html(); //script
            var compiledAboutTemplate = Template7.compile(aboutTemplate); //تحضير الاسكربت لاستقبال المتغيرات
            var result = compiledAboutTemplate({data}); 

            //  console.log(data);
            $('#about_content').html(result);
            
            
            if(data.facebook == 0 && data.twitter == 0 && data.whatsapp == 0 && data.google == 0)
              $('#share_links').hide();
              
            if(data.facebook == 0)
              $('#share_facebook').hide();
            if(data.twitter == 0)
              $('#share_twitter').hide();
            if(data.whatsapp == 0)
              $('#share_whatsapp').hide();
            if(data.google == 0)
              $('#share_google').hide();
              
            
            
            myApp.hideIndicator();

        },
        error: function (jqXHR, textStatus, errorThrown) {
            myApp.alert('خطأ فى الاتصال', '<b style="color:#e04e4e">خطأ</b>');
            myApp.hideIndicator();
        }
    });


});







myApp.onPageInit('policy', function (page) {   
$('.navbar-inner').css('background-color','#f2a000');
    $.ajax({
        type: "get",
        url: url + "policy",
        dataType: "JSON",
        beforeSend: function () {
            myApp.showIndicator();
        },
        success: function (data) {
            data.image = data.image ? images_url + data.image : 'img/mylogo1.png';
            //alert(data.content);
            myApp.hideIndicator();
            var policyTemplate = $('#policy_script').html(); //script
            var compiledpolicyTemplate = Template7.compile(policyTemplate); //تحضير الاسكربت لاستقبال المتغيرات
            var result = compiledpolicyTemplate({data}); 

            //  console.log(data);
            $('#policy_content').html(result);

            myApp.hideIndicator();

        },
        error: function (jqXHR, textStatus, errorThrown) {
            myApp.alert('خطأ فى الاتصال', '<b style="color:#e04e4e">تنبيه !</b>');
            myApp.hideIndicator();
        }
    });


});





myApp.onPageInit('contact', function (page) {

    $.ajax({
        type: "POST",
        url: url + "contact",
        dataType: "JSON",
        beforeSend: function () {
            myApp.showIndicator();
        },
        success: function (result) {


            $.get(url + "contact", function (data) {

                var contactTemplate = $('#contact_script').html(); //script
                var compiledContactTemplate = Template7.compile(contactTemplate); //تحضير الاسكربت لاستقبال المتغيرات
                var result = compiledContactTemplate({
                    data
                }); // ارسال المتغير

                // console.log(data);
                $('#contact_content').html(result);

            }, 'JSON');


            myApp.hideIndicator();

        },
        error: function (jqXHR, textStatus, errorThrown) {
            myApp.alert('خطأ فى الاتصال', '<b style="color:#e04e4e">تنبيه !</b>');
            myApp.hideIndicator();
        }
    });


    // 

    //var data_home = JSON.parse(localStorage.getItem('data'));


});



myApp.onPageInit('send-message', function (page) {

var member = JSON.parse(localStorage.getItem('member'));  

if(member != null){
	
	 $('.not_login').hide();
	  $('#m_id').val(member.member_id);
	    $('#complain_form').submit(function (e) {
        e.preventDefault();
        //myApp.alert('','Ok');  
        $('#name8 ,#email8 , #phone8, #content8').css('border', 'none');  
        if (!$('#content8').val()) {
            $("#content8").css("border-bottom", "2px solid #36a5a4");
            $("#content8").focus();
        } else {
               
            // Create a formdata object and add the files
            var formData = new FormData($(this)[0]);
            // var data = JSON.stringify(formData);
            //console.log(data) ;
            $.ajax({
                url: url + "complain",
                type: 'POST',
                data: formData,
                cache: false,
                contentType: 'json',
                processData: false, // Don't process the files
                contentType: false, // Set content type to false as jQuery will tell the server its a query string request
                beforeSend: function () {
                    myApp.showIndicator();
                },
                success: function (out, textStatus, jqXHR) {
                    out = JSON.parse(out);

                    // console.log(out);
                    // Success so call function to process the form
                    if (out.status == true) {
                        $('#complain_form')[0].reset();
                        myApp.alert(' شكرا لك', '<b style="color:#8bc18b">تم إرسال الرسالة بنجاح</b>');

                    } else {
                        console.log(out.msg) ;
                        myApp.alert(out.msg, '<b style="color:#e04e4e">تنبيه !</b>');
                    }

                    myApp.hideIndicator();

                },
                error: function (jqXHR, textStatus, errorThrown) {
                    myApp.alert('خطا فى الاتصال', '<b style="color:#e04e4e">خطأ !</b>');
                    myApp.hideIndicator();
                }
            });

        }

    });
	
}else{
	

    $('#complain_form').submit(function (e) {
        e.preventDefault();
        //myApp.alert('','Ok');  
        $('#name8 ,#email8 , #phone8, #content8').css('border', 'none');  
        if (!$('#name8').val()) {
            $("#name8").css("border-bottom", "2px solid #36a5a4");
            $("#name8").focus();
        } else if (!$('#email8').val()) {
            $("#email8").css("border-bottom", "2px solid #36a5a4");
            $("#email8").focus();
        } else if (!$('#phone8').val()) {
            $("#phone8").css("border-bottom", "2px solid #36a5a4");
            $("#phone8").focus();
        } else if (!$('#content8').val()) {
            $("#content8").css("border-bottom", "2px solid #36a5a4");
            $("#content8").focus();
        } else {

            // Create a formdata object and add the files
            var formData = new FormData($(this)[0]);
            // var data = JSON.stringify(formData);
            //console.log(data) ;
            $.ajax({
                url: url + "complain",
                type: 'POST',
                data: formData,
                cache: false,
                contentType: 'json',
                processData: false, // Don't process the files
                contentType: false, // Set content type to false as jQuery will tell the server its a query string request
                beforeSend: function () {
                    myApp.showIndicator();
                },
                success: function (out, textStatus, jqXHR) {
                    out = JSON.parse(out);

                    // console.log(out);
                    // Success so call function to process the form
                    if (out.status == true) {
                        $('#complain_form')[0].reset();
                        myApp.alert(' شكرا لك', '<b style="color:#8bc18b">تم إرسال الرسالة بنجاح</b>');

                    } else {
                        console.log(out.msg) ;
                        myApp.alert(out.msg, '<b style="color:#e04e4e">تنبيه !</b>');
                    }

                    myApp.hideIndicator();

                },
                error: function (jqXHR, textStatus, errorThrown) {
                    myApp.alert('خطا فى الاتصال', '<b style="color:#e04e4e">خطأ !</b>');
                    myApp.hideIndicator();
                }
            });

        }

    });	
	
}


});






myApp.onPageInit('news', function (page) {

    var newsTemplate = $('#news_script').html(); //script
    var compiledNewsTemplate = Template7.compile(newsTemplate); //تحضير الاسكربت لاستقبال المتغيرات


    $.ajax({
        type: "post",
        url: url + "news",
        dataType: "JSON",
        beforeSend: function () {
            myApp.showIndicator();
        },
        success: function (result) {
            myApp.hideIndicator();
            if (result.status == true) {

                if (result != null && result.msg != null && result.msg.news != null) {


                    for (var i in result.msg.news) {
 						 
 						 if(i == 10) 
 						 break;
                        var item = result.msg.news[i];
                        item.image = item.image ? images_url + item.image : 'img/mylogo1.png';
                      
                    
                        var res_news = compiledNewsTemplate({item});
                        
                        $('#news_content').append(res_news);

                    }
                    
                   
                         
           /* بدايه الاسكرول     */
                	 total = result.msg.news.length;            //************* غير هنا ****************//

                	// Loading flag
        var loading = false;
// Last loaded index
        var lastIndex = $$('#news_content .card ').length;            //************* غير هنا ****************//
// Max items to load
        var maxItems = total-1;
console.log('total :'+ maxItems);
// Append items per load
        var itemsPerLoad = 10;
// Attach 'infinite' event handler
        $$('.infinite-scroll').on('infinite', function () {
        	 console.log('lastIndex :' + lastIndex);
            // Exit, if loading in progress
            if (loading) return;
            // Set loading flag
            loading = true;
            // Emulate 1s loading
            setTimeout(function () {
                // Reset loading flag
                loading = false;
                console.log('lastIndex :' + lastIndex);
                if (lastIndex >= maxItems) {
                    // Nothing more to load, detach infinite scroll events to prevent unnecessary loadings
                    myApp.detachInfiniteScroll($$('.infinite-scroll'));
                    // Remove preloader
                    $$('.infinite-scroll-preloader').remove();                          //************* انقلها ****************//
                    return;
                }if (lastIndex < maxItems) {
                    // Generate new items HTML
                    var html = '';
                    for (var i = lastIndex + 1; i <= lastIndex + itemsPerLoad; i++) {              //************* غير هنا ****************//
                    	
                     var item = result.msg.news[i];
                        item.image = item.image ? images_url + item.image : 'img/mylogo1.png';
                      
                    
                        var res_news = compiledNewsTemplate({item});
                        
                        $('#news_content').append(res_news);

                    }
                }
                // Append new items
              //  $$('.list-block ul').append(html);
                // Update last loaded index
            }, 1000);
            lastIndex = $$('#news_content .card').length;                     //************* غير هنا ****************//
            console.log('lastIndex after :' + lastIndex);
        });

                 	
                	
          /* نهايه الاسكرول     */           
                    
                    
               if(result.msg.seen == 0){
					$(".veiws-number").hide();
				}else{
					$(".veiws-number").show();
				}
                    
                    

                } else {
                	 $$('.infinite-scroll-preloader').remove();           
                    $('#news_content').html('<div class="center" style="text-align: center;padding-top: 10%; font-size: 30px;color:brown;">عذراً ...لا توجد نتائج!</div>');
                }

            } else {
            	 $$('.infinite-scroll-preloader').remove();           
                $('#news_content').html('<div class="center" style="text-align: center;padding-top: 10%; font-size: 30px;color:brown;">عذراً ...لا توجد نتائج!</div>');
            }


        },
        error: function (jqXHR, textStatus, errorThrown) {
        	 $$('.infinite-scroll-preloader').remove();           
            myApp.alert('خطا فى الاتصال', '<b style="color:#e04e4e">تنبيه !</b>');
            myApp.hideIndicator();
        }
    });


});







   
function news_details(id){
    
    var newsDetailsTemplate = $('#news_details_script').html(); //script
    var compiledNewsDetailsTemplate = Template7.compile(newsDetailsTemplate); //تحضير الاسكربت لاستقبال المتغيرات
    
     $.ajax({
        type: "post",
        data : {'id' : id},
        url: url + "news_details",
        dataType: "JSON",
        beforeSend: function () {
            myApp.showIndicator();
        },
        success: function (result) {
            myApp.hideIndicator();
            if (result.status == true) {

                if (result.msg.details != null) {

                        result.msg.details.image = result.msg.details.image ? images_url + result.msg.details.image : 'img/mylogo1.png';
                      
                        var data = result.msg.details;
                        var res_newsDetails = compiledNewsDetailsTemplate({data});
                        
                        $('#news_details_content').html(res_newsDetails);
                        
                        if(result.msg.seen == 0){
							$("#views_number").hide();
						}else{
							$("#views_number").show();
						}
                        
                        news_comments(data.id);
                        
                        

            } else {
                $('#news_details_content').html('<div class="center" style="text-align: center;padding-top: 10%; font-size: 30px;color:brown;">عذراً ...لا توجد نتائج!</div>');
            }

            } else {
                $('#news_details_content').html('<div class="center" style="text-align: center;padding-top: 10%; font-size: 30px;color:brown;">عذراً ...لا توجد نتائج!</div>');
            }

        },
        error: function (jqXHR, textStatus, errorThrown) {
            myApp.alert('خطا فى الاتصال', '<b style="color:#e04e4e">تنبيه !</b>');
            myApp.hideIndicator();
        }
    });
    
}


function add_comment(news_id){
	
	var member = JSON.parse(localStorage.getItem('member'));
    if(member != null){
    	var member_id = member.member_id;
    	if(!$.trim($("#comment").val())){
    		myApp.alert('من فضلك قم بكتابة تعليق', '<b style="color:#e04e4e">تنبيه !</b>');
    	}else{
    	
     $.ajax({
        type: "post",
        data : {'news_id' : news_id,'member_id':member_id,'content': $("#comment").val()},
        url: url + "add_comment",
        dataType: "JSON",
        beforeSend: function () {
            myApp.showIndicator();
        },
        success: function (result) {
            myApp.hideIndicator();
            if (result.status == true) {
             $("#comment").val('');
             var c = result.msg;
             
             c.type = 'عضو';
             
             if(c.member_group_id == 3)
             c.type = 'متدرب';
             if(c.member_group_id == 2)
             c.type = 'مدرب';
             if(c.member_group_id == 1)
             c.type = 'مركز تدريب';
             
             
             
             c.image = c.image ? images_url + c.image : 'img/person.png'; 
             var comment = '<li><a href="#">'; 
             comment += '<div class="item-media"><img src="'+c.image+'" width="55" height="55" style="border-radius: 50%;"> <p style="padding-right: 6px; padding-bottom: 4px;color: #347c7c; font-size: 13px;line-height: 1.5;">'+c.name+'('+c.type+')'+' <br> <span style="font-size: 11px;color: rgba(0,0,0,.4);">'+c.date_time+'</span></p></div>';
             comment += '<div class="item-inner" style="margin-left: 0;margin-right: 9px;margin-top: -48px;">';  
             comment += '<p class="" style="background: #dedede;padding: 10px;height: auto;font-size: 11px;border-radius: 17px;margin-right: 38px;"> '+c.content+' </p> </div></a></li>';
          
          
             
             
             if(c.active == 0){
             myApp.alert(' تم حفظ التعليق وفى انتظار المراجعه من المسؤول', '<b style="color:#e04e4e">تنبيه !</b>');
             } else {
             $('#comments_content').prepend(comment);	 
             myApp.alert(' تم حفظ التعليق ', '<b style="color:#e04e4e">تنبيه !</b>');
             }
            
            } else {
                 myApp.alert(result.msg, '<b style="color:#e04e4e">تنبيه !</b>');
            }

        },
        error: function (jqXHR, textStatus, errorThrown) {
            myApp.alert('خطأ فى الاتصال', '<b style="color:#e04e4e">تنبيه !</b>');
            myApp.hideIndicator();
        }
    });
    
    	
    }}else{
    	 mainView.router.loadPage('login-trainee.html'); 
    	//myApp.alert('يجب التسجيل أولاً للتعليق', '<b style="color:#e04e4e">تنبيه !</b>');
    }
	
}






function applay_to_course(course_id){
	
	var member = JSON.parse(localStorage.getItem('member'));
    if(member != null || member.member_group_id != 3){
    	var member_id = member.member_id;
    myApp.confirm(' هل تريد التقديم فى هذه الدوره؟','<b style="color:#e04e4e">تنبيه !</b>', function () {
     $.ajax({
        type: "post",
        data : {'course_id' : course_id,'member_id':member_id},
        url: url + "applay_to_course",
        dataType: "JSON",
        beforeSend: function () {
            myApp.showIndicator();
        },
        success: function (result) {
            myApp.hideIndicator();
            if (result.status == true) {
             myApp.alert(' شكرا لك', '<b style="color:#8bc18b">تم إرسال طلب التقديم الى المركز المسؤل </b>');
            } else {
                 myApp.alert('تم التقديم على هذه الدوره من قبل', '<b style="color:#e04e4e">تنبيه !</b>');
            }

        },
        error: function (jqXHR, textStatus, errorThrown) {
            myApp.alert('خطا فى الاتصال', '<b style="color:#e04e4e">تنبيه !</b>');
            myApp.hideIndicator();
        }
    });
    
    });	
    
    }else{
    	myApp.alert('يجب التسجيل كمتدرب أولاً للتقديم', '<b style="color:#e04e4e">تنبيه !</b>');
    }
	
	
}



   
function news_comments(news_id){
    
    var commentsTemplate = $('#comments_script').html(); //script
    var compiledCommentsTemplate = Template7.compile(commentsTemplate); //تحضير الاسكربت لاستقبال المتغيرات
    
    
    var allowInfinite = true;
	
	// Last loaded index
	var lastItemIndex = $$('#comments_content').length;
	
	// Max items to load
	var maxItems = 2000;

    var itemsPerLoad = 5;
    
    
    
     $.ajax({
        type: "post",
        data : {'id' : news_id},
        url: url + "news_comments",
        dataType: "JSON",
        beforeSend: function () {
            myApp.showIndicator();
        },
        success: function (result) {
            myApp.hideIndicator();
            if (result.status == true) {

                if (result.msg != null) {

	
                
                     for (var i in result.msg) {
                      var item = result.msg[i];
                      item.image = item.image ? images_url + item.image : 'img/person.png';
                      
                            
   
                        var res_com = compiledCommentsTemplate({item});
                        $('#comments_content').append(res_com);
                         
                    }   
                        
            } else {
                $('#news_details_content').html('<div class="center" style="text-align: center;padding-top: 10%; font-size: 30px;color:brown;">لا توجد تعليقات</div>');
            }

            } else {
                $('#news_details_content').html('<div class="center" style="text-align: center;padding-top: 10%; font-size: 30px;color:brown;">لا توجد تعليقات</div>');
            }

        },
        error: function (jqXHR, textStatus, errorThrown) {
            myApp.alert('خطأ فى الاتصال', '<b style="color:#e04e4e">تنبيه !</b>');
            myApp.hideIndicator();
        }
    });
    
}



function show_courses(){

    var coursesTemplate = $('#courses_script').html(); //script
    var compiledCoursesTemplate = Template7.compile(coursesTemplate); //تحضير الاسكربت لاستقبال المتغيرات

    var member = JSON.parse(localStorage.getItem('member'));
    if(member != null && member.member_group_id ==3){
    	var member_id = member.member_id;
    }else{
    	var member_id = 0;
    }
    $.ajax({
        type: "post",
        data:{'search':$('#search').val(),'member_id':member_id},
        url: url + "courses",
        dataType: "JSON",
        beforeSend: function () {
            myApp.showIndicator();
        },
        success: function (result) {
            myApp.hideIndicator();
            if (result.status == true) {

                if (result.msg.courses != null) {
                	$('#courses_content').html('');

                    for (var i in result.msg.courses) {
                    	if(i == 10)            //************* غير هنا ****************//
                    	break;                 //************* غير هنا ****************//
                      var item = result.msg.courses[i];
                      item.image = item.image ? images_url + item.image : 'img/mylogo1.png';
                      item.mark = '';
                      if(item.new == 0 && item.prop == 1){
                      item.mark = '<img src="img/suggest.png" alt="" class="tag-suggest">';
                      }else if(item.new == 1 && item.prop == 0){
                      item.mark = '<img src="img/new.png" alt="" class="tag-new">';
                      }else if(item.new == 1 && item.prop == 1){
                      item.mark = '<img src="img/new-suggest.png" alt="" class="suggest-new">';
                      }else{
                      item.mark = '';
                      }
                    
                        var res_courses = compiledCoursesTemplate({item});
                        $('#courses_content').append(res_courses);
                    }
                    
                    
                    
                    
                    
                    
                    
                    
           /* بدايه الاسكرول     */
                	 total = result.msg.courses.length;            //************* غير هنا ****************//

                	// Loading flag
        var loading = false;
// Last loaded index
        var lastIndex = $$('#courses_content li').length;            //************* غير هنا ****************//
// Max items to load
        var maxItems = total-1;
console.log('total :'+ maxItems);
// Append items per load
        var itemsPerLoad = 10;
// Attach 'infinite' event handler
        $$('.infinite-scroll').on('infinite', function () {
        	 console.log('lastIndex :' + lastIndex);
            // Exit, if loading in progress
            if (loading) return;
            // Set loading flag
            loading = true;
            // Emulate 1s loading
            setTimeout(function () {
                // Reset loading flag
                loading = false;
                console.log('lastIndex :' + lastIndex);
                if (lastIndex >= maxItems) {
                    // Nothing more to load, detach infinite scroll events to prevent unnecessary loadings
                    myApp.detachInfiniteScroll($$('.infinite-scroll'));
                    // Remove preloader
                    $$('.infinite-scroll-preloader').remove();
                    return;
                }if (lastIndex < maxItems) {
                    // Generate new items HTML
                    var html = '';
                    for (var i = lastIndex + 1; i <= lastIndex + itemsPerLoad; i++) {              //************* غير هنا ****************//
                    	
                     var item = result.msg.courses[i];
                      item.image = item.image ? images_url + item.image : 'img/mylogo1.png';
                      item.mark = '';
                      if(item.new == 0 && item.prop == 1){
                      item.mark = '<img src="img/suggest.png" alt="" class="tag-suggest">';
                      }else if(item.new == 1 && item.prop == 0){
                      item.mark = '<img src="img/new.png" alt="" class="tag-new">';
                      }else if(item.new == 1 && item.prop == 1){
                      item.mark = '<img src="img/new-suggest.png" alt="" class="suggest-new">';
                      }else{
                      item.mark = '';
                      }
                    
                        var res_courses = compiledCoursesTemplate({item});
                        $('#courses_content').append(res_courses);
                    }
                }
                // Append new items
              //  $$('.list-block ul').append(html);
                // Update last loaded index
            }, 1000);
            lastIndex = $$('#courses_content li').length;                     //************* غير هنا ****************//
            console.log('lastIndex after :' + lastIndex);
        });

                 	
                	
          /* نهايه الاسكرول     */         
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    

                } else {
                	    $$('.infinite-scroll-preloader').remove();
                    $('#courses_content').html('<div class="center" style="text-align: center;padding-top: 10%; font-size: 30px;color:brown;">عذراً ...لا توجد نتائج!</div>');
                }

            } else {
            	    $$('.infinite-scroll-preloader').remove();
                $('#courses_content').html('<div class="center" style="text-align: center;padding-top: 10%; font-size: 30px;color:brown;">عذراً ...لا توجد نتائج!</div>');
            }


        },
        error: function (jqXHR, textStatus, errorThrown) {
            myApp.alert('خطا فى الاتصال', '<b style="color:#e04e4e">تنبيه !</b>');
            myApp.hideIndicator();
        }
    });
}






myApp.onPageInit('trainee-cources', function (page) {

$(function (){
    $('.favorite .fa').click(function () {
    $(this).toggleClass('fa-star-o fa-star');
    });
  });

show_courses();

});   




myApp.onPageInit('fav-cources', function (page) {

 var coursesTemplate = $('#fav_courses_script').html(); //script
    var compiledCoursesTemplate = Template7.compile(coursesTemplate); //تحضير الاسكربت لاستقبال المتغيرات

    var member = JSON.parse(localStorage.getItem('member'));
    if(member != null && member.member_group_id ==3){
    	var member_id = member.member_id;
    }else{
    	var member_id = 0;
    }
    $.ajax({
        type: "post",
        data:{'member_id':member_id},
        url: url + "fav_courses",
        dataType: "JSON",
        beforeSend: function () {
            myApp.showIndicator();
        },
        success: function (result) {
            myApp.hideIndicator();
            if (result.status == true) {

                if (result.msg.courses != null) {

                    for (var i in result.msg.courses) {
                      var item = result.msg.courses[i];
                      item.image = item.image ? images_url + item.image : 'img/mylogo1.png';
                      item.mark = '';
                      if(item.new == 0 && item.prop == 1){
                      item.mark = '<img src="img/suggest.png" alt="" class="tag-suggest">';
                      }else if(item.new == 1 && item.prop == 0){
                      item.mark = '<img src="img/new.png" alt="" class="tag-new">';
                      }else if(item.new == 1 && item.prop == 1){
                      item.mark = '<img src="img/new-suggest.png" alt="" class="suggest-new">';
                      }else{
                      item.mark = '';
                      }
                    
                        var res_courses = compiledCoursesTemplate({item});
                        $('#fav_courses_content').append(res_courses);
                    }

                } else {
                    $('#fav_courses_content').html('<div class="center" style="text-align: center;padding-top: 10%; font-size: 30px;color:brown;">عذراً ...لا توجد نتائج!</div>');
                }

            } else {
                $('#fav_courses_content').html('<div class="center" style="text-align: center;padding-top: 10%; font-size: 30px;color:brown;">عذراً ...لا توجد نتائج!</div>');
            }


        },
        error: function (jqXHR, textStatus, errorThrown) {
            myApp.alert('خطا فى الاتصال', '<b style="color:#e04e4e">تنبيه !</b>');
            myApp.hideIndicator();
        }
    });

});  





myApp.onPageInit('new-cources', function (page) {

 var coursesTemplate = $('#new_courses_script').html(); //script
    var compiledCoursesTemplate = Template7.compile(coursesTemplate); //تحضير الاسكربت لاستقبال المتغيرات

    var member = JSON.parse(localStorage.getItem('member'));
    if(member != null && member.member_group_id ==3){
    	var member_id = member.member_id;
    }else{
    	var member_id = 0;
    }
    $.ajax({
        type: "post",
        data:{'member_id':member_id},
        url: url + "new_courses",
        dataType: "JSON",
        beforeSend: function () {
            myApp.showIndicator();
        },
        success: function (result) {
            myApp.hideIndicator();
            if (result.status == true) {

                if (result.msg.courses != null) {

                    for (var i in result.msg.courses) {
                    	if(i == 10 )                           //************* غير هنا ****************//
                    	break;											 //************* غير هنا ****************//
                      var item = result.msg.courses[i];
                      item.image = item.image ? images_url + item.image : 'img/mylogo1.png';
                      item.mark = '';
                      if(item.new == 0 && item.prop == 1){
                      item.mark = '<img src="img/suggest.png" alt="" class="tag-suggest">';
                      }else if(item.new == 1 && item.prop == 0){
                      item.mark = '<img src="img/new.png" alt="" class="tag-new">';
                      }else if(item.new == 1 && item.prop == 1){
                      item.mark = '<img src="img/new-suggest.png" alt="" class="suggest-new">';
                      }else{
                      item.mark = '';
                      }
                    
                        var res_courses = compiledCoursesTemplate({item});
                        $('#new_courses_content').append(res_courses);
                    }
                    
                    
                    
                          
                    
                    
           /* بدايه الاسكرول     */
                	 total = result.msg.courses.length;            //************* غير هنا ****************//

                	// Loading flag
        var loading = false;
// Last loaded index
        var lastIndex = $$('#new_courses_content li').length;            //************* غير هنا ****************//
// Max items to load
        var maxItems = total-1;
console.log('total :'+ maxItems);
// Append items per load
        var itemsPerLoad = 10;
// Attach 'infinite' event handler
        $$('.infinite-scroll').on('infinite', function () {
        	 console.log('lastIndex :' + lastIndex);
            // Exit, if loading in progress
            if (loading) return;
            // Set loading flag
            loading = true;
            // Emulate 1s loading
            setTimeout(function () {
                // Reset loading flag
                loading = false;
                console.log('lastIndex :' + lastIndex);
                if (lastIndex >= maxItems) {
                    // Nothing more to load, detach infinite scroll events to prevent unnecessary loadings
                    myApp.detachInfiniteScroll($$('.infinite-scroll'));
                    // Remove preloader
                    $$('.infinite-scroll-preloader').remove();                          //************* انقلها ****************//
                    return;
                }if (lastIndex < maxItems) {
                    // Generate new items HTML
                    var html = '';
                    for (var i = lastIndex + 1; i <= lastIndex + itemsPerLoad; i++) {              //************* غير هنا ****************//
                    	
                     var item = result.msg.courses[i];
                      item.image = item.image ? images_url + item.image : 'img/mylogo1.png';
                      item.mark = '';
                      if(item.new == 0 && item.prop == 1){
                      item.mark = '<img src="img/suggest.png" alt="" class="tag-suggest">';
                      }else if(item.new == 1 && item.prop == 0){
                      item.mark = '<img src="img/new.png" alt="" class="tag-new">';
                      }else if(item.new == 1 && item.prop == 1){
                      item.mark = '<img src="img/new-suggest.png" alt="" class="suggest-new">';
                      }else{
                      item.mark = '';
                      }
                    
                        var res_courses = compiledCoursesTemplate({item});
                        $('#new_courses_content').append(res_courses);
                    }
                }
                // Append new items
              //  $$('.list-block ul').append(html);
                // Update last loaded index
            }, 1000);
            lastIndex = $$('#new_courses_content li').length;                     //************* غير هنا ****************//
            console.log('lastIndex after :' + lastIndex);
        });

                 	
                	
          /* نهايه الاسكرول     */           
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    

                } else {
                	$$('.infinite-scroll-preloader').remove();   
                    $('#new_courses_content').html('<div class="center" style="text-align: center;padding-top: 10%; font-size: 30px;color:brown;">عذراً ...لا توجد نتائج!</div>');
                }

            } else {
            	$$('.infinite-scroll-preloader').remove();   
                $('#new_courses_content').html('<div class="center" style="text-align: center;padding-top: 10%; font-size: 30px;color:brown;">عذراً ...لا توجد نتائج!</div>');
            }


        },
        error: function (jqXHR, textStatus, errorThrown) {
        	$$('.infinite-scroll-preloader').remove();   
            myApp.alert('خطا فى الاتصال', '<b style="color:#e04e4e">تنبيه !</b>');
            myApp.hideIndicator();
        }
    });

});  




myApp.onPageInit('prop-cources', function (page) {

 var coursesTemplate = $('#prop_courses_script').html(); //script
    var compiledCoursesTemplate = Template7.compile(coursesTemplate); //تحضير الاسكربت لاستقبال المتغيرات

    var member = JSON.parse(localStorage.getItem('member'));
    if(member != null && member.member_group_id ==3){
    	var member_id = member.member_id;
    }else{
    	var member_id = 0;
    }
    $.ajax({
        type: "post",
        data:{'member_id':member_id},
        url: url + "prop_courses",
        dataType: "JSON",
        beforeSend: function () {
            myApp.showIndicator();
        },
        success: function (result) {
            myApp.hideIndicator();
            if (result.status == true) {

                if (result.msg.courses != null) {

                    for (var i in result.msg.courses) {
                    	if(i == 10)
                    	break;
                      var item = result.msg.courses[i];
                      item.image = item.image ? images_url + item.image : 'img/mylogo1.png';
                      item.mark = '';
                      if(item.new == 0 && item.prop == 1){
                      item.mark = '<img src="img/suggest.png" alt="" class="tag-suggest">';
                      }else if(item.new == 1 && item.prop == 0){
                      item.mark = '<img src="img/new.png" alt="" class="tag-new">';
                      }else if(item.new == 1 && item.prop == 1){
                      item.mark = '<img src="img/new-suggest.png" alt="" class="suggest-new">';
                      }else{
                      item.mark = '';
                      }
                    
                        var res_courses = compiledCoursesTemplate({item});
                        $('#prop_courses_content').append(res_courses);
                    }
                    
                    
                    
                    
                    
                    
                    
                    
           /* بدايه الاسكرول     */
                	 total = result.msg.courses.length;            //************* غير هنا ****************//

                	// Loading flag
        var loading = false;
// Last loaded index
        var lastIndex = $$('#prop_courses_content li').length;            //************* غير هنا ****************//
// Max items to load
        var maxItems = total-1;
console.log('total :'+ maxItems);
// Append items per load
        var itemsPerLoad = 10;
// Attach 'infinite' event handler
        $$('.infinite-scroll').on('infinite', function () {
        	 console.log('lastIndex :' + lastIndex);
            // Exit, if loading in progress
            if (loading) return;
            // Set loading flag
            loading = true;
            // Emulate 1s loading
            setTimeout(function () {
                // Reset loading flag
                loading = false;
                console.log('lastIndex :' + lastIndex);
                if (lastIndex >= maxItems) {
                    // Nothing more to load, detach infinite scroll events to prevent unnecessary loadings
                    myApp.detachInfiniteScroll($$('.infinite-scroll'));
                    // Remove preloader
                    $$('.infinite-scroll-preloader').remove();
                    return;
                }if (lastIndex < maxItems) {
                    // Generate new items HTML
                    var html = '';
                    for (var i = lastIndex + 1; i <= lastIndex + itemsPerLoad; i++) {              //************* غير هنا ****************//
                    	
                    var item = result.msg.courses[i];
                      item.image = item.image ? images_url + item.image : 'img/mylogo1.png';
                      item.mark = '';
                      if(item.new == 0 && item.prop == 1){
                      item.mark = '<img src="img/suggest.png" alt="" class="tag-suggest">';
                      }else if(item.new == 1 && item.prop == 0){
                      item.mark = '<img src="img/new.png" alt="" class="tag-new">';
                      }else if(item.new == 1 && item.prop == 1){
                      item.mark = '<img src="img/new-suggest.png" alt="" class="suggest-new">';
                      }else{
                      item.mark = '';
                      }
                    
                        var res_courses = compiledCoursesTemplate({item});
                        $('#prop_courses_content').append(res_courses);
                    }
                }
                // Append new items
              //  $$('.list-block ul').append(html);
                // Update last loaded index
            }, 1000);
            lastIndex = $$('#prop_courses_content li').length;                     //************* غير هنا ****************//
            console.log('lastIndex after :' + lastIndex);
        });

                 	
                	
          /* نهايه الاسكرول     */         
                 
                    
                    

                } else {
                	    $$('.infinite-scroll-preloader').remove();
                    $('#prop_courses_content').html('<div class="center" style="text-align: center;padding-top: 10%; font-size: 30px;color:brown;">عذراً ...لا توجد نتائج!</div>');
                }

            } else {
            	    $$('.infinite-scroll-preloader').remove();
                $('#prop_courses_content').html('<div class="center" style="text-align: center;padding-top: 10%; font-size: 30px;color:brown;">عذراً ...لا توجد نتائج!</div>');
            }


        },
        error: function (jqXHR, textStatus, errorThrown) {
            myApp.alert('خطا فى الاتصال', '<b style="color:#e04e4e">تنبيه !</b>');
            myApp.hideIndicator();
        }
    });

});  



/*
$(document).ready(function () {
var member = JSON.parse(localStorage.getItem('member'));

if(member != null){
	mainView.router.loadPage('trainee-cources.html'); 
	$(".is_login").show();
	$(".not_login").hide();
	
	if(member.member_group_id == 1){
		$(".is_center").show();
		$(".is_trainer").hide();
		$(".is_trainee").hide();
		
		// الاشعارات
     $.ajax({
	        type: "post",
	        url: url + "count_nots",
	        data:{'member_id':member.member_id},
	        dataType: "JSON",    
	        success: function (result) {
	        	  $("#count_nots").html(result.msg);
	        	  if(result.msg > 0)
	        	   myApp.alert('<b style="color:#36a5a4;">لديك عدد <div class="chip" style="background-color:#01a686;border-radius: 50%;padding: 12px 8px;"> '+result.msg+'</div> من الإشعارات الجديده</b><br><br><a style="color:#01a686;" href="show-nots.html" ><i class="fa fa-bell"></i>   رؤية الإشعارات  <i class="fa fa-bell"></i></a>','');
	            }               
       });
    	
	}else if(member.member_group_id == 2){
		$(".is_center").hide();
		$(".is_trainer").show();
		$(".is_trainee").hide();
	}else{
		$(".is_center").hide();
		$(".is_trainer").hide();
		$(".is_trainee").show();
	}
			
}else{
	$(".is_login").hide();
	$(".not_login").show();
}


// المؤهلات الدراسيه	
 $.ajax({
        type: "post",
        url: url + "get_education",
        dataType: "JSON",    
        success: function (result) {
            
            if (result.status == true) {
               
            if(result != null && result.msg != null){
         
              localStorage.setItem('education', JSON.stringify(result.msg));
            }}}               
    });
    
    
// التخصصات    
  $.ajax({
        type: "post",
        url: url + "get_specialist",
        dataType: "JSON",    
        success: function (data) {
            
            if (data.status == true) {
               
            if(data != null && data.msg != null){
         
              localStorage.setItem('specialist', JSON.stringify(data.msg));
            }}}               
    });   
    
    
    

});
*/


myApp.onPageInit('forget_password', function (page) {

var member = JSON.parse(localStorage.getItem('member'));


    $('#forget_password_form').submit(function (e) {
        e.preventDefault();

        //myApp.alert('','Ok');  

        $('#email999').css('border', 'none');
        if (!$('#email999').val()) {
            $("#email999").css("border-bottom", "2px solid #01A686");
            $("#email999").focus();
        } else if(member != null){ 
           myApp.alert('يجب تسجيل الخروج من الحساب الحالى اولاً', '<b style="color:#e04e4e">تنبيه !</b>');
        }else {

            // Create a formdata object and add the files
            var formData = new FormData($(this)[0]);
            // var data = JSON.stringify(formData);
            // console.log(data) ;
            $.ajax({
                url: url + "forget_password",
                type: 'POST',
                data: formData,
                cache: false,
                contentType: 'json',
                processData: false, // Don't process the files
                contentType: false, // Set content type to false as jQuery will tell the server its a query string request
                beforeSend: function () {
                    myApp.showPreloader('جارى التحقق من البريد الإلكترونى....');
                },
                success: function (out, textStatus, jqXHR) {
                    out = JSON.parse(out);

                    //console.log(out);
                    // Success so call function to process the form
                    if (out.status == true) {
                        $('#forget_password_form')[0].reset();
                        myApp.hidePreloader();
                       
                        myApp.alert('شكرا لك', '<b style="color:#8bc18b">'+out.msg+'</b>');

                    } else {
                            myApp.hidePreloader();
                            myApp.alert(out.msg, '<b style="color:#e04e4e">تنبيه !</b>');
                    }

                    myApp.hidePreloader();

                },
                error: function (jqXHR, textStatus, errorThrown) {
                    myApp.alert('خطأ فى الاتصال', '<b style="color:#e04e4e">تنبيه !</b>');
                    myApp.hideIndicator();
                }
            });


        }


    });



});






myApp.onPageInit('login', function (page) {

var member = JSON.parse(localStorage.getItem('member'));


    $('#login_form').submit(function (e) {
        e.preventDefault();

        //myApp.alert('','Ok');  

        $('#name2,#password2').css('border', 'none');
        if (!$('#name2').val()) {
            $("#name2").css("border-bottom", "2px solid #01A686");
            $("#name2").focus();
        } else if (!$('#password2').val()) {
            $("#password2").css("border-bottom", "2px solid #01A686");
            $("#password2").focus();
        } else if(member != null){ 
           myApp.alert('يجب تسجيل الخروج من الحساب الحالى اولاً', '<b style="color:#e04e4e">تنبيه !</b>');
        }else {

            // Create a formdata object and add the files
            var formData = new FormData($(this)[0]);
            // var data = JSON.stringify(formData);
            // console.log(data) ;
            $.ajax({
                url: url + "login",
                type: 'POST',
                data: formData,
                cache: false,
                contentType: 'json',
                processData: false, // Don't process the files
                contentType: false, // Set content type to false as jQuery will tell the server its a query string request
                beforeSend: function () {
                    myApp.showPreloader('جارى التحقق من بيانات الحساب....');
                },
                success: function (out, textStatus, jqXHR) {
                    out = JSON.parse(out);

                    //console.log(out);
                    // Success so call function to process the form
                    if (out.status == true) {
                        $('#login_form')[0].reset();
                        myApp.hidePreloader();
                        localStorage.setItem('member', JSON.stringify(out.msg));
                        myApp.alert('مرحبا '+ out.msg.name, '<b style="color:#8bc18b">تم الدخول بنجاح  </b>', function () {
                            myApp.showPreloader('جارى التحميل....');
                         
                                myApp.hidePreloader();
                             //   location.replace("index.html");
                             rendar_home();
                             mainView.router.loadPage('trainee-cources.html'); 
                         

                        });


                    } else {
                        if (out.msg.errors == 'empty') {
                        	myApp.hidePreloader();
                            myApp.alert('بيانات المستخدم مطلوبة', '<b style="color:#e04e4e">تنبيه !</b>');
                        }else if(out.msg.errors == 'ban'){
                        	myApp.hidePreloader();
                            myApp.alert('تم حظر هذا الحساب...من فضلك تواصل مع الإداره لمعرفة المزيد.', '<b style="color:#e04e4e">تنبيه !</b>');    
                        }else if(out.msg.errors == 'code' ){
                        myApp.hidePreloader();
           myApp.prompt('من فضلك أدخل كود التفعيل  ؟', 'تفعيل الحساب', function (value) {

            if (value != '' && value !=null) {

                var post_data = {
                    "code": value,
                    "member": out.msg.m_data
                }

                $.ajax({
                    type: "post",
                    url: url + "active_acc",
                    data: post_data,
                    dataType: "JSON",
                    beforeSend: function () {
                        myApp.showIndicator();
                    },
                    success: function (result) {
                        //console.log(result);
                        myApp.hideIndicator();
                        if (result.status == 1) {
                           $('#login_form')[0].reset();
                            var member_new = result.msg;
                            localStorage.setItem('member', JSON.stringify(member_new));
                            
                            myApp.alert('شكرا لك', '<b style="color:#8bc18b">تم التفعيل بنجاح</b>', function () {
                            myApp.showPreloader('جارى التحميل....');
                         
                                myApp.hidePreloader();
                             //   location.replace("index.html");
                             rendar_home();
                             mainView.router.loadPage('trainee-cources.html'); 
                         

                        });
                                                
                        } else if(result.status == 2){
                            myApp.alert('من فضلك ادخل كود التفعيل الوارد لديك فى البريد الإلكترونى الخاص بك', '<b style="color:#e04e4e">كود التفعيل غير صحيح</b>');
                        }else if(result.status == 3){
                           myApp.alert(' من فضلك أدخل كود التفعيل', '<b style="color:#e04e4e">تنبيه !</b>');
                        }else{
                        	 myApp.alert(' من فضلك قم بتسجيل الدخول من جديد', '<b style="color:#e04e4e">تنبيه !</b>');
                        }

                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        myApp.alert('خطا فى الاتصال', '<b style="color:#e04e4e">خطأ</b>');
                        myApp.hideIndicator();
                    }

                });


            } else {
                myApp.alert(' من فضلك أدخل كود التفعيل', '<b style="color:#e04e4e">تنبيه !</b>');
            }


        });

                        
                        
                        
                        } else {
                            myApp.hidePreloader();
                            myApp.alert('بيانات مستخدم غير صحيحة', '<b style="color:#e04e4e">خطأ</b>');
                        }
                    }

                    myApp.hidePreloader();

                },
                error: function (jqXHR, textStatus, errorThrown) {
                    myApp.alert('خطا فى الاتصال', '<b style="color:#e04e4e">خطأ</b>');
                    myApp.hideIndicator();
                }
            });


        }


    });



});


function log_out() {

    var member = localStorage.getItem('member');
    if (member != null) {
        myApp.confirm('', ' تسجيل الخروج من  الحساب ؟', function () {

            // myApp.showPreloader('جارى تسجيل الخروج من الحساب....');
            // myApp.hidePreloader();
                            
            myApp.alert('شكرا لك', '<b style="color:#8bc18b">تم الخروج بنجاح</b>', function () {
                localStorage.removeItem('member');
                myApp.showPreloader('جارى التحميل ....');
            
                    myApp.hidePreloader();
                    // location.replace("index.html");
                    rendar_home();
                  mainView.router.loadPage('index.html'); 

            });
        });

    } else {
        myApp.alert('لم تقم بتسجيل الدخول بعد ', '<b style="color:#e04e4e">خطأ !</b>');
    }

}


/////////////////////////////////Traineeee /////////////////////////////////////////////////////

myApp.onPageInit('signup-trainee', function (page) {

var edu = JSON.parse(localStorage.getItem('education'));

for(var i in edu){
              	
              	var item = edu[i];
              	
              	$("#select_edu").append('<option value="'+item.id+'">'+item.title+'</option>');
              	
              }



var country = JSON.parse(localStorage.getItem('country'));

for(var i in country){ 
              	
              	var item = country[i];
              	if(item.id == 1)
              	continue;
              	
              	$(".country_select").append('<option value="'+item.id+'">'+item.arabic+'</option>');
              	
            }  


var city = JSON.parse(localStorage.getItem('city'));

for(var i in city){
              	
              	var item = city[i];
              	if(item.arabic == '' || item.arabic == null)
              	continue;
              	  
              	if(i == 0)
              	 $(".city_select").append('<option value="'+item.id+'" selected>'+item.arabic+'</option>'); 
              	else 
              	 $(".city_select").append('<option value="'+item.id+'">'+item.arabic+'</option>');
              	
            } 


    $('#signup_trainee_form').submit(function (e) {
        e.preventDefault();
        //myApp.alert('','Ok');  
        $('#username, #name ,#email , #password , #confirm_password , #address , #phone, #city').css('border', 'none');
        if (!$('#name').val()) {
            $("#name").css("border-bottom", "2px solid #01A686 ");
            $("#name").focus();
        } else if (!$('#username').val()) {
            $("#username").css("border-bottom", "2px solid #01A686");
            $("#username").focus();
        } else if (!$('#email').val()) {
            $("#email").css("border-bottom", "2px solid #01A686");
            $("#email").focus();
        } else if (!$('#password').val()) {
            $("#password").css("border-bottom", "2px solid #01A686");
            $("#password").focus();
        } else if (!$('#confirm_password').val()) {
            $("#confirm_password").css("border-bottom", "2px solid #01A686");
            $("#confirm_password").focus();
        } else if($("#sure").prop('checked') == false){
         myApp.alert('يجب الموافقه على سياسة الخصوصية', '<b style="color:#e04e4e">خطأ</b>');
        } else {


            // Create a formdata object and add the files
            var formData = new FormData($(this)[0]);
            // var data = JSON.stringify(formData);
            //console.log(data) ;
            $.ajax({
                url: url + "register",
                type: 'POST',
                data: formData,
                cache: false,
                contentType: 'json',
                processData: false, // Don't process the files
                contentType: false, // Set content type to false as jQuery will tell the server its a query string request
                beforeSend: function () {
                    myApp.showIndicator();
                },
                success: function (out, textStatus, jqXHR) {
                    out = JSON.parse(out);

                    // console.log(out);
                    //<b style="color:green">تم التسجيل بنجاح</b>
                    // Success so call function to process the form

                    if (out.status == true) {
                        $('#signup_trainee_form')[0].reset();
                        myApp.alert(' شكرا لك', '<b style="color:#8bc18b">'+out.msg.return_msg+'</b>');

                    } else {
                        //console.log(out.msg) ;
                        myApp.alert(out.msg, '<b style="color:#e04e4e">خطأ</b>');
                    }

                    myApp.hideIndicator();

                },
                error: function (jqXHR, textStatus, errorThrown) {
                    myApp.alert('خطا فى الاتصال', '<b style="color:#e04e4e">خطأ</b>');
                    myApp.hideIndicator();
                }
            });

        }

    });


});



myApp.onPageInit('profile-trainee', function (page) {

    var data = JSON.parse(localStorage.getItem('member'));
    
    
    
    if (data != null) {
        if (data.image == '')
            data.image = 'img/mylogo1.png';
        else
            data.image = images_url + data.image;
           
        data.female = data.male = '';
           
        if(data.gender == 'female'){    
            data.female = '<input type="radio" name="gender" value="female" checked="checked">';
            data.male = '<input type="radio" name="gender" value="male">';
        }else{
            data.male = '<input type="radio" name="gender" value="male" checked="checked">';
            data.female = '<input type="radio" name="gender" value="female">';
        }
        var profileTemplate = $('#profile_trainee_script').html(); //script
        var compiledProfileTemplate = Template7.compile(profileTemplate); //تحضير الاسكربت لاستقبال المتغيرات
        var result = compiledProfileTemplate({data}); // ارسال المتغير


        $('#profile_trainee_content').html(result);
        
    var edu = JSON.parse(localStorage.getItem('education'));

    for(var i in edu){
              	
  	var item = edu[i];
  	if(data.education_id == item.id)
  	$("#select_edu2").append('<option value="'+item.id+'" selected>'+item.title+'</option>');
    else
    $("#select_edu2").append('<option value="'+item.id+'">'+item.title+'</option>');
              	
           }
           
           
       var country = JSON.parse(localStorage.getItem('country'));

for(var i in country){ 
              	
              	var item = country[i];
              	if(item.id == 1)
              	continue;
              	
              	if(data.country_id == item.id)
              	$(".country_select").append('<option value="'+item.id+'" selected>'+item.arabic+'</option>');
              	else
              	$(".country_select").append('<option value="'+item.id+'">'+item.arabic+'</option>');
              	
            }  


var city = JSON.parse(localStorage.getItem('city'));

for(var i in city){
              	
              	var item = city[i];
              	if(item.arabic == '' || item.arabic == null)
              	continue;
              	   
              	if(data.city_id == item.id)
              	 $(".city_select").append('<option value="'+item.id+'" selected>'+item.arabic+'</option>');
              	else 
              	 $(".city_select").append('<option value="'+item.id+'">'+item.arabic+'</option>');
              	
            }      
           ///Change Password ///
           
           
      
    var change_passwordTemplate = $('#change_password_script').html(); //script
    var compiledChange_passwordTemplate = Template7.compile(change_passwordTemplate); //تحضير الاسكربت لاستقبال المتغيرات
    var result2 = compiledChange_passwordTemplate({
        data
    }); // ارسال المتغير


    $('#change_password_content').html(result2);     
           
        
    }

});


/////////////////////////////////Trainer /////////////////////////////////////////////////////


myApp.onPageInit('signup-trainer', function (page) {

var edu = JSON.parse(localStorage.getItem('education'));

var spec = JSON.parse(localStorage.getItem('specialist'));

for(var i in edu){
              	
              	var item = edu[i];
              	
              	$("#select_edu3").append('<option value="'+item.id+'">'+item.title+'</option>');
              	
              }
              
for(var i in spec){
              	
              	var item = spec[i];
              	
              	$("#select_spec").append('<option value="'+item.id+'">'+item.title+'</option>');
              	
              }              
              

var country = JSON.parse(localStorage.getItem('country'));

for(var i in country){ 
              	
              	var item = country[i];
              	if(item.id == 1)
              	continue;
              	
              	$(".country_select").append('<option value="'+item.id+'">'+item.arabic+'</option>');
              	
            }  


var city = JSON.parse(localStorage.getItem('city'));

for(var i in city){
              	
              	var item = city[i];
              	if(item.arabic == '' || item.arabic == null)
              	continue;
              	  
              	if(i == 0)
              	 $(".city_select").append('<option value="'+item.id+'" selected>'+item.arabic+'</option>'); 
              	else 
              	 $(".city_select").append('<option value="'+item.id+'">'+item.arabic+'</option>');
              	
            } 

    $('#signup_trainer_form').submit(function (e) {
        e.preventDefault();
        //myApp.alert('','Ok');  
        $('#username44 , #name44 ,#email44 , #password44 , #confirm_password44 , #phone44,#city44').css('border', 'none');
        if (!$('#name44').val()) {
            $("#name44").css("border-bottom", "2px solid #01A686 ");
            $("#name44").focus();
        } else if (!$('#username44').val()) {
            $("#username44").css("border-bottom", "2px solid #01A686");
            $("#username44").focus();
        } else if (!$('#email44').val()) {
            $("#email44").css("border-bottom", "2px solid #01A686");
            $("#email44").focus();
        } else if (!$('#password44').val()) {
            $("#password44").css("border-bottom", "2px solid #01A686");
            $("#password44").focus();
        } else if (!$('#confirm_password44').val()) {
            $("#confirm_password44").css("border-bottom", "2px solid #01A686");
            $("#confirm_password44").focus();
        }else if ( document.getElementById("cv").files.length == 0 ) {
            myApp.alert('من فضلك ارفق ملف السيره الذاتيه الخاص بك', '<b style="color:#e04e4e">تنبيه !</b>');
        }else if($("#sure44").prop('checked') == false){
         myApp.alert('يجب الموافقه على سياسة الخصوصية', '<b style="color:#e04e4e">تنبيه !</b>');
        } else {


            // Create a formdata object and add the files
            var formData = new FormData($(this)[0]);
            // var data = JSON.stringify(formData);
            //console.log(data) ;
            $.ajax({
                url: url + "register_trainer",
                type: 'POST',
                data: formData,
                cache: false,
                contentType: 'json',
                processData: false, // Don't process the files
                contentType: false, // Set content type to false as jQuery will tell the server its a query string request
                beforeSend: function () {
                    myApp.showIndicator();
                },
                success: function (out, textStatus, jqXHR) {
                    out = JSON.parse(out);

                    // console.log(out);
                    //<b style="color:green">تم التسجيل بنجاح</b>
                    // Success so call function to process the form

                    if (out.status == true) {
                        $('#signup_trainer_form')[0].reset();
                        myApp.alert(' شكرا لك', '<b style="color:#8bc18b">'+out.msg.return_msg+'</b>');

                    } else {
                        //console.log(out.msg) ;
                        myApp.alert(out.msg, '<b style="color:#e04e4e">خطأ</b>');
                    }

                    myApp.hideIndicator();

                },
                error: function (jqXHR, textStatus, errorThrown) {
                    myApp.alert('خطا فى الاتصال', '<b style="color:#e04e4e">خطأ</b>');
                    myApp.hideIndicator();
                }
            });

        }

    });


});



myApp.onPageInit('profile-trainer', function (page) {

    var data = JSON.parse(localStorage.getItem('member'));
    
    
    if (data != null) {
        if (data.image == '')
            data.image = 'img/mylogo1.png';
        else
            data.image = images_url + data.image;
            
         if (data.cv == '')
            data.cv = 'img/mylogo1.png';
        else
            data.cv = images_url + data.cv;    
           
        data.female = data.male = '';
           
        if(data.gender == 'female'){    
            data.female = '<input type="radio" name="gender" value="female" checked="checked">';
            data.male = '<input type="radio" name="gender" value="male">';
        }else{
            data.male = '<input type="radio" name="gender" value="male" checked="checked">';
            data.female = '<input type="radio" name="gender" value="female">';
        }
        var profileTemplate = $('#profile_trainer_script').html(); //script
        var compiledProfileTemplate = Template7.compile(profileTemplate); //تحضير الاسكربت لاستقبال المتغيرات
        var result = compiledProfileTemplate({data}); // ارسال المتغير


        $('#profile_trainer_content').html(result);
        
    var edu = JSON.parse(localStorage.getItem('education'));

    for(var i in edu){
              	
  	var item = edu[i];
  	if(data.education_id == item.id)
  	$("#select_edu33").append('<option value="'+item.id+'" selected>'+item.title+'</option>');
    else
    $("#select_edu33").append('<option value="'+item.id+'">'+item.title+'</option>');
              	
           }
    
    
    var spec = JSON.parse(localStorage.getItem('specialist'));

    for(var s in spec){
              	
  	var item_i = spec[s];
  	if(data.specialist_id == item_i.id)
  	$("#select_spec2").append('<option value="'+item_i.id+'" selected>'+item_i.title+'</option>');
    else
    $("#select_spec2").append('<option value="'+item_i.id+'">'+item_i.title+'</option>');
              	
           }       
           
   var country = JSON.parse(localStorage.getItem('country'));

for(var i in country){ 
              	
              	var item = country[i];
              	if(item.id == 1)
              	continue;
              	
              	if(data.country_id == item.id)
              	$(".country_select").append('<option value="'+item.id+'" selected>'+item.arabic+'</option>');
              	else
              	$(".country_select").append('<option value="'+item.id+'">'+item.arabic+'</option>');
              	
            }  


var city = JSON.parse(localStorage.getItem('city'));

for(var i in city){
              	
              	var item = city[i];
              	if(item.arabic == '' || item.arabic == null)
              	continue;
              	   
              	if(data.city_id == item.id)
              	 $(".city_select").append('<option value="'+item.id+'" selected>'+item.arabic+'</option>');
              	else 
              	 $(".city_select").append('<option value="'+item.id+'">'+item.arabic+'</option>');
              	
            } 
      
      
      
           
           ///Change Password ///
           
           
      
    var change_passwordTemplate = $('#change_password_script').html(); //script
    var compiledChange_passwordTemplate = Template7.compile(change_passwordTemplate); //تحضير الاسكربت لاستقبال المتغيرات
    var result2 = compiledChange_passwordTemplate({data}); // ارسال المتغير


    $('#change_password_content').html(result2);     
           
        
    }

});




myApp.onPageInit('add-trainer-courses', function (page) {
	
	var data = JSON.parse(localStorage.getItem('member'));
	
	
	var Template = $('#add_trainer_courses_script').html(); //script
    var compiledTemplate = Template7.compile(Template); //تحضير الاسكربت لاستقبال المتغيرات
    var result = compiledTemplate({data}); // ارسال المتغير


    $('#add_trainer_courses_content').html(result);     
           
        
    
	
});



   


function get_trainer_course(id){

var Template = $('#edit_trainer_courses_script').html(); //script
var compiledTemplate = Template7.compile(Template); //تحضير الاسكربت لاستقبال المتغيرات

	
   $.ajax({
        type: "POST",
        data:{'id':id},
        url: url + "get_trainer_course",
        dataType: "JSON",
        beforeSend: function () {
            myApp.showIndicator();
        },
        success: function (data) {
           
            if(data != null && data.msg != null){
            	//console.log(data);
             data.msg.image = data.msg.image ? images_url + data.msg.image : 'img/mylogo1.png';
           	 if(data.msg.active == 1){
           	 	data.msg.is_active = '<input type="radio" name="active" value="1" checked="checked">';
           	 	data.msg.not_active = '<input type="radio" name="active" value="0" >';
           	 }else{
           	 	data.msg.is_active = '<input type="radio" name="active" value="1" >';
           	 	data.msg.not_active = '<input type="radio" name="active" value="0" checked="checked">';
           	 }
           	 data = data.msg;
           	 var result = compiledTemplate({data});
           	 
           	 $('#edit_trainer_courses_content').html(result);
            }else{
            $('#edit_trainer_courses_content').html('<div class="center" style="text-align: center;padding-top: 10%; font-size: 30px;color:brown;">عذراً ...لا توجد نتائج!</div>');	
            }
            
            myApp.hideIndicator();

        },
        error: function (jqXHR, textStatus, errorThrown) {
            myApp.alert('خطأ فى الاتصال', '<b style="color:#e04e4e">خطأ</b>');
            myApp.hideIndicator();
        }
    });
           
        
    
}	





myApp.onPageInit('trainer-courses', function (page) {
	
    var coursesTemplate = $('#trainer_courses_script').html(); //script
    var compiledCoursesTemplate = Template7.compile(coursesTemplate); //تحضير الاسكربت لاستقبال المتغيرات

    var member = JSON.parse(localStorage.getItem('member'));
    
    if(member != null){
    
    $.ajax({
        type: "post",
        data:{'id':member.member_id},
        url: url + "trainer_courses",
        dataType: "JSON",
        beforeSend: function () {
            myApp.showIndicator();
        },
        success: function (result) {
            myApp.hideIndicator();
            if (result.status == true) {

                if (result.msg != null && result.msg.courses != null) {

                    for (var i in result.msg.courses) {
                      var item = result.msg.courses[i];
                      item.image = item.image ? images_url + item.image : 'img/mylogo1.png';
                      
                        var res_courses = compiledCoursesTemplate({item});
                        $('#trainer_courses_content').append(res_courses);
                    }

                } else {
                    $('#trainer_courses_content').html('<div class="center" style="text-align: center;padding-top: 10%; font-size: 30px;color:brown;">عذراً ...لا توجد نتائج!</div>');
                }

            } else {
                $('#trainer_courses_content').html('<div class="center" style="text-align: center;padding-top: 10%; font-size: 30px;color:brown;">عذراً ...لا توجد نتائج!</div>');
            }


        },
        error: function (jqXHR, textStatus, errorThrown) {
            myApp.alert('خطا فى الاتصال', '<b style="color:#e04e4e">تنبيه !</b>');
            myApp.hideIndicator();
        }
    });
    }else{
    	$('#trainer_courses_content').html('<div class="center" style="text-align: center;padding-top: 10%; font-size: 30px;color:brown;">عذراً ...لا توجد نتائج!</div>');
    }

});


function delete_trainer_corse(id){
	
	var member = JSON.parse(localStorage.getItem('member'));
	
	myApp.confirm(' هل تريد حذف هذه الدوره ؟','<b style="color:#e04e4e">تنبيه !</b>', function () {
	$.ajax({
        type: "post",
        data:{'id':id , 'member_id':member.member_id},
        url: url + "delete_trainer_course",
        dataType: "JSON",
        beforeSend: function () {
            myApp.showIndicator();
        },
        success: function (result) {
            myApp.hideIndicator();
            if (result.status == true) {

                if (result.msg != null && result.msg.courses != null) {
                	
                 var coursesTemplate = $('#trainer_courses_script').html(); //script
                 var compiledCoursesTemplate = Template7.compile(coursesTemplate); //تحضير الاسكربت لاستقبال المتغيرات
                  $('#trainer_courses_content').html('');
                    for (var i in result.msg.courses) {
                      var item = result.msg.courses[i];
                      item.image = item.image ? images_url + item.image : 'img/mylogo1.png';
                      
                        var res_courses = compiledCoursesTemplate({item});
                        $('#trainer_courses_content').append(res_courses);
                    }
                    
                   myApp.alert(' شكرا لك', '<b style="color:#8bc18b">تم الحذف بنجاح</b>');


                } else {
                    $('#trainer_courses_content').html('<div class="center" style="text-align: center;padding-top: 10%; font-size: 30px;color:brown;">عذراً ...لا توجد نتائج!</div>');
                }

            } else {
                $('#trainer_courses_content').html('<div class="center" style="text-align: center;padding-top: 10%; font-size: 30px;color:brown;">عذراً ...لا توجد نتائج!</div>');
            }


        },
        error: function (jqXHR, textStatus, errorThrown) {
            myApp.alert('خطا فى الاتصال', '<b style="color:#e04e4e">تنبيه !</b>');
            myApp.hideIndicator();
        }
    });
	
});
	
}


function do_seen(id){
	
	$.ajax({
        type: "post",
        url: url + "do_seen",
        data:{'id':id},
        dataType: "JSON",
       success: function (result) {
           
        },
        error: function (jqXHR, textStatus, errorThrown) {
            myApp.alert('خطا فى الاتصال', '<b style="color:#e04e4e">تنبيه !</b>');
           
        }
    });
	
}





function delete_readed_nots(){
	
	var member = JSON.parse(localStorage.getItem('member'));
    var Template = $('#not_script').html(); //script
    var compiledTemplate = Template7.compile(Template); //تحضير الاسكربت لاستقبال المتغيرات

	myApp.confirm(' هل تريد حذف الإشعارات المقروئه ؟','<b style="color:#e04e4e">تنبيه !</b>', function () {
	$.ajax({
        type: "post",
        data:{'member_id':member.member_id},
        url: url + "delete_readed_nots",
        dataType: "JSON",
        beforeSend: function () {
            myApp.showIndicator();
        },
         success: function (result) {
            myApp.hideIndicator();
            if (result.status == true) {

                if (result.msg != null) {
                	$('#del_btn').show();
                    $('#not_content').html('');
                    for (var i in result.msg) {
                      var item = result.msg[i];
                      if(item.seen == 1)
                      item.seen = 'background-color:#d5edee;';
                      var res_nots = compiledTemplate({item});
                        $('#not_content').append(res_nots);
                    }

                } else {
                    $('#not_content').html('<div class="center" style="text-align: center;padding-top: 10%; font-size: 30px;color:brown;">ليس لديك اشعارات!</div>');
                    $('#del_btn').hide();
                }

            } else {
                $('#not_content').html('<div class="center" style="text-align: center;padding-top: 10%; font-size: 30px;color:brown;"> ليس لديك اشعارات!</div>');
                $('#del_btn').hide();
            }


        },
        error: function (jqXHR, textStatus, errorThrown) {
            myApp.alert('خطا فى الاتصال', '<b style="color:#e04e4e">تنبيه !</b>');
            myApp.hideIndicator();
        }
    });
	
});
	
}



function delete_all_nots(){
	
	var member = JSON.parse(localStorage.getItem('member'));
    var Template = $('#not_script').html(); //script
    var compiledTemplate = Template7.compile(Template); //تحضير الاسكربت لاستقبال المتغيرات

	myApp.confirm(' هل تريد حذف جميع الإشعارات ؟','<b style="color:#e04e4e">تنبيه !</b>', function () {
	$.ajax({
        type: "post",
        data:{'member_id':member.member_id},
        url: url + "delete_all_nots",
        dataType: "JSON",
        beforeSend: function () {
            myApp.showIndicator();
        },
         success: function (result) {
          
            if (result.status == true) {

                if (result.msg != null) {
                	$('#del_btn').show();
                    $('#not_content').html('');
                    for (var i in result.msg) {
                      var item = result.msg[i];
                      if(item.seen == 1)
                      item.seen = 'background-color:#d5edee;';
                      var res_nots = compiledTemplate({item});
                        $('#not_content').append(res_nots);
                    }

                } else {
                	$('#del_btn').hide();
                    $('#not_content').html('<div class="center" style="text-align: center;padding-top: 10%; font-size: 30px;color:brown;">ليس لديك اشعارات!</div>');
                    
                }

            } else {
            	$('#del_btn').hide();
                $('#not_content').html('<div class="center" style="text-align: center;padding-top: 10%; font-size: 30px;color:brown;"> ليس لديك اشعارات!</div>');
                
            }
            myApp.hideIndicator();

        },
        error: function (jqXHR, textStatus, errorThrown) {
            myApp.alert('خطا فى الاتصال', '<b style="color:#e04e4e">تنبيه !</b>');
            myApp.hideIndicator();
        }
    });
	
});
	
}



myApp.onPageInit('show-nots', function (page) {
	
    var Template = $('#not_script').html(); //script
    var compiledTemplate = Template7.compile(Template); //تحضير الاسكربت لاستقبال المتغيرات

    var member = JSON.parse(localStorage.getItem('member'));
    
    if(member != null  && member.member_group_id == 1){
    
    $.ajax({
        type: "post",
        url: url + "show_nots",
        data:{'member_id':member.member_id},
        dataType: "JSON",
        beforeSend: function () {
            myApp.showIndicator();
        },
        success: function (result) {
            myApp.hideIndicator();
            if (result.status == true) {

                if (result.msg != null) {
                    $('#del_btn').show();
                    $('#not_content').html('');
                    for (var i in result.msg) {
                      var item = result.msg[i];
                      if(item.seen == 1)
                      item.seen = 'background-color:#d5edee;';
                      var res_nots = compiledTemplate({item});
                        $('#not_content').append(res_nots);
                    }

                } else {
                    $('#del_btn').hide();
                    $('#not_content').html('<div class="center" style="text-align: center;padding-top: 10%; font-size: 30px;color:brown;">ليس لديك اشعارات!</div>');
                   
                }

            } else {
                $('#del_btn').hide(); 
                $('#not_content').html('<div class="center" style="text-align: center;padding-top: 10%; font-size: 30px;color:brown;"> ليس لديك اشعارات!</div>');
                 
            }

          myApp.hideIndicator();
        },
        error: function (jqXHR, textStatus, errorThrown) {
            myApp.alert('خطا فى الاتصال', '<b style="color:#e04e4e">تنبيه !</b>');
            myApp.hideIndicator();
        }
    });
    }else{
    	 $('#del_btn').hide(); 
    	 $('#not_content').html('<div class="center" style="text-align: center;padding-top: 10%; font-size: 30px;color:brown;">ليس لديك اشعارات!</div>');
         myApp.hideIndicator();
    }

});



function show_trainers(){
	
	var Template = $('#trainers_script').html(); //script
    var compiledTemplate = Template7.compile(Template); //تحضير الاسكربت لاستقبال المتغيرات
  $('#trainers_content').html('');
    var member = JSON.parse(localStorage.getItem('member'));
    
	if(member != null){
    
    $.ajax({
        type: "post",
        url: url + "show_trainers",
        data:{'search':$('#search_trainers').val(),'select':$('#trainer_spec').val()},
        dataType: "JSON",
        beforeSend: function () {
            myApp.showIndicator();
        }, 
        success: function (result) { 
            myApp.hideIndicator();
            if (result.status == true) {

                if (result.msg != null && result.msg.trainers != null) {
                	
                	 $('#trainers_content').html('');
                	for (var i in result.msg.trainers) {
                    	  if (i == 10)         // limit 10 row
                break;

                      var item = result.msg.trainers[i];
                      item.image = item.image ? images_url + item.image : 'img/person.png';
                      
                        var res_trainers = compiledTemplate({item});
                        $('#trainers_content').append(res_trainers);
                    }
                	
                	 /* بدايه الاسكرول     */
                	 total = result.msg.trainers.length;

                	// Loading flag
        var loading = false;
// Last loaded index
        var lastIndex = $$('#trainers_content li').length;
// Max items to load
        var maxItems = total-1;
console.log('total :'+ maxItems);
// Append items per load
        var itemsPerLoad = 10;
// Attach 'infinite' event handler
        $$('.infinite-scroll').on('infinite', function () {
        	 console.log('lastIndex :' + lastIndex);
            // Exit, if loading in progress
            if (loading) return;
            // Set loading flag
            loading = true;
            // Emulate 1s loading
            setTimeout(function () {
                // Reset loading flag
                loading = false;
                console.log('lastIndex :' + lastIndex);
                if (lastIndex >= maxItems) {
                    // Nothing more to load, detach infinite scroll events to prevent unnecessary loadings
                    myApp.detachInfiniteScroll($$('.infinite-scroll'));
                    // Remove preloader
                    $$('.infinite-scroll-preloader').remove();
                    return;
                }if (lastIndex < maxItems) {
                    // Generate new items HTML
                    var html = '';
                    for (var i = lastIndex + 1; i <= lastIndex + itemsPerLoad; i++) {
                    	
                         var item = result.msg.trainers[i];
                      item.image = item.image ? images_url + item.image : 'img/person.png';
                      
                        var res_trainers = compiledTemplate({item});
                        $('#trainers_content').append(res_trainers);
                        
                    }
                }
                // Append new items
              //  $$('.list-block ul').append(html);
                // Update last loaded index
            }, 1000);
            lastIndex = $$('#trainers_content li').length;
            console.log('lastIndex after :' + lastIndex);
        });

                 	
                	
          /* نهايه الاسكرول     */
               
                } else {
                	$$('.infinite-scroll-preloader').remove();
                    $('#trainers_content').html('<div class="center" style="text-align: center;padding-top: 10%; font-size: 30px;color:brown;">عذراً ...لا توجد نتائج!</div>');
                }

            } else {
            	$$('.infinite-scroll-preloader').remove();
                $('#trainers_content').html('<div class="center" style="text-align: center;padding-top: 10%; font-size: 30px;color:brown;">عذراً ...لا توجد نتائج!</div>');
            }


        },
        error: function (jqXHR, textStatus, errorThrown) {
            myApp.alert('خطا فى الاتصال', '<b style="color:#e04e4e">تنبيه !</b>');
            myApp.hideIndicator();
        }
    });
    }else{
    	$$('.infinite-scroll-preloader').remove();
    	$('#trainers_content').html('<div class="center" style="text-align: center;padding-top: 10%; font-size: 30px;color:brown;">عذراً ...لا توجد نتائج!</div>');
    }
	
}







myApp.onPageInit('show-trainers', function (page) {
	
	var spec = JSON.parse(localStorage.getItem('specialist'));
	
	for(var i in spec){
              	
              	var item = spec[i];
              	
              	$("#trainer_spec").append('<option value="'+item.id+'">'+item.title+'</option>');
              	
              } 
	
   show_trainers();
    

});



var course_rank_id = 0;
var course_rank_value = 0;
 function open_rate_course(course_id ){
  	course_rank_id = course_id;
     course_rank_value = 0;

  	   myApp.popup('#rank_course');
  }
  function rank_rate_val(val){
      course_rank_value = val;

  }

 function rank_course(){
     var value = course_rank_value;//$('input[name="rating_course"]').val();
  	 var member = JSON.parse(localStorage.getItem('member'));
    
    if(member != null){
    
    $.ajax({
        type: "get",
        data:{'member_id':member.member_id,'course_rank_id':course_rank_id,'value':value},
        url: url + "rank_course",
        dataType: "JSON",
        beforeSend: function () {
            myApp.showIndicator();
        },
        success: function (result) {
            myApp.hideIndicator();
            if(result.status == true)
            myApp.alert(' شكرا لك', '<b style="color:#8bc18b">تم التقييم بنجاح</b>');
            else
            myApp.alert(result.msg, '<b style="color:#e04e4e">تنبيه !</b>');

            myApp.closeModal('#rank_course');
        },
        error: function (jqXHR, textStatus, errorThrown) {
            myApp.alert('خطأ فى الاتصال', '<b style="color:#e04e4e">تنبيه !</b>');
            myApp.hideIndicator();
            myApp.closeModal('#rank_coures'); 
        }
    });
    } 
	
  	
  	
  }
var member_rank_id = 0;

function open_rate(member_id ){
    member_rank_id = member_id;
    rank_member_value = 0;
    myApp.popup('#rank_member');
}

var  rank_member_value = 0;
function rank_member_val(val){
    rank_member_value = val;
}
  
  function rank(){
  	var value = rank_member_value; //$('input[name="rating"]').val();
  	 var member = JSON.parse(localStorage.getItem('member'));
    
    if(member != null){
    
    $.ajax({
        type: "get",
        data:{'member_id':member.member_id,'member_rank_id':member_rank_id,'value':value},
        url: url + "rank_member",
        dataType: "JSON",
        beforeSend: function () {
            myApp.showIndicator();
        },
        success: function (result) {
            myApp.hideIndicator();
            if(result.status == true)
            myApp.alert(' شكرا لك', '<b style="color:#8bc18b">تم التقييم بنجاح</b>');
            else
            myApp.alert(result.msg, '<b style="color:#e04e4e">تنبيه !</b>');

            myApp.closeModal('#rank_member');
        },
        error: function (jqXHR, textStatus, errorThrown) {
            myApp.alert('خطأ فى الاتصال', '<b style="color:#e04e4e">تنبيه !</b>');
            myApp.hideIndicator();
            myApp.closeModal('#rank_member'); 
        }
    });
    } 
	
  	
  	
  }





function trainer_info(id){



var Template = $('#trainer_info_script').html(); //script
var compiledTemplate = Template7.compile(Template); //تحضير الاسكربت لاستقبال المتغيرات

	
   $.ajax({
        type: "POST",
        data:{'member_id':id},
        url: url + "trainer_info",
        dataType: "JSON",
        beforeSend: function () {
            myApp.showIndicator();
        },
        success: function (data) {
           
            if(data != null && data.msg != null){
            	//console.log(data);
             data.msg.image = data.msg.image ? images_url + data.msg.image : 'img/person.png';
              
           	 data.msg.cv = data.msg.cv ? images_url + data.msg.cv : null;
           	
           	 data = data.msg;
           	 
           	 
           	 
           	 var result = compiledTemplate({data});
           	 
           	$('#trainer_info_content').html(result);
           	
           	var member = JSON.parse(localStorage.getItem('member'));
    
    if(member == null || member.member_group_id != 3){
    $("#rate_act").hide();
    }	
           	
            if(data.phone == null)
           	$('#trainer_info_phone').hide();
           	
           	if(data.city_name == null)
           	$('#trainer_info_city').hide();
           	
           	if(data.country_name == null)
           	$('#trainer_info_country').hide();
           	
           	if(data.cv == null)
           	$("#trainer_cv").hide();
           	
            }else{
            $('#trainer_info_content').html('<div class="center" style="text-align: center;padding-top: 10%; font-size: 30px;color:brown;">عذراً ...لا توجد نتائج!</div>');	
            }
            
            myApp.hideIndicator();

        },
        error: function (jqXHR, textStatus, errorThrown) {
            myApp.alert('خطأ فى الاتصال', '<b style="color:#e04e4e">خطأ</b>');
            myApp.hideIndicator();
        }
    });
           
        
    
}	




function trainer_courses_show(id){
		
    var coursesTemplate = $('#trainer_courses_show_script').html(); //script
    var compiledCoursesTemplate = Template7.compile(coursesTemplate); //تحضير الاسكربت لاستقبال المتغيرات

    var member = JSON.parse(localStorage.getItem('member'));
    
    if(member != null){
    
    $.ajax({
        type: "post",
        data:{'id':id},
        url: url + "trainer_courses_show",
        dataType: "JSON",
        beforeSend: function () {
            myApp.showIndicator();
        },
        success: function (result) {
            myApp.hideIndicator();
            if (result.status == true) {

                if (result.msg != null && result.msg.courses != null) {

                    for (var i in result.msg.courses) {
                      var item = result.msg.courses[i];
                      item.image = item.image ? images_url + item.image : 'img/mylogo1.png';
                      
                        var res_courses = compiledCoursesTemplate({item});
                        $('#trainer_courses_show_content').append(res_courses);
                    }

                } else {
                    $('#trainer_courses_show_content').html('<div class="center" style="text-align: center;padding-top: 10%; font-size: 30px;color:brown;">عذراً ...لا توجد نتائج!</div>');
                }

            } else {
                $('#trainer_courses_show_content').html('<div class="center" style="text-align: center;padding-top: 10%; font-size: 30px;color:brown;">عذراً ...لا توجد نتائج!</div>');
            }


        },
        error: function (jqXHR, textStatus, errorThrown) {
            myApp.alert('خطا فى الاتصال', '<b style="color:#e04e4e">تنبيه !</b>');
            myApp.hideIndicator();
        }
    });
    }else{
    	$('#trainer_courses_content').html('<div class="center" style="text-align: center;padding-top: 10%; font-size: 30px;color:brown;">عذراً ...لا توجد نتائج!</div>');
    }
	
	
}


function trainer_course_details(id){

var Template = $('#trainer_course_details_script').html(); //script
var compiledTemplate = Template7.compile(Template); //تحضير الاسكربت لاستقبال المتغيرات

   $.ajax({
        type: "POST",
        data:{'id':id},
        url: url + "get_trainer_course",
        dataType: "JSON",
        beforeSend: function () {
            myApp.showIndicator();
        },
        success: function (data) {
           
            if(data != null && data.msg != null){
            	//console.log(data);
             data.msg.image = data.msg.image ? images_url + data.msg.image : 'img/mylogo1.png';
           	 
           	 data.msg.price = data.msg.price ? '<br><p class="centerize"> سعر الدورة : <b  style="color:#f2a000;">'+data.msg.price+'</b></p>' : '';
           	 
           	 
           	 data = data.msg;
           	 var result = compiledTemplate({data});
           	 
           	 $('#trainer_course_details_content').html(result);
            }else{
            $('#trainer_course_details_content').html('<div class="center" style="text-align: center;padding-top: 10%; font-size: 30px;color:brown;">عذراً ...لا توجد نتائج!</div>');	
            }
            
            myApp.hideIndicator();

        },
        error: function (jqXHR, textStatus, errorThrown) {
            myApp.alert('خطأ فى الاتصال', '<b style="color:#e04e4e">خطأ</b>');
            myApp.hideIndicator();
        }
    });
           
        
    
}


///////////////////////////////// tc /////////////////////////////////////////

/* map ********/


function check_latlng(){
    if(localStorage.getItem('lat') == null)
        localStorage.setItem('lat','24.3595215') ;
    if(localStorage.getItem('lng') == null)
        localStorage.setItem('lng','46.9420118') ;
}


var markers = [];
var lat_lang;
var map;


var show_map = 0;
function initializeAdd(old_location = null) {
    show_map = 1;
    geocoder = new google.maps.Geocoder();

if(old_location != null && old_location != ''){
var res = old_location.split(",");
var lat	= res[0];
var lng = res[1];
}else{
var lat	= localStorage.getItem('lat');
var lng = localStorage.getItem('lng');

}


var latlng = new google.maps.LatLng(lat, lng);


    //var latlng = new google.maps.LatLng(localStorage.getItem('lat'), localStorage.getItem('lng'));


//console.log(old_latlng);

    var myOptions = {
        zoom: 11, 
        center: latlng,
        draggable:true, 
        mapTypeId: google.maps.MapTypeId.ROADMAP
    };

    map = new google.maps.Map(document.getElementById("map1"), myOptions);

    var marker = new google.maps.Marker({
        position: latlng,
        map: map

    });
    markers.push(marker);


    google.maps.event.addListener(map, 'click', function(event) {
        deleteMarkers();

        placeMarker(map, event.latLng);
    });


    //if website


    infoWindow = new google.maps.InfoWindow;

    // Try HTML5 geolocation.
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(function(position) {
            var pos = {
                lat: position.coords.latitude,
                lng: position.coords.longitude
            };
            $('#location').val(position.coords.latitude+','+position.coords.longitude);
            infoWindow.setPosition(pos);
            infoWindow.setContent('الموقع الحالى');
            infoWindow.open(map);
            map.setCenter(pos);


        }, function() {

            handleLocationError(true, infoWindow, map.getCenter());

        });
    } else {
        // Browser doesn't support Geolocation
        handleLocationError(false, infoWindow, map.getCenter());

    }



    // end web site


}


function initializeAdd2(old_location = null) {
    show_map = 1;
    geocoder = new google.maps.Geocoder();

if(old_location != null && old_location != ''){
var res = old_location.split(",");
var lat	= res[0];
var lng = res[1];
}else{
var lat	= localStorage.getItem('lat');
var lng = localStorage.getItem('lng');

}


var latlng = new google.maps.LatLng(lat, lng);


    //var latlng = new google.maps.LatLng(localStorage.getItem('lat'), localStorage.getItem('lng'));


//console.log(old_latlng);

    var myOptions = {
        zoom: 11, 
        center: latlng,
        draggable:true, 
        mapTypeId: google.maps.MapTypeId.ROADMAP
    };

    map = new google.maps.Map(document.getElementById("map2"), myOptions);

    var marker = new google.maps.Marker({
        position: latlng,
        map: map

    });
    markers.push(marker);


    google.maps.event.addListener(map, 'click', function(event) {
        deleteMarkers();

        placeMarker2(map, event.latLng);
    });


    //if website


    infoWindow = new google.maps.InfoWindow;

    // Try HTML5 geolocation.
    if (navigator.geolocation) { 
        navigator.geolocation.getCurrentPosition(function(position) {
            var pos = {
                lat: position.coords.latitude,
                lng: position.coords.longitude
            };
            $('.location').val(position.coords.latitude+','+position.coords.longitude);
            infoWindow.setPosition(pos);
            infoWindow.setContent('الموقع الحالى');
            infoWindow.open(map);
            map.setCenter(pos);


        }, function() {

            handleLocationError(true, infoWindow, map.getCenter());

        });
    } else {
        // Browser doesn't support Geolocation
        handleLocationError(false, infoWindow, map.getCenter());

    }



    // end web site


}





function show_single_map(old_location = null) {
    show_map = 1;
    geocoder = new google.maps.Geocoder();

if(old_location != null && old_location != ''){
var res = old_location.split(",");
var lat	= res[0];
var lng = res[1];
}else{
var lat	= localStorage.getItem('lat');
var lng = localStorage.getItem('lng');

}


var latlng = new google.maps.LatLng(lat, lng);


    //var latlng = new google.maps.LatLng(localStorage.getItem('lat'), localStorage.getItem('lng'));


//console.log(old_latlng);

    var myOptions = {
        zoom: 11, 
        center: latlng,
        draggable:true, 
        mapTypeId: google.maps.MapTypeId.ROADMAP
    };

    map = new google.maps.Map(document.getElementById("map2"), myOptions);

    var marker = new google.maps.Marker({
        position: latlng,
        map: map

    });
    markers.push(marker);


    // google.maps.event.addListener(map, 'click', function(event) {
        // deleteMarkers();
// 
        // placeMarker2(map, event.latLng);
    // });


    //if website


    // infoWindow = new google.maps.InfoWindow;
    //
    // // Try HTML5 geolocation.
    // if (navigator.geolocation) {
    //     navigator.geolocation.getCurrentPosition(function(position) {
    //         var pos = {
    //             lat: position.coords.latitude,
    //             lng: position.coords.longitude
    //         };
    //        // $('.location').val(position.coords.latitude+','+position.coords.longitude);
    //         infoWindow.setPosition(pos);
    //         infoWindow.setContent('الموقع الحالى');
    //         infoWindow.open(map);
    //         map.setCenter(pos);
    //
    //
    //     }, function() {
    //
    //         handleLocationError(true, infoWindow, map.getCenter());
    //
    //     });
    // } else {
    //     // Browser doesn't support Geolocation
    //     handleLocationError(false, infoWindow, map.getCenter());
    //
    // }



    // end web site


}



function placeMarker(map, location) {

    var marker = new google.maps.Marker({
        position: location,
        map: map,
        draggable:true,
        animation:google.maps.Animation.DROP
    });

    var infowindow = new google.maps.InfoWindow({
        content:location.lat() + ',' + location.lng()
    });

    lat_lang = infowindow.content;
//        document.getElementById('lat_lng').value = lng_lat;
//            infowindow.open(map,marker);
    markers.push(marker);
    $('#location').attr('value',lat_lang);

}


function placeMarker2(map, location) {

    var marker = new google.maps.Marker({
        position: location,
        map: map,
        draggable:true,
        animation:google.maps.Animation.DROP
    });

    var infowindow = new google.maps.InfoWindow({
        content:location.lat() + ',' + location.lng()
    });

    lat_lang = infowindow.content;
//        document.getElementById('lat_lng').value = lng_lat;
//            infowindow.open(map,marker);
    markers.push(marker);
    $('.location').attr('value',lat_lang);

}


// Sets the map on all markers in the array.
function setMapOnAll(map) {
    for (var i = 0; i < markers.length; i++) {
        markers[i].setMap(map);
    }
}

// Removes the markers from the map, but keeps them in the array.
function clearMarkers() {
    setMapOnAll(null);
}

// Shows any markers currently in the array.
function showMarkers() {
    setMapOnAll(map);
}

// Deletes all markers in the array by removing references to them.
function deleteMarkers() {
    clearMarkers();
    markers = [];
}




/* end map */


myApp.onPageInit('signup-tc', function (page){

    initializeAdd();

var country = JSON.parse(localStorage.getItem('country'));

for(var i in country){ 
              	
              	var item = country[i];
              	if(item.id == 1)
              	continue;
              	
              	$(".country_select").append('<option value="'+item.id+'">'+item.arabic+'</option>');
              	
            }  


var city = JSON.parse(localStorage.getItem('city'));

for(var i in city){
              	
              	var item = city[i];
              	if(item.arabic == '' || item.arabic == null)
              	continue;
              	  
              	if(i == 0)
              	 $(".city_select").append('<option value="'+item.id+'" selected>'+item.arabic+'</option>'); 
              	else 
              	 $(".city_select").append('<option value="'+item.id+'">'+item.arabic+'</option>');
              	
            }  

    $('#signup_tc_form').submit(function (e){
        e.preventDefault();
        //myApp.alert('','Ok');  
        $('#username66 , #name66 ,#email66 , #password66 , #confirm_password66 , #phone66,#city66').css('border', 'none');
        if (!$('#name66').val()) {
            $("#name66").css("border-bottom", "2px solid #01A686 ");
            $("#name66").focus();
        } else if (!$('#username66').val()) {
            $("#username66").css("border-bottom", "2px solid #01A686");
            $("#username66").focus();
        } else if (!$('#email66').val()) {
            $("#email66").css("border-bottom", "2px solid #01A686");
            $("#email66").focus();
        } else if (!$('#password66').val()) {
            $("#password66").css("border-bottom", "2px solid #01A686");
            $("#password66").focus();
        } else if (!$('#confirm_password66').val()) {
            $("#confirm_password66").css("border-bottom", "2px solid #01A686");
            $("#confirm_password66").focus();
        } else if($("#sure66").prop('checked') == false){
         myApp.alert('يجب الموافقه على سياسة الخصوصية', '<b style="color:#e04e4e">تنبيه !</b>');
        } else { 
  
            // Create a formdata object and add the files
            var formData = new FormData($(this)[0]);
            // var data = JSON.stringify(formData);
            //console.log(data) ;
            $.ajax({
                url: url + "register_tc",
                type: 'POST',
                data: formData,
                cache: false,
                contentType: 'json',
                processData: false, // Don't process the files
                contentType: false, // Set content type to false as jQuery will tell the server its a query string request
                beforeSend: function () {
                    myApp.showIndicator();
                },
                success: function (out, textStatus, jqXHR) {
                    out = JSON.parse(out);
 
                   if (out.status == true) {
                        $('#signup_tc_form')[0].reset();
                       myApp.alert(' شكرا لك', '<b style="color:#8bc18b">'+out.msg.return_msg+'</b>');

                    } else {
                        //console.log(out.msg) ;
                        myApp.alert(out.msg, '<b style="color:#e04e4e">خطأ</b>');
                    }

                    myApp.hideIndicator();

                },
                error: function (jqXHR, textStatus, errorThrown) {
                    myApp.alert('خطا فى الاتصال', '<b style="color:#e04e4e">خطأ</b>');
                    myApp.hideIndicator();
                }
            });

        }

    });


});




myApp.onPageInit('profile-tc', function (page) {



    var data = JSON.parse(localStorage.getItem('member'));
    console.log(data);
    if (data != null) {
        if (data.image == '')
            data.image = 'img/mylogo1.png';
        else
            data.image = images_url + data.image;
       
        var profileTemplate = $('#profile_tc_script').html(); //script
        var compiledProfileTemplate = Template7.compile(profileTemplate); //تحضير الاسكربت لاستقبال المتغيرات
        var result = compiledProfileTemplate({data}); // ارسال المتغير


        $('#profile_tc_content').html(result);
        
        
        
 var country = JSON.parse(localStorage.getItem('country'));

for(var i in country){ 
              	
              	var item = country[i];
              	if(item.id == 1)
              	continue;
              	
              	if(data.country_id == item.id)
              	$(".country_select").append('<option value="'+item.id+'" selected>'+item.arabic+'</option>');
              	else
              	$(".country_select").append('<option value="'+item.id+'">'+item.arabic+'</option>');
              	
            }  



var city = JSON.parse(localStorage.getItem('city'));

for(var i in city){
              	
              	var item = city[i];
              	if(item.arabic == '' || item.arabic == null)
              	continue;
              	   
              	if(data.city_id == item.id)
              	 $(".city_select").append('<option value="'+item.id+'" selected>'+item.arabic+'</option>');
              	else 
              	 $(".city_select").append('<option value="'+item.id+'">'+item.arabic+'</option>');
              	
            }
    
           
           ///Change Password ///
       
    var change_passwordTemplate = $('#change_password_script').html(); //script
    var compiledChange_passwordTemplate = Template7.compile(change_passwordTemplate); //تحضير الاسكربت لاستقبال المتغيرات
    var result2 = compiledChange_passwordTemplate({data}); // ارسال المتغير


    $('#change_password_content').html(result2);    
    
    
    
     
         
    }
    
    
    initializeAdd(data.location);

});




myApp.onPageInit('add-tc-courses', function (page) {
	
	
	
	var data = JSON.parse(localStorage.getItem('member'));
	
	
	var Template = $('#add_tc_courses_script').html(); //script
    var compiledTemplate = Template7.compile(Template); //تحضير الاسكربت لاستقبال المتغيرات
    var result = compiledTemplate({data}); // ارسال المتغير
    
   
    $('#add_tc_courses_content').html(result);     
           
 //    initializeAdd2();  
     
     
     var myDate = new Date();

	var gryday     = myDate.getDate();
	var grymonth   = myDate.getMonth() + 1;
	var gryyear    = myDate.getFullYear();
	fromGreg(gryday,grymonth,gryyear);
	var monthNames = ['يناير', 'فبراير', 'مارس', 'أبريل', 'مايو', 'يونيه', 'يوليو', 'أغسطس', 'سبتمبر', 'أكتوبر', 'نوفمبر', 'ديسمبر'];

	var calendarInline = myApp.calendar({

		container : '#calendar-inline-container',
		value : [new Date()],
		weekHeader : false,
		toolbarTemplate : '<div class="toolbar calendar-custom-toolbar">' + '<div class="toolbar-inner">' + '<div class="left">' + '<a href="#" class="link icon-only"><i class="icon icon-forward "></i></a>' + '</div>' + '<div class="center"></div>' + '<div class="right">' + '<a href="#" class="link icon-only"><i class="icon icon-back"></i></a>' + '</div>' + '</div>' + '</div>',
		onOpen : function(p) {
			$$('.calendar-custom-toolbar .center').text(monthNames[p.currentMonth] + ' , ' + p.currentYear);
			$$('.calendar-custom-toolbar .left .link').on('click', function() {
				calendarInline.prevMonth();
			});
			$$('.calendar-custom-toolbar .right .link').on('click', function() {
				calendarInline.nextMonth();
			});
		},
		onMonthYearChangeStart : function(p) {
			$$('.calendar-custom-toolbar .center').text(monthNames[p.currentMonth] + ' , ' + p.currentYear);
			$('.picker-calendar-day').click(function (){
				var year = $(this).attr('data-year');
				var month = $(this).attr('data-month');
				var day = $(this).attr('data-day');
				month = +month + 1;
				fromGreg(day,month,year);
			});
		}
	});

	$('.picker-calendar-day').click(function (){
	 var year = $(this).attr('data-year');
	 var month = $(this).attr('data-month');
	 var day = $(this).attr('data-day');
		month = +month + 1;
	fromGreg(day,month,year);
	});
	 
    
	 
});



// calendar

function fromGreg(gryday,grymonth,gryyear){


	var grutype    = gryday+'/'+grymonth+'/'+gryyear;

	console.log("day : "+ gryday +" ,month : "+ grymonth +" ,year : "+ gryyear )

	var calendar = $.calendars.instance('gregorian', 'ar-EG');
	var date = calendar.parseDate('dd/mm/yyyy', grutype);
	calendar = $.calendars.instance('ummalqura');
	calendar2 = $.calendars.instance('persian');
	calendar3 = $.calendars.instance('gregorian', 'ar-EG');
	date = calendar.fromJD(date.toJD());
	date2 = calendar2.fromJD(date.toJD());
	date3 = calendar3.fromJD(date.toJD());

	$('#hijry_date').val(calendar.formatDate('dd/MM/yyyy', date));
	$('#farsi2_date').val(calendar2.formatDate('dd/MM/yyyy', date2));
	$('#gresults_date').val(calendar3.formatDate('DD dd/MM/yyyy', date3));
	
}


myApp.onPageInit('tc-courses', function (page) {
	
    var coursesTemplate = $('#tc_courses_script').html(); //script
    var compiledCoursesTemplate = Template7.compile(coursesTemplate); //تحضير الاسكربت لاستقبال المتغيرات

    var member = JSON.parse(localStorage.getItem('member'));
    
    if(member != null){
    
    $.ajax({
        type: "post",
        data:{'id':member.member_id},
        url: url + "tc_courses",
        dataType: "JSON",
        beforeSend: function () {
            myApp.showIndicator();
        },
        success: function (result) {
            myApp.hideIndicator();
            if (result.status == true) {

                if (result.msg != null && result.msg.courses != null) {

                    for (var i in result.msg.courses) {
                      var item = result.msg.courses[i];
                      item.image = item.image ? images_url + item.image : 'img/mylogo1.png';
                      
                        var res_courses = compiledCoursesTemplate({item});
                        $('#tc_courses_content').append(res_courses);
                    }

                } else {
                    $('#tc_courses_content').html('<div class="center" style="text-align: center;padding-top: 10%; font-size: 30px;color:brown;">عذراً ...لا توجد نتائج!</div>');
                }

            } else {
                $('#tc_courses_content').html('<div class="center" style="text-align: center;padding-top: 10%; font-size: 30px;color:brown;">عذراً ...لا توجد نتائج!</div>');
            }


        },
        error: function (jqXHR, textStatus, errorThrown) {
            myApp.alert('خطا فى الاتصال', '<b style="color:#e04e4e">تنبيه !</b>');
            myApp.hideIndicator();
        }
    });
    }else{
    	$('#tc_courses_content').html('<div class="center" style="text-align: center;padding-top: 10%; font-size: 30px;color:brown;">عذراً ...لا توجد نتائج!</div>');
    }

});




function delete_tc_corse(id){
	
	var member = JSON.parse(localStorage.getItem('member'));
	
	myApp.confirm(' هل تريد حذف هذه الدوره ؟','<b style="color:#e04e4e">تنبيه !</b>', function () {
	$.ajax({
        type: "post",
        data:{'id':id , 'member_id':member.member_id},
        url: url + "delete_tc_course",
        dataType: "JSON",
        beforeSend: function () {
            myApp.showIndicator();
        },
        success: function (result) {
            myApp.hideIndicator();
            if (result.status == true) {

                if (result.msg != null && result.msg.courses != null) {
                	
                 var coursesTemplate = $('#tc_courses_script').html(); //script
                 var compiledCoursesTemplate = Template7.compile(coursesTemplate); //تحضير الاسكربت لاستقبال المتغيرات
                  $('#tc_courses_content').html('');
                    for (var i in result.msg.courses) {
                      var item = result.msg.courses[i];
                      item.image = item.image ? images_url + item.image : 'img/mylogo1.png';
                      
                        var res_courses = compiledCoursesTemplate({item});
                        $('#tc_courses_content').append(res_courses);
                    }
                    
                   myApp.alert(' شكرا لك', '<b style="color:#8bc18b">تم الحذف بنجاح</b>');


                } else {
                    $('#tc_courses_content').html('<div class="center" style="text-align: center;padding-top: 10%; font-size: 30px;color:brown;">عذراً ...لا توجد نتائج!</div>');
                }

            } else {
                $('#tc_courses_content').html('<div class="center" style="text-align: center;padding-top: 10%; font-size: 30px;color:brown;">عذراً ...لا توجد نتائج!</div>');
            }


        },
        error: function (jqXHR, textStatus, errorThrown) {
            myApp.alert('خطا فى الاتصال', '<b style="color:#e04e4e">تنبيه !</b>');
            myApp.hideIndicator();
        }
    });
	
});
	
}




function get_tc_course(id){
	
var member = JSON.parse(localStorage.getItem('member'));

var Template = $('#edit_tc_courses_script').html(); //script
var compiledTemplate = Template7.compile(Template); //تحضير الاسكربت لاستقبال المتغيرات

	
   $.ajax({
        type: "POST",
        data:{'id':id,'member_id':member.member_id},
        url: url + "get_tc_course",
        dataType: "JSON",
        beforeSend: function () {
            myApp.showIndicator();
        },
        success: function (data) {
           
            if(data != null && data.msg != null){
            	console.log(data);
             data.msg.image = data.msg.image ? images_url + data.msg.image : 'img/mylogo1.png';
           	 if(data.msg.available == 1){
           	 	data.msg.is_active = '<input type="radio" name="available" value="1" checked="checked">';
           	 	data.msg.not_active = '<input type="radio" name="available" value="0" >';
           	 }else{
           	 	data.msg.is_active = '<input type="radio" name="available" value="1" >';
           	 	data.msg.not_active = '<input type="radio" name="available" value="0" checked="checked">';
           	 }
           	 
         
           	 data = data.msg;
           	 var result = compiledTemplate({data});
           	 
           	 $('#edit_tc_courses_content').html(result);
           	 
           	// initializeAdd2(data.location);
           	 
           	
           	 var myDate = new Date();

	var gryday     = myDate.getDate();
	var grymonth   = myDate.getMonth() + 1;
	var gryyear    = myDate.getFullYear();
	 if(data.course_birth_date == null){
           	fromGreg(gryday,grymonth,gryyear); 	
           	 }
	
	var monthNames = ['يناير', 'فبراير', 'مارس', 'أبريل', 'مايو', 'يونيه', 'يوليو', 'أغسطس', 'سبتمبر', 'أكتوبر', 'نوفمبر', 'ديسمبر'];

	var calendarInline = myApp.calendar({

		container : '#calendar-inline-container',
		value : [new Date()],
		weekHeader : false,
		toolbarTemplate : '<div class="toolbar calendar-custom-toolbar">' + '<div class="toolbar-inner">' + '<div class="left">' + '<a href="#" class="link icon-only"><i class="icon icon-forward "></i></a>' + '</div>' + '<div class="center"></div>' + '<div class="right">' + '<a href="#" class="link icon-only"><i class="icon icon-back"></i></a>' + '</div>' + '</div>' + '</div>',
		onOpen : function(p) {
			$$('.calendar-custom-toolbar .center').text(monthNames[p.currentMonth] + ' , ' + p.currentYear);
			$$('.calendar-custom-toolbar .left .link').on('click', function() {
				calendarInline.prevMonth();
			});
			$$('.calendar-custom-toolbar .right .link').on('click', function() {
				calendarInline.nextMonth();
			});
		},
		onMonthYearChangeStart : function(p) {
			$$('.calendar-custom-toolbar .center').text(monthNames[p.currentMonth] + ' , ' + p.currentYear);
			$('.picker-calendar-day').click(function (){
				var year = $(this).attr('data-year');
				var month = $(this).attr('data-month');
				var day = $(this).attr('data-day');
				month = +month + 1;
				fromGreg(day,month,year);
			});
		}
	});

	$('.picker-calendar-day').click(function (){
	 var year = $(this).attr('data-year');
	 var month = $(this).attr('data-month');
	 var day = $(this).attr('data-day');
		month = +month + 1;
	fromGreg(day,month,year);
	});
           	 
           	 
           	 
            }else{
            $('#edit_tc_courses_content').html('<div class="center" style="text-align: center;padding-top: 10%; font-size: 30px;color:brown;">عذراً ...لا توجد نتائج!</div>');	
            }
            
            
            
            myApp.hideIndicator();

        },
        error: function (jqXHR, textStatus, errorThrown) {
            myApp.alert('خطأ فى الاتصال', '<b style="color:#e04e4e">خطأ</b>');
            myApp.hideIndicator();
        }
    });
           
  
  
  
        
    
}	





function course_details(id){

var member = JSON.parse(localStorage.getItem('member'));

var Template = $('#course_details_script').html(); //script
var compiledTemplate = Template7.compile(Template); //تحضير الاسكربت لاستقبال المتغيرات
 
    if(member != null && member.member_group_id ==3){
    	var member_id = member.member_id;
    }else{
    	var member_id = 0;
    }


 
    
    
   $.ajax({
        type: "POST",
        data:{'id':id,'member_id':member_id},
        url: url + "get_tc_course",
        dataType: "JSON",
        beforeSend: function () {
            myApp.showIndicator();
        },
        success: function (data) {
           
            if(data != null && data.msg != null){
            	//console.log(data);
             data.msg.image = data.msg.image ? images_url + data.msg.image : 'img/mylogo1.png';
           	 
           	 data.msg.price = data.msg.price ? data.msg.price : 'غير محدد';
           	 
           	 if(data.msg.available == 1){
           	 	data.msg.available = 'متاحة';
           	 }else{
           	 	data.msg.available = 'غير متاحة';
           	 }
           	 
           	 if(data.msg.center != null){
           	 	 data.msg.center.image = data.msg.center.image ? images_url + data.msg.center.image : 'img/mylogo1.png';
           	 
           	 if(data.msg.center.country_name != null && data.msg.center.city_name != null){
           	 	data.msg.center.slash = ' - ';
           	 }
           	 
           	 if(data.msg.center.address != null && data.msg.center.city_name != null){
           	 	data.msg.center.slashh = ' - ';
           	 }
           	 
           	 if(data.msg.center.address != null && data.msg.center.city_name == null && data.msg.center.country_name != null){
           	 	data.msg.center.slashh = ' - '; 
           	 }
           	 
           	 }
           	
           	 data = data.msg;
           	 var result = compiledTemplate({data});
           	 
           	 $('#course_details_content').html(result);
           	 
           	 if(member != null ){
           	 if(member.member_group_id != 3){

                 $("#do_rank").css('display','none');
			 	$("#for_trainee").css('display','none');
			 }else{
			 	$("#for_trainee").css('display','block');

                 $("#do_rank").css('display','block');
			 }
			 
			 if(member.member_group_id != 1 || member.member_id != data.member_id){
			 	$("#for_center").css('display','none');
			 }else{
			 	$("#for_center").css('display','block');
			 }
			 
			 }else{
			 	$("#for_trainee").css('display','none');
			 	$("#for_center").css('display','none');
			 }
			 
			  if(data.center.phone != null && data.center.phone != 0 && data.center.phone != ''){
			 	$("#c_phone").show();
			 }else{
			 	$("#c_phone").hide();
			 }
			 
			 if(data.center == null){
           	 	$("#center_info").hide();
           	 }else{
           	 	$("#center_info").show();
           	 }
			 
			 if(data.location != null && data.location != ''){
           	 	$(".location_info").show();
           	 	}else{
           	 	$(".location_info").hide();
           	    }
			 
           	 //show_single_map(data.location);
           	 //show_single_map
           	 
            }else{
            $('#course_details_content').html('<div class="center" style="text-align: center;padding-top: 10%; font-size: 30px;color:brown;">عذراً ...لا توجد نتائج!</div>');	
            }
            
            myApp.hideIndicator();

        },
        error: function (jqXHR, textStatus, errorThrown) {
            myApp.alert('خطأ فى الاتصال', '<b style="color:#e04e4e">خطأ</b>');
            myApp.hideIndicator();
        }
    });
           
        
    
}




//اضافة الى المفضلة

function add_fav(id){
	
	var member = JSON.parse(localStorage.getItem('member'));
	
	if(member != null && member.member_group_id == 3){
		
	myApp.confirm(' هل تريد اضافة هذه الدوره الى الدورات المفضلة؟','<b style="color:#e04e4e">تنبيه !</b>', function () {
	$.ajax({
        type: "post",
        data:{'id':id , 'member_id':member.member_id},
        url: url + "add_fav_courses",
        dataType: "JSON",
        beforeSend: function () {
            myApp.showIndicator();
        },
        success: function (result) {
            myApp.hideIndicator();
            if (result.status == true) {
                   myApp.alert(' شكرا لك', '<b style="color:#8bc18b">تمت إضافة هذه الدوره الى الدورات المفضلة لديك</b>');
                $("#fav").remove();
                $("#fav_but").prepend('<div class="col-50 trainer-external" id="fav"><a href="#" class="button centerize button-fill button-big " onclick="delete_fav('+id+')"  style="width: 87%;" >   إزالة من المفضله </a></div>');
               } else{
                   myApp.alert(' من فضلك أعد المحاوله لاحقاً', '<b style="color:#e04e4e">تنبيه !</b>');
               }

        },
        error: function (jqXHR, textStatus, errorThrown) {
            myApp.alert('خطا فى الاتصال', '<b style="color:#e04e4e">تنبيه !</b>');
            myApp.hideIndicator();
        }
    });
	
});

}else{
 myApp.alert('يجب التسجيل كمتدرب اولاً', '<b style="color:#e04e4e">تنبيه !</b>');	
}
	
}



//ازاله من المفضلة

function delete_fav(id){
	
	var member = JSON.parse(localStorage.getItem('member'));
	
	if(member != null && member.member_group_id == 3){
	
	myApp.confirm(' هل تريد إزالة هذه الدوره من الدورات المفضلة؟','<b style="color:#e04e4e">تنبيه !</b>', function () {
	$.ajax({
        type: "post",
        data:{'id':id , 'member_id':member.member_id},
        url: url + "delete_fav_courses",
        dataType: "JSON",
        beforeSend: function () {
            myApp.showIndicator();
        },
        success: function (result) {
            myApp.hideIndicator();
            
            
            if (result.status == true) {
                   myApp.alert(' شكرا لك', '<b style="color:#8bc18b">تمت إزالة هذه الدوره من الدورات المفضلة لديك</b>');
                $("#fav").remove();
                $("#fav_but").prepend('<div class="col-50 trainer-external" id="fav"><a href="#" class="button centerize button-fill button-big " onclick="add_fav('+id+')"  style="width: 87%;" >   أضف الى المفضلة </a></div>');
               } else{
                   myApp.alert(' من فضلك أعد المحاوله لاحقاً', '<b style="color:#e04e4e">تنبيه !</b>');
               }
               
               

        },
        error: function (jqXHR, textStatus, errorThrown) {
            myApp.alert('خطا فى الاتصال', '<b style="color:#e04e4e">تنبيه !</b>');
            myApp.hideIndicator();
        }
    });
	
});

}else{
 myApp.alert('يجب التسجيل كمتدرب اولاً', '<b style="color:#e04e4e">تنبيه !</b>');	
}
	
}





function show_centers(){
	
	 var Template = $('#tc_script').html(); //script
    var compiledTemplate = Template7.compile(Template); //تحضير الاسكربت لاستقبال المتغيرات
    $('#tc_content').html('');
    var member = JSON.parse(localStorage.getItem('member'));
    
    if(member != null){
    
    $.ajax({
        type: "post",
        url: url + "show_tc",
        data:{'search':$('#search_centers').val()},
        dataType: "JSON",
        beforeSend: function () {
            myApp.showIndicator();
        },
        success: function (result) {
            myApp.hideIndicator();
            if (result.status == true) {

                if (result.msg != null && result.msg.tc != null) {
                	$('#tc_content').html('');
 for (var i in result.msg.tc) {
 	if(i == 10)           //************* غير هنا ****************//
 	break;                  //************* غير هنا ****************//
                      var item = result.msg.tc[i];
                      item.image = item.image ? images_url + item.image : 'img/panel-img.png';
                      
                      if(item.city_name != null && item.country_name != null)
                      item.slash = ' - ';
                      
                        var res_tc = compiledTemplate({item});
                        $('#tc_content').append(res_tc);
                        
                         if(item.phone != null && item.phone != 0 && item.phone != ''){
						 	$("#show_tc_phone"+item.member_id).show();
						 }else{
						 	$("#show_tc_phone"+item.member_id).hide();
						 }
                    }



	 /* بدايه الاسكرول     */
                	 total = result.msg.tc.length;            //************* غير هنا ****************//

                	// Loading flag
        var loading = false;
// Last loaded index
        var lastIndex = $$('#tc_content li').length;            //************* غير هنا ****************//
// Max items to load
        var maxItems = total-1;
console.log('total :'+ maxItems);
// Append items per load
        var itemsPerLoad = 10;
// Attach 'infinite' event handler
        $$('.infinite-scroll').on('infinite', function () {
        	 console.log('lastIndex :' + lastIndex);
            // Exit, if loading in progress
            if (loading) return;
            // Set loading flag
            loading = true;
            // Emulate 1s loading
            setTimeout(function () {
                // Reset loading flag
                loading = false;
                console.log('lastIndex :' + lastIndex);
                if (lastIndex >= maxItems) {
                    // Nothing more to load, detach infinite scroll events to prevent unnecessary loadings
                    myApp.detachInfiniteScroll($$('.infinite-scroll'));
                    // Remove preloader
                    $$('.infinite-scroll-preloader').remove();
                    return;
                }if (lastIndex < maxItems) {
                    // Generate new items HTML
                    var html = '';
                    for (var i = lastIndex + 1; i <= lastIndex + itemsPerLoad; i++) {              //************* غير هنا ****************//
                    	
                      var item = result.msg.tc[i];
                      item.image = item.image ?  images_url + item.image : 'img/panel-img.png';
                      
                      if(item.city_name != null && item.country_name != null)
                      item.slash = ' - ';
                        
                      
                        var res_tc = compiledTemplate({item});
                        $('#tc_content').append(res_tc);
                        
                         if(item.phone != null && item.phone != 0 && item.phone != ''){
						 	$("#show_tc_phone"+item.member_id).show();
						 }else{
						 	$("#show_tc_phone"+item.member_id).hide();
						 }
                    }
                }
                // Append new items
              //  $$('.list-block ul').append(html);
                // Update last loaded index
            }, 1000);
            lastIndex = $$('#tc_content li').length;                     //************* غير هنا ****************//
            console.log('lastIndex after :' + lastIndex);
        });

                 	
                	
          /* نهايه الاسكرول     */

                } else {
                	    $$('.infinite-scroll-preloader').remove();
                    $('#tc_content').html('<div class="center" style="text-align: center;padding-top: 10%; font-size: 30px;color:brown;">عذراً ...لا توجد نتائج!</div>');
                }

            } else {
            	    $$('.infinite-scroll-preloader').remove();
                $('#tc_content').html('<div class="center" style="text-align: center;padding-top: 10%; font-size: 30px;color:brown;">عذراً ...لا توجد نتائج!</div>');
            }


        },
        error: function (jqXHR, textStatus, errorThrown) {
            myApp.alert('خطا فى الاتصال', '<b style="color:#e04e4e">تنبيه !</b>');
            myApp.hideIndicator();
        }
    });
    }else{
    	    $$('.infinite-scroll-preloader').remove();
    	$('#trainers_content').html('<div class="center" style="text-align: center;padding-top: 10%; font-size: 30px;color:brown;">عذراً ...لا توجد نتائج!</div>');
    }
}



myApp.onPageInit('show-centers', function (page) {
	
  show_centers(); 

});




function tc_info(id){

var Template = $('#tc_info_script').html(); //script
var compiledTemplate = Template7.compile(Template); //تحضير الاسكربت لاستقبال المتغيرات
	
   $.ajax({
        type: "POST",
        data:{'member_id':id},
        url: url + "tc_info",
        dataType: "JSON",
        beforeSend: function () {
            myApp.showIndicator();
        },
        success: function (data) {
           
            if(data != null && data.msg != null){
            	//console.log(data);
             data.msg.image = data.msg.image ? images_url + data.msg.image : 'img/panel-img.png';
           	 data = data.msg;
           	 var result = compiledTemplate({data});
           	 
           	$('#tc_info_content').html(result);
           	
           	var member = JSON.parse(localStorage.getItem('member'));
           	 if(member == null || member.member_group_id != 3){
		    $("#rate_act").hide();
		    }	
		   	
           	if(data.phone == null)
           	$('#tc_info_phone').hide();
           	
           	if(data.address == null)
           	$('#tc_info_address').hide();
           	
           	if(data.city_name == null)
           	$('#tc_info_city').hide();
           	
           	if(data.country_name == null)
           	$('#tc_info_country').hide();
           	
           	if(data.location == null || data.location == '')
           	$('.tc_info_location').hide();
           	
           	
           	 show_single_map(data.location);
           	
            }else{
            $('#tc_info_content').html('<div class="center" style="text-align: center;padding-top: 10%; font-size: 30px;color:brown;">عذراً ...لا توجد نتائج!</div>');	
            }
            
            myApp.hideIndicator();

        },
        error: function (jqXHR, textStatus, errorThrown) {
            myApp.alert('خطأ فى الاتصال', '<b style="color:#e04e4e">تنبيه !</b>');
            myApp.hideIndicator();
        }
    });
    
}	





function tc_courses_show(id){
		
    var coursesTemplate = $('#tc_courses_show_script').html(); //script
    var compiledCoursesTemplate = Template7.compile(coursesTemplate); //تحضير الاسكربت لاستقبال المتغيرات

    var member = JSON.parse(localStorage.getItem('member'));
    
    if(member != null){
    
    $.ajax({
        type: "post",
        data:{'id':id,'member_id':member.member_id},
        url: url + "tc_courses_show",
        dataType: "JSON",
        beforeSend: function () {
            myApp.showIndicator();
        },
        success: function (result) {
            myApp.hideIndicator();
            if (result.status == true) {

                if (result.msg != null && result.msg.courses != null) {

                    for (var i in result.msg.courses) {
                      var item = result.msg.courses[i];
                      item.image = item.image ? images_url + item.image : 'img/mylogo1.png';
                      
                      item.mark = '';
                      if(item.new == 0 && item.prop == 1){
                      item.mark = '<img src="img/suggest.png" alt="" class="tag-suggest">';
                      }else if(item.new == 1 && item.prop == 0){
                      item.mark = '<img src="img/new.png" alt="" class="tag-new">';
                      }else if(item.new == 1 && item.prop == 1){
                      item.mark = '<img src="img/new-suggest.png" alt="" class="suggest-new">';
                      }else{
                      item.mark = '';
                      }
                        var res_courses = compiledCoursesTemplate({item});
                        $('#tc_courses_show_content').append(res_courses);
                    }

                } else {
                    $('#tc_courses_show_content').html('<div class="center" style="text-align: center;padding-top: 10%; font-size: 30px;color:brown;">عذراً ...لا توجد نتائج!</div>');
                }

            } else {
                $('#tc_courses_show_content').html('<div class="center" style="text-align: center;padding-top: 10%; font-size: 30px;color:brown;">عذراً ...لا توجد نتائج!</div>');
            }


        },
        error: function (jqXHR, textStatus, errorThrown) {
            myApp.alert('خطا فى الاتصال', '<b style="color:#e04e4e">تنبيه !</b>');
            myApp.hideIndicator();
        }
    });
    }else{
    	$('#tc_courses_content').html('<div class="center" style="text-align: center;padding-top: 10%; font-size: 30px;color:brown;">عذراً ...لا توجد نتائج!</div>');
    }
	
	
}


myApp.onPageInit('Applicants', function (page) {
	
    var Template = $('#app_script').html(); //script
    var compiledTemplate = Template7.compile(Template); //تحضير الاسكربت لاستقبال المتغيرات

    var member = JSON.parse(localStorage.getItem('member'));
    
    if(member != null && member.member_group_id == 1){
    
    $.ajax({
        type: "post",
        url: url + "show_tc_app",
        data:{'center_id':member.member_id},
        dataType: "JSON",
        beforeSend: function () {
            myApp.showIndicator();
        },
        success: function (result) {
            
            if (result.status == true) {

                if (result.msg != null && result.msg != null) {

                    for (var i in result.msg) {
                      var item = result.msg[i];
                      item.image = item.image ? images_url + item.image : 'img/mylogo1.png';
                      
                        var res_app = compiledTemplate({item});
                        $('#app_content').append(res_app);
                    }

                } else {
                    $('#app_content').html('<div class="center" style="text-align: center;padding-top: 10%; font-size: 30px;color:brown;">عذراً ...لا يوجد متقدمين حتى الآن!</div>');
                }

            } else {
                $('#app_content').html('<div class="center" style="text-align: center;padding-top: 10%; font-size: 30px;color:brown;">عذراً ...لا يوجد متقدمين حتى الآن!</div>');
            }

         myApp.hideIndicator();
        },
        error: function (jqXHR, textStatus, errorThrown) {
            myApp.alert('خطا فى الاتصال', '<b style="color:#e04e4e">تنبيه !</b>');
            myApp.hideIndicator();
        }
    });
    }else{
    	$('#app_content').html('<div class="center" style="text-align: center;padding-top: 10%; font-size: 30px;color:brown;">عذراً ...لا توجد نتائج!</div>');
    }

});



function show_course_app(id) {
	
    var Template = $('#course_app_script').html(); //script
    var compiledTemplate = Template7.compile(Template); //تحضير الاسكربت لاستقبال المتغيرات

    var member = JSON.parse(localStorage.getItem('member'));
    
    if(member != null && member.member_group_id == 1){
    
    $.ajax({
        type: "post",
        url: url + "show_course_app",
        data:{'course_id':id,'center_id':member.member_id},
        dataType: "JSON",
        beforeSend: function () {
            myApp.showIndicator();
        },
        success: function (result) {
            
            if (result.status == true) {

                if (result.msg != null && result.msg != null) {

                    for (var i in result.msg) {
                      var item = result.msg[i];
                      item.image = item.image ? images_url + item.image : 'img/person.png';
                      
                        var res_app = compiledTemplate({item});
                        $('#course_app_content').append(res_app);
                    }

                } else {
                    $('#course_app_content').html('<div class="center" style="text-align: center;padding-top: 10%; font-size: 30px;color:brown;">عذراً ...لا يوجد متقدمين حتى الآن!</div>');
                }

            } else {
                $('#course_app_content').html('<div class="center" style="text-align: center;padding-top: 10%; font-size: 30px;color:brown;">عذراً ...لا يوجد متقدمين حتى الآن!</div>');
            }

         myApp.hideIndicator();
        },
        error: function (jqXHR, textStatus, errorThrown) {
            myApp.alert('خطا فى الاتصال', '<b style="color:#e04e4e">تنبيه !</b>');
            myApp.hideIndicator();
        }
    });
    }else{
    	$('#course_app_content').html('<div class="center" style="text-align: center;padding-top: 10%; font-size: 30px;color:brown;">عذراً ...لا توجد نتائج!</div>');
    }

}





function show_course_approved_app(id) {
	
    var Template = $('#course_approved_app_script').html(); //script
    var compiledTemplate = Template7.compile(Template); //تحضير الاسكربت لاستقبال المتغيرات

    var member = JSON.parse(localStorage.getItem('member'));
    
    if(member != null && member.member_group_id == 1){
    
    $.ajax({
        type: "post",
        url: url + "show_course_approved_app",
        data:{'course_id':id,'center_id':member.member_id},
        dataType: "JSON",
        beforeSend: function () {
            myApp.showIndicator();
        },
        success: function (result) {
           
            if (result.status == true) {

                if (result.msg != null && result.msg != null) {

                    for (var i in result.msg) {
                      var item = result.msg[i];
                      item.image = item.image ? images_url + item.image : 'img/person.png';
                      
                        var res_app = compiledTemplate({item});
                        $('#course_approved_app_content').append(res_app);
                    }

                } else {
                    $('#course_approved_app_content').html('<div class="center" style="text-align: center;padding-top: 10%; font-size: 30px;color:brown;">عذراً ...لا يوجد متدربين!</div>');
                }

            } else {
                $('#course_approved_app_content').html('<div class="center" style="text-align: center;padding-top: 10%; font-size: 30px;color:brown;">عذراً ...لا يوجد متدربين!</div>');
            }

        myApp.hideIndicator();
        
        },
        error: function (jqXHR, textStatus, errorThrown) {
            myApp.alert('خطا فى الاتصال', '<b style="color:#e04e4e">تنبيه !</b>');
            myApp.hideIndicator();
        }
    });
    }else{
    	$('#course_approved_app_content').html('<div class="center" style="text-align: center;padding-top: 10%; font-size: 30px;color:brown;">عذراً ...لا توجد نتائج!</div>');
    }

}




function approve_app(id){
	
	var member = JSON.parse(localStorage.getItem('member'));
	
	if(member != null && member.member_group_id == 1){
		
	myApp.confirm(' هل انت موافق على التحاق هذا المتدرب بهذه الدوره؟','<b style="color:#e04e4e">تنبيه !</b>', function () {
	$.ajax({
        type: "post",
        data:{'id':id , 'member_id':member.member_id},
        url: url + "approve_app",
        dataType: "JSON",
        beforeSend: function () {
            myApp.showIndicator();
        },
        success: function (result) {
            myApp.hideIndicator();
            if (result.status == true) {
                   myApp.alert(' شكرا لك', '<b style="color:#8bc18b">تم قبول طلب المتدرب</b>');
                $("#app"+id).hide();
               } else{
                   myApp.alert(' من فضلك أعد المحاوله لاحقاً', '<b style="color:#e04e4e">تنبيه !</b>');
               }

        },
        error: function (jqXHR, textStatus, errorThrown) {
            myApp.alert('خطا فى الاتصال', '<b style="color:#e04e4e">تنبيه !</b>');
            myApp.hideIndicator();
        }
    });
	
});

}else{
 myApp.alert('يجب التسجيل اولاً', '<b style="color:#e04e4e">تنبيه !</b>');	
}
	

}


function disapprove_app(id){
	
	var member = JSON.parse(localStorage.getItem('member'));
	
	if(member != null && member.member_group_id == 1){
		
	myApp.confirm(' هل انت متأكد من رفض هذا الطلب؟','<b style="color:#e04e4e">تنبيه !</b>', function () {
	$.ajax({
        type: "post",
        data:{'id':id , 'member_id':member.member_id},
        url: url + "disapprove_app",
        dataType: "JSON",
        beforeSend: function () {
            myApp.showIndicator();
        },
        success: function (result) {
            myApp.hideIndicator();
            if (result.status == true) {
                   myApp.alert(' شكرا لك', '<b style="color:#8bc18b">تم رفض طلب المتدرب</b>');
                $("#app"+id).hide();
               } else{
                   myApp.alert(' من فضلك أعد المحاوله لاحقاً', '<b style="color:#e04e4e">تنبيه !</b>');
               }

        },
        error: function (jqXHR, textStatus, errorThrown) {
            myApp.alert('خطا فى الاتصال', '<b style="color:#e04e4e">تنبيه !</b>');
            myApp.hideIndicator();
        }
    });
	
});

}else{
 myApp.alert('يجب التسجيل اولاً', '<b style="color:#e04e4e">تنبيه !</b>');	
}
	

}




function delete_app(id){
	
	var member = JSON.parse(localStorage.getItem('member'));
	
	if(member != null && member.member_group_id == 1){
		
	myApp.confirm(' هل انت متأكد من حذف هذا المتدرب من الدورة؟','<b style="color:#e04e4e">تنبيه !</b>', function () {
	$.ajax({
        type: "post",
        data:{'id':id , 'member_id':member.member_id},
        url: url + "disapprove_app",
        dataType: "JSON",
        beforeSend: function () {
            myApp.showIndicator();
        },
        success: function (result) {
            myApp.hideIndicator();
            if (result.status == true) {
                 myApp.alert(' شكرا لك', '<b style="color:#8bc18b">تم حذف المتدرب</b>');
                $("#d_app"+id).hide();
               } else{
                   myApp.alert(' من فضلك أعد المحاوله لاحقاً', '<b style="color:#e04e4e">تنبيه !</b>');
               }

        },
        error: function (jqXHR, textStatus, errorThrown) {
            myApp.alert('خطا فى الاتصال', '<b style="color:#e04e4e">تنبيه !</b>');
            myApp.hideIndicator();
        }
    });
	
});

}else{
 myApp.alert('يجب التسجيل اولاً', '<b style="color:#e04e4e">تنبيه !</b>');	
}
	

}



// amr bdreldin 


myApp.onPageInit('likes', function (page) {
 
 
  var likeTemplate = $('#likes_script').html();
 
  var compiledLikeTemplate = Template7.compile(likeTemplate); //تحضير الاسكربت لاستقبال المتغيرات
  
   var member = JSON.parse(localStorage.getItem('member'));
    
    if(member != null){
  $.ajax({
        type: "POST",
        data:{'member_id':member.member_id},
        url: url + "get_question",
        dataType: "JSON",
        beforeSend: function () {
            myApp.showIndicator();
        },
        success: function (data) {
           
            if(data.status == true){
            	 $('#likes_content_member_id').val(member.member_id);
             for( var i in data.msg){
                 var item = data.msg[i];
                 var result = compiledLikeTemplate({item});
           	 $('#likes_content').append(result);
             }
           	
            }else{
            $('#likes_content').html('<div class="center" style="text-align: center;padding-top: 10%; font-size: 30px;color:brown;">عذراً ...لا توجد نتائج!</div>');	
            }
            
            myApp.hideIndicator();

        },
        error: function (jqXHR, textStatus, errorThrown) {
            myApp.alert('خطأ فى الاتصال', '<b style="color:#e04e4e">تنبيه !</b>');
            myApp.hideIndicator();
        }
    });
    
    
    
     $('#likes_form').submit(function (e) {
        e.preventDefault();
        
           var formData = new FormData($(this)[0]);
         //  console.log(formData);
            $.ajax({
        type: "POST",
        data:formData,
        url: url + "calc_result",
        dataType: "JSON",
        cache: false,
        contentType: 'json',
        processData: false, // Don't process the files
        contentType: false, // Set content type to false as jQuery will tell the server its a query string request
             
        beforeSend: function () {
            myApp.showIndicator();
        },
        success: function (data) {


            if(data.status == true){
            	 $('#likes_form')[0].reset();
                localStorage.setItem('member', JSON.stringify(data.msg.member));
            	 $('#likes_div').html(data.msg.report);
            	 rendar_home(0);
           	
            }else{
                 myApp.alert(data.msg, '<b style="color:#e04e4e">تنبيه !</b>');
             }
            
            myApp.hideIndicator();

        },
        error: function (jqXHR, textStatus, errorThrown) {
            myApp.alert('خطأ فى الاتصال', '<b style="color:#e04e4e">تنبيه !</b>');
            myApp.hideIndicator();
        }
    });
           
           });
        
        
    }else {
          myApp.alert('يجب تسجيل الدخول', '<b style="color:#e04e4e">تنبيه !</b>');
    }
    

});



myApp.onPageInit('result', function (page) {
    
      var resultTemplate = $('#result_script').html();
 
  var compiledResultTemplate = Template7.compile(resultTemplate); //تحضير الاسكربت لاستقبال المتغيرات
  
   var member = JSON.parse(localStorage.getItem('member'));
    
    if(member != null){
        
         $.ajax({
        type: "POST",
        data:{'member_id':member.member_id},
        url: url + "get_result",
        dataType: "JSON",
        beforeSend: function () {
            myApp.showIndicator();
        },
        success: function (data) {
           
            if(data.status == true){
            	  
             for( var i in data.msg){
                 var item = data.msg[i];
                 var result = compiledResultTemplate({item});
           	 $('#result_content').append(result);
             }
           	
            }else{
            $('#result_content').html('<div class="center" style="text-align: center;padding-top: 10%; font-size: 30px;color:brown;">عذراً ...لا توجد نتائج!</div>');	
            }
            
            myApp.hideIndicator();

        },
        error: function (jqXHR, textStatus, errorThrown) {
            myApp.alert('خطأ فى الاتصال', '<b style="color:#e04e4e">تنبيه !</b>');
            myApp.hideIndicator();
        }
    });
    
        
        
        
    }
    
});




function result_report(result_id) {
    
  var resultReportTemplate = $('#result_report_script').html();
  var compiledResultReportTemplate = Template7.compile(resultReportTemplate); //تحضير الاسكربت لاستقبال المتغيرات
  var member = JSON.parse(localStorage.getItem('member'));
    
    if(member != null){
        
         $.ajax({
        type: "POST",
        data:{'member_id':member.member_id,'result_id':result_id},
        url: url + "get_result_report",
        dataType: "JSON",
        beforeSend: function () {
            myApp.showIndicator();
        },
        success: function (data) {
           if(data.status == true){
               var item = data.msg.report;
                 var result = compiledResultReportTemplate({item});
           	 $('#result_report_content').html(result);
             }else{
            $('#result_report_content').html('<div class="center" style="text-align: center;padding-top: 10%; font-size: 30px;color:brown;">عذراً ...لا توجد نتائج!</div>');	
            }
            
            myApp.hideIndicator();

        },
        error: function (jqXHR, textStatus, errorThrown) {
            myApp.alert('خطأ فى الاتصال', '<b style="color:#e04e4e">تنبيه !</b>');
            myApp.hideIndicator();
        }
    });
    
        
        
        
    }
    
}






// amr bdreldin















myApp.onPageInit('common_ques', function (page) {
	
    var Template = $('#common_ques_script').html(); //script
    var compiledTemplate = Template7.compile(Template); //تحضير الاسكربت لاستقبال المتغيرات

   
    $.ajax({
        type: "post",
        url: url + "common_ques",
        dataType: "JSON",
        beforeSend: function () {
            myApp.showIndicator();
        },
        success: function (result) {
            
            if (result.status == true) {

                if (result.msg != null && result.msg != null) {

                    for (var i in result.msg) {
                      var item = result.msg[i];
                      
                        var res_ques = compiledTemplate({item});
                        $('#common_ques_content').append(res_ques);
                    }

                } else {
                    $('#common_ques_content').html('<div class="center" style="text-align: center;padding-top: 10%; font-size: 30px;color:brown;">عذراً ...لا توجد نتائج !</div>');
                }

            } else {
                $('#common_ques_content').html('<div class="center" style="text-align: center;padding-top: 10%; font-size: 30px;color:brown;">عذراً ...لا توجد نتائج !</div>');
            }

         myApp.hideIndicator();
        },
        error: function (jqXHR, textStatus, errorThrown) {
            myApp.alert('خطا فى الاتصال', '<b style="color:#e04e4e">تنبيه !</b>');
            myApp.hideIndicator();
        }
    });
   

});




function ques_answer(id){

var Template = $('#ques_answer_script').html(); //script
var compiledTemplate = Template7.compile(Template); //تحضير الاسكربت لاستقبال المتغيرات

	
   $.ajax({
        type: "POST",
        data:{'id':id},
        url: url + "ques_answer",
        dataType: "JSON",
        beforeSend: function () {
            myApp.showIndicator();
        },
        success: function (data) {
           
            if(data != null && data.msg != null){
            
           	 data = data.msg;
           	 
           	 var result = compiledTemplate({data});
           	 
           	$('#ques_answer_content').html(result);
               	
            }else{
            $('#ques_answer_content').html('<div class="center" style="text-align: center;padding-top: 10%; font-size: 30px;color:brown;">عذراً ...لا توجد نتائج !</div>');	
            }
            
            myApp.hideIndicator();

        },
        error: function (jqXHR, textStatus, errorThrown) {
            myApp.alert('خطأ فى الاتصال', '<b style="color:#e04e4e">خطأ</b>');
            myApp.hideIndicator();
        }
    });
           
        
    
}



//news 




myApp.onPageInit('add_news', function (page) {
	
	var data = JSON.parse(localStorage.getItem('member'));
	
	
	var Template = $('#add_news_script').html(); //script
    var compiledTemplate = Template7.compile(Template); //تحضير الاسكربت لاستقبال المتغيرات
    var result = compiledTemplate({data}); // ارسال المتغير


    $('#add_news_content').html(result);     
           
        
    
	
});



   


function get_my_news(id){

var Template = $('#edit_news_script').html(); //script
var compiledTemplate = Template7.compile(Template); //تحضير الاسكربت لاستقبال المتغيرات

	
   $.ajax({
        type: "POST",
        data:{'id':id},
        url: url + "get_my_news",
        dataType: "JSON",
        beforeSend: function () {
            myApp.showIndicator();
        },
        success: function (data) {
           
            if(data != null && data.msg != null){
            	//console.log(data);
             data.msg.image = data.msg.image ? images_url + data.msg.image : 'img/mylogo1.png';
           	 data = data.msg;
           	 var result = compiledTemplate({data});
           	 
           	 $('#edit_news_content').html(result);
            }else{
            $('#edit_news_content').html('<div class="center" style="text-align: center;padding-top: 10%; font-size: 30px;color:brown;">عذراً ...لا توجد نتائج!</div>');	
            }
            
            myApp.hideIndicator();

        },
        error: function (jqXHR, textStatus, errorThrown) {
            myApp.alert('خطأ فى الاتصال', '<b style="color:#e04e4e">خطأ</b>');
            myApp.hideIndicator();
        }
    });
           
        
    
}	



myApp.onPageInit('my_news', function (page) {
	
    var coursesTemplate = $('#my_news_script').html(); //script
    var compiledCoursesTemplate = Template7.compile(coursesTemplate); //تحضير الاسكربت لاستقبال المتغيرات

    var member = JSON.parse(localStorage.getItem('member'));
    
    if(member != null){
    
    $.ajax({
        type: "post",
        data:{'id':member.member_id},
        url: url + "my_news",
        dataType: "JSON",
        beforeSend: function () {
            myApp.showIndicator();
        },
        success: function (result) {
            myApp.hideIndicator();
            if (result.status == true) {

                if (result.msg != null && result.msg.news != null) {

                    for (var i in result.msg.news) {
                      var item = result.msg.news[i];
                      item.image = item.image ? images_url + item.image : 'img/mylogo1.png';
                     
                      if(item.active == 1){
		           	 	item.active = '<div class="item-after" style="color:#11ceaa;font-size: 10px;"> منشور </div>';
		           	 }else{
		           	 	item.active = '<div class="item-after" style="color:#ff2d55;font-size: 10px;"> قيد المراجعه </div>';
		           	 }
                      
                        var res_my_news = compiledCoursesTemplate({item});
                        $('#my_news_content').append(res_my_news);
                    }

                } else {
                    $('#my_news_content').html('<div class="center" style="text-align: center;padding-top: 10%; font-size: 30px;color:brown;">عذراً ...لا توجد نتائج !</div>');
                }

            } else {
                $('#my_news_content').html('<div class="center" style="text-align: center;padding-top: 10%; font-size: 30px;color:brown;">عذراً ...لا توجد نتائج !</div>');
            }


        },
        error: function (jqXHR, textStatus, errorThrown) {
            myApp.alert('خطا فى الاتصال', '<b style="color:#e04e4e">تنبيه !</b>');
            myApp.hideIndicator();
        }
    });
    }else{
    	$('#my_news_content').html('<div class="center" style="text-align: center;padding-top: 10%; font-size: 30px;color:brown;">عذراً ...لا توجد نتائج !</div>');
    }

});





